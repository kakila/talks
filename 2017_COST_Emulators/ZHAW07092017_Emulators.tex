\documentclass[xcolor=dvipsnames, USenglish]{beamer}
\input{../includes/beamer_basics.tex} % PACKAGES
\input{../includes/mathsymbols.tex}   % SYMBOLS

% ----------- extra packages
% Plots
\usepackage{pgfplots}
\pgfplotsset{every axis/.append style={line width=1pt}}

%\usepackage{framed}
%\definecolor{shadecolor}{BrickRed}

\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm, text centered, text width=1.5cm, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=0.5cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]

% ----------- Extra symbols
\newcommand{\ccov}[1]{{\color{red}k}\left(#1\right)}
\newcommand{\cmean}[1]{{\color{blue}m}\left(#1\right)}
\newcommand{\sm}{\scalebox{0.5}{-1}}

% ----------- References used and shown
\begin{filecontents}{\jobname.bib}
@article{Carbajal2016,
archivePrefix = {arXiv},
arxivId = {1609.08395},
author = {Carbajal, Juan Pablo and Leit{\~{a}}o, Jo{\~{a}}o Paulo and Albert, Carlo and Rieckermann, J{\"{o}}rg},
doi = {10.1016/j.envsoft.2017.02.006},
eprint = {1609.08395},
journal = {Env. Mod. {\&} Soft.},
title = {{Appraisal of data-driven and mechanistic emulators of nonlinear simulators: The case of hydrodynamic urban drainage models}},
volume = {92},
year = {2017}
}
@article{Wani2017,
author = {Wani, Omar and Scheidegger, Andreas and Carbajal, Juan Pablo and Rieckermann, J{\"{o}}rg and Blumensaat, Frank},
doi = {10.1016/j.watres.2017.05.038},
journal = {Water Research},
month = {sep},
title = {{Parameter estimation of hydrologic models using a likelihood function for censored and binary observations}},
volume = {121},
year = {2017}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% Path to graphics
\graphicspath{{../img/}}

% -----------
\usepackage{../beamer_themes/beamerthemeEawag_blue} % Eawag style

%----------------
% title information
\title{Speeding up simulators with emulators}
\subtitle{Introduction, examples and perspectives}
\author[\texttt{juanpablo.carbajal@eawag.ch}]{Juan Pablo Carbajal}
\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science
  and Technology}
\date[07.09.2017]{September 7, 2017}

% ====================================================================

\begin{document}

% ----------------
% Title frame
% load backgound for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}
{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}
% load backgound for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}
\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------
\section{Introduction}

\begin{frame}
  \frametitle{Speaker's profile}
  \centering
  \includegraphics[width=\textwidth]{intro.png}
\end{frame}

%%----------------
\section{Numerical models}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Large{\textbf{Simulators}}\\
\Large{\textbf{and simulation-hungry applications}}
\end{frame}
}

\begin{frame}{Numerical models: common scenarios}

  \begin{columns}[c]
    \column{.5\textwidth}
      \includegraphics[width=\textwidth]{example1.png}

    \column{.5\textwidth}
    \begin{enumerate}
      \item Inputs (several):
        \begin{itemize}
          \item Rain, e.g. Hyetograph
          \item Initial water levels
          \item Pipe parameters
          \item \dots
        \end{itemize}
      \item Internal states (many)
      \item Outputs (few):
        \begin{itemize}
          \item Water level or flow at marked outlet
        \end{itemize}
    \end{enumerate}
  \end{columns}

\end{frame}

\begin{frame}{Numerical models: common scenarios}
  \framesubtitle{Fan-out, fan-in}

  \centering
  \includegraphics[width=0.8\textwidth]{FanOutIn.png}

\end{frame}

\begin{frame}
  \frametitle{Numerical models: common scenarios}

  Applications:
  \begin{itemize}
  \item Preliminary and detailed engineering design
  \item Optimization
  \item \alert{System identification} (a.k.a. model calibration)
  \item \alert{Real-time control}
  \item \dots
  \end{itemize}

\end{frame}

\begin{frame}{System identification}

  \begin{columns}
    \column{.5\textwidth}
      \includegraphics[width=\textwidth]{f4.png}\\
      \vspace{-1em}\source{GPML toolbox}

    \column{.5\textwidth}
      \begin{enumerate}
        \item Get data
        \item \alert{Find} model parameters values (distributions) consistent with data. Loads of simulations!
      \end{enumerate}
  \end{columns}

\end{frame}

\begin{frame}{Model predictive realtime control}
  \begin{tikzpicture}
      \only<1-4>{
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[width=\textwidth]{model_predictive.png}};
      }
      \begin{scope}[x={(image.south east)},y={(image.north west)}]
        \only<1>{\fill[white] (0.3,0) rectangle (1,1);}
        \only<2>{\fill[white] (0.529,0) rectangle (1,1);}
        \only<3>{\fill[white] (0.756,0) rectangle (1,1);}
      \end{scope}

  \end{tikzpicture}
\end{frame}

\begin{frame}{Model predictive realtime control}
  \vspace{-2em}
  \centering
  \includegraphics[width=\textwidth]{Network_Water.png}\\
  \vspace{-2em}\flushleft\source{RTC4Water}\hfill

  \begin{columns}[t]
    \column{.3\textwidth}
      1. Measure current state
    \column{.4\textwidth}
      2. \alert{Optimize} decisions based on prediction of future states
    \column{.3\textwidth}
      3. Recalibrate model
  \end{columns}
\end{frame}

%----------------
\section{Model order reduction and emulation}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Large{\textbf{"Effective" simulators}}
\end{frame}
}

\begin{frame}[t]{Simulation speed-up tools?}

  \alert{Simulation-hungry} applications are common.
  What can we do?

  \begin{columns}[t]
    \column{0.5\textwidth}
      \begin{block}{Model order reduction (MOR)}
      New model with fewer states approximates original model, e.g. reduced basis methods, balanced truncation, cross Gramian, \ldots
      \footnote{\url{morwiki.mpi-magdeburg.mpg.de}}
      \end{block}
    \column{0.5\textwidth}
      \begin{block}{Emulation}
        Use input-output data to build an interpolant, e.g. Gaussian processes, neural networks, polynomial expansions, \ldots

        Can use details of the original model.
      \end{block}
  \end{columns}
  \vspace{1em}
  There is a gamut from emulation to MOR.
  Emulation $=$ extreme MOR.
  Emulation is "simpler" than MOR.
\end{frame}

\subsection{Model order reduction}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Large{\textbf{MOR, super-brief}}
\end{frame}
}

\begin{frame}{Model order reduction}
  \centering
  \only<1>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<2>{
    \includegraphics[width=0.8\textwidth]{MOR.png}

    Internal states can be recovered (aprox.)
  }
  \only<3>{
    \includegraphics[width=0.8\textwidth]{Snapshots.png}\\
    \includegraphics[width=0.6\textwidth]{Manifold.png}\\
    \vspace{-2em}\flushleft\source{Quarteroni et al.(2016), ISBN 978-3-319-15430-5, Springer}
  }

\end{frame}

\subsection{Emulation}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Large{\textbf{Emulation}}
\end{frame}
}

\begin{frame}{Emulation}
  \framesubtitle{TIMTOWTDI}
  \centering
  \only<1>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<2>{
    \includegraphics[width=0.8\textwidth]{EmulationIdea.png}

    Internal states are lost

    Another name: Input-Output relations of complex nonlinear systems (linear case: \emph{transfer functions})
  }
\end{frame}

\subsubsection{Emulation results}
\begin{frame}[t]{Emulation: some results}
  Emulation of 1D shallow water equations (Saint-Venant)
  \begin{gather*}
  2 c \pder{h}{t} + \pder{Q}{x} = 0\\
  \pder{Q}{t} + 2 \frac{Q}{A} \pder{Q}{x} - \left(2 \frac{Q^2}{A^2} c + g A\right)\pder{h}{x} + gA\left[S_f(Q,h) - S(x)\right] = 0
  \end{gather*}
  \centering
  \includegraphics[width=0.9\textwidth]{stvenant_schema.png}
\end{frame}

\begin{frame}[t, label=emuresults]{Emulation: some results}
  \centering
  \begin{table}[h]
    \caption{Emulation errors for several models}
    \begin{tabularx}{\textwidth}{lp{2.165cm}>{\raggedright\arraybackslash}X}
      \toprule
      Model & RMSE(\%) [\# examples]& Application\\
      \midrule
      Wartegg I (SWMM)\footfullcite{Carbajal2016}   & $\sim 4\%$ [90] & Control\\
      Adliswil (SWMM)\footnotemark[\value{footnote}]    & $\sim 3\%$ [128] & Identification\\
      Wartegg II (SWMM)\footfullcite{Wani2017}  & $\sim 2\%$ [250] & Identification\\
      \bottomrule
    \end{tabularx}
  \end{table}
  \hyperlink{GPregression}{\beamerbutton{Gaussian processes emulation}}
\end{frame}

\begin{frame}{Emulation: Wartegg II}
  \framesubtitle{Speed-up x$1000$ ($1$ week into $10$ minutes \\+ assembly time $\sim 3$ hr)}

  \centering
  \includegraphics[width=0.8\textwidth]{wani_emulation.png}

\end{frame}

%----------------
\section{Summary}

\begin{frame}[t]{Recap}
  \centering
  \begin{itemize}
    \item General simulators $\notin$ simulation intensive applications
    \item Speed-up: general vs. specific (fan-out $\rightarrow$ fan-in)
    \item Detail vs. speed tradeoff is relaxed via emulation/MOR
    \item If general simulators needed (internal states) $\rightarrow$ MOR
    \item If specific problem with "low dimensional" output $\rightarrow$ Emulation
  \end{itemize}

  \vspace{2em}
  \colorbox{Goldenrod}{\makebox[\textwidth]{\textbf{Want to  try out emulation?}}}
  \\\vspace{0.2em}
  Check the \alert{\textbf{EmuMore project}} (contact me!)
\end{frame}

%%----------------
\section{About the EmuMore project}
\begin{frame}{The EmuMore project}
  \framesubtitle{\url{kakila.bitbucket.io/emumore/}}
  \centering
  \includegraphics[scale=1]{emu.png}
  \begin{itemize}
   \item Aim: provide tools and support for simulation speed-up
   \item Collaboration Eawag-HSR
   \item Strong Open Science aligment
   \item Strong Libre Software aligment
  \end{itemize}
\end{frame}

\begin{frame}{Status}
  \framesubtitle{\url{kakila.bitbucket.io/emumore/}}
  %\vspace{-2em}
  \centering
  \includegraphics[width=\textwidth]{emu_teaser.png}
  \begin{itemize}
   \item running since May 2017
   \item 14 \alert{emulation} collaborations (4+ with open datasets, to-be-released)
   \item 1 conference article, 3 journal publications (in preparation)
   \item incoming hands-on workshop in \alert{November 2017}
  \end{itemize}
\end{frame}

%----------------
\section{Closing}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

%----------------
\appendix
%----------------
\section{Gaussian processes (GP)}
\begin{frame}[t,label = GPregression]{Gaussian processes (GP): characterization}
  A GP is a distribution of functions defined by a mean function $\cmean{t}$ and a covariance function $\ccov{t,t'}$.

  A {\it prior} GP conditioned on a design data set $\bset{t_i, y_i} = (T, y(T))$, gives {\it posterior} GP.

  The mean function of posteior GP is used for prediction and the formula reads
  \begin{block}{mean of posterior GP}
  \vspace*{-\baselineskip}
  \setlength\belowdisplayshortskip{0pt}
  \begin{equation*}
  f(t) = \ccov{t, T} \; \big(\ccov{T,T} + \sigma_y I \big)^{-1} \big( y(T) - \cmean{T}\big) + \cmean{t}
  \end{equation*}
  \vspace*{-\baselineskip}
  \end{block}
\noindent The evaluated covariance function $\ccov{T,T}$ is the {\it covariance matrix}.
  \hyperlink{emuresults}{\beamerbutton{Go back}}
\end{frame}

\begin{frame}[t]{Example: ODE}
  \only<1>{
  \vspace{-1em}
  1st order ODE with piece-wise constant input and random input, giving \alert{distribution of functions}
    \setlength\abovedisplayskip{0pt}
    \setlength\belowdisplayskip{0pt}
    \begin{gather*}
    L f(t) - u(t) = \eta(t)\\
    \eta(t) \sim \mathcal{N}\left(0, \Sigma \delta(t-t')\right)
    \end{gather*}
    \begin{center}
    \includegraphics[height=0.5\textheight,trim=1.5cm 6mm 1.45cm 9mm, clip]{SDE.pdf}
    \end{center}
  }
  \only<2>{
    \vspace{-2em}
    Covariance function
    \begin{center}
    \includegraphics[height=0.75\textheight,trim=1cm 3.8mm 1.5cm 8mm, clip]{KernelGPDS.pdf}
    \end{center}
  }
  \only<3>{
    \vspace{-2em}
    \setlength\abovedisplayskip{0pt}
    \setlength\belowdisplayskip{0pt}
    \[
    f(t) = k\left(t, t_i\right) \, \big( k\left(t_i,t_i\right)\big)^{\sm} \, \hat{f}\left(t_i\right) + L^{\sm}u(t)
    \]

    \begin{center}
    \includegraphics[height=0.75\textheight,trim=1.2cm 5mm 1.5cm 7mm, clip]{GPfromDS.pdf}
    \end{center}
  }
    \hyperlink{emuresults}{\beamerbutton{Go back}}

 \end{frame}

\end{document}
