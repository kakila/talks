## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Extrapolation of a trigonometric function
#
g = @(t) sin (2*pi*[1 2] .* t) * [0.7; 0.3];
nx = 25;
x  = sort (rand (nx,1) - 0.5);
y  = g (x);

nx_ = 200;
x_  = linspace (-1.5,1.5,nx_).';
y_  = g (x_);

## Polynomial
yp = interp1 (x,y,x_,'spline','extrap');

## Periodic
#
pkg load gpml
cf = {@covPeriodic};
lf = {@likGauss};
hyp.cov = [0;0;0];
hyp.lik = -7;
hyp = minimize (hyp, @gp, -100,@infExact,[],cf,lf,x,y);
yb  = gp (hyp,@infExact,[],cf,lf,x,y,x_);

## Smooth
#
cf      = {@covSEiso};
lf      = {@likGauss};
hyp.cov = [log(0.1);log(0.75)];
hyp.lik = -7;

prior        = struct ();
prior.cov = {@priorClamped,[]};
infe         = {@infPrior, @infExact, prior};

hyp = minimize (hyp, @gp, -100,infe,[],cf,lf,x,y);
yr   = gp (hyp,infe,[],cf,lf,x,y,x_);

figure (1)
clf
h = plot (x, y, 'ob;data;', x_,y_,'-b;true;');
set(h(1),'markerfacecolor', 'b');
hold on
hp = plot (x_,yp,'-r;spline;','linewidth', 2);
hr = plot (x_,yr,'--c;local;','linewidth', 2);
hb = plot (x_,yb,'--g;periodic;','linewidth', 2);
hold off

hl = legend ('Location','NorthOutside','Orientation','horizontal')
axis ([-1.5 1.5 -1.3 1.3])
xlabel ('x')
ylabel ('y')
grid on

## Print files
#
function genpdf (fname)
  print('-dsvg','tmp.svg');
  unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l tmp.svg'));
  unix (sprintf('inkscape --without-gui --file="tmp.svg" --export-pdf="%s.pdf"',fname));
  delete ('tmp.svg');
endfunction

imgpath = fullfile ('..','..','img');

fname = 'extrapolation_data';
set (h(2), 'visible', 'off');
set (hp, 'visible', 'off');
set (hb, 'visible', 'off');
set (hr, 'visible', 'off');
set (hl, 'visible', 'off');
genpdf (fullfile (imgpath, fname));

fname = 'extrapolation_poly';
set (hp, 'visible', 'on');
set (hr, 'visible', 'on');
genpdf (fullfile (imgpath, fname));

fname = 'extrapolation_gp';
set (hb, 'visible', 'on');
genpdf (fullfile (imgpath, fname));

fname = 'extrapolation_all';
set (h(2), 'visible', 'on');
set (hl, 'visible', 'on');
genpdf (fullfile (imgpath, fname));
