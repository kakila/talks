## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## intrapolation of a band-limited function
#

if ~exist('A','var')
  ff = linspace (1,5,10);
  A  = randn(size(ff)).'; A/=sum(A);
  g  = @(t) sin (2*pi*ff .* t) * A;
endif

x_  = linspace (0,1,200).';
y_  = g (x_);
fs  = ceil (2 * max (ff) * 1.2);
x   = linspace (0,1,fs).';
dx  = x(2) - x(1);
fs  = 1/dx;
y   = g (x);

## Polynomial
yp      = interp1 (x,y,x_,'previous');
yp(:,2) = interp1 (x,y,x_,'linear');
yp(:,3) = interp1 (x,y,x_,'spline');

## Band limited
T  = x(end) - x(1) + dx;
n  = -20:20;
nn = length (n);
X  = (x + n*T)(:);
Y  = repmat (y, 1, nn);
for i=1:nn
  if mod(n(i),2)
    Y(:,i) = flipud (Y(:,i));
  endif
endfor
Y  = Y(:);
yb = sinc (fs * (x_ - X.')) * Y;

err = sqrt (sumsq (y_ - [yp yb])) / sqrt (sumsq(y_));
printf ("Error:\nnearest\t%.1f\nlinear\t%.1f\nspline\t%.1f\nsinc\t%.1f\n", err*100);

figure (1)
clf
h = plot (x, y, 'ob;data;', x_,y_,'-b;true;');
set(h(1),'markerfacecolor', 'b');
hold on
hp = plot (x_,yp(:,1),'-;nearest;', ...
           x_,yp(:,2),'-;linear;', ...
           x_,yp(:,3),'-;spline;');
hb = plot (x_,yb,'--c;sinc;','linewidth', 2);

hold off

hl = legend ('Location','NorthOutside','Orientation','horizontal');
axis tight
xlabel ('x')
ylabel ('y')
grid on

## Print files
#
function genpdf (fname)
  print('-dsvg','tmp.svg');
  unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l tmp.svg'));
  unix (sprintf('inkscape --without-gui --file="tmp.svg" --export-pdf="%s.pdf"',fname));
  delete ('tmp.svg');
endfunction

imgpath = fullfile ('..','..','img');

fname = 'intrapolation_data';
set (h(2), 'visible', 'off');
set (hp, 'visible', 'off');
set (hb, 'visible', 'off');
set (hl, 'visible', 'off');
genpdf (fullfile (imgpath, fname));

fname = 'intrapolation_poly';
set (hp, 'visible', 'on');
set (hb, 'visible', 'on');
genpdf (fullfile (imgpath, fname));

fname = 'intrapolation_all';
set (h(2), 'visible', 'on');
set (hl, 'visible', 'on');
genpdf (fullfile (imgpath, fname));
