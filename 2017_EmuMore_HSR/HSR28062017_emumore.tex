\documentclass[xcolor=dvipsnames]{beamer}

\input{../includes/beamer_basics.tex}

\usepackage{pgfplots}
\pgfplotsset{every axis/.append style={line width=1pt}}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@article{Carbajal2016,
archivePrefix = {arXiv},
arxivId = {1609.08395},
author = {Carbajal, Juan Pablo and Leit{\~{a}}o, Jo{\~{a}}o Paulo and Albert, Carlo and Rieckermann, J{\"{o}}rg},
doi = {10.1016/j.envsoft.2017.02.006},
eprint = {1609.08395},
journal = {Env. Mod. {\&} Soft.},
title = {{Appraisal of data-driven and mechanistic emulators of nonlinear simulators: The case of hydrodynamic urban drainage models}},
volume = {92},
year = {2017}
}
@article{Wani2017,
author = {Wani, Omar and Scheidegger, Andreas and Carbajal, Juan Pablo and Rieckermann, J{\"{o}}rg and Blumensaat, Frank},
doi = {10.1016/j.watres.2017.05.038},
journal = {Water Research},
month = {sep},
title = {{Parameter estimation of hydrologic models using a likelihood function for censored and binary observations}},
volume = {121},
year = {2017}
}
@article{Davidsen2017,
author = {Davidsen, Steffen and L{\"{o}}we, Roland and Thrys{\o}e, Cecilie and Arnbjerg-Nielsen, Karsten},
doi = {10.2166/hydro.2017.152},
issn = {1464-7141},
journal = {Journal of Hydroinformatics},
month = {may},
title = {{Simplification of one-dimensional hydraulic networks by automated processes evaluated on 1D/2D deterministic flood models}},
year = {2017}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% Customs commmands
% Calculus
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\rot}[1]{\nabla \times {#1}}
\newcommand{\diver}[1]{\nabla \cdot {#1}}
\newcommand{\definter}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\inter}[2]{\int {#1}\ud {#2}}
\newcommand{\braket}[2]{\langle {#1} , {#2} \rangle}

% Misc
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{#1}\!\times\!{#2}}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}

% Operators
\DeclareMathOperator*{\err}{err}
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\linspan}{span}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\sinc}{sinc}

% Logicals
\newcommand{\suchthat}{\big \backslash \;}
% Comments
\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}
% Line
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% -----------
% Set Eawag style

%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
%\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Accelerating numerical simulators}
\subtitle{\textbf{EmuMore} project (Eawag-HSR)}

\author[JPi Carbajal]{Juan Pablo Carbajal\\\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science
  and Technology}

\date{July 10, 2017}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame
% load backgound for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load backgound for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[eawag] % set footer
\addtocounter{framenumber}{-1}  % don't count title page


%----------------
\section{Introducing myself}

\begin{frame}
  \frametitle{Background}
  \centering
  \includegraphics[width=\textwidth]{intro.png}
\end{frame}

%%----------------
\section{Numerical models}
\begin{frame}{Numerical models: common scenarios}

  \begin{columns}[c]
    \column{.5\textwidth}
      \includegraphics[width=\textwidth]{example1.png}

    \column{.5\textwidth}
    \begin{enumerate}
      \item Inputs (several):
        \begin{itemize}
          \item Rain, e.g. Hyetograph
          \item Initial water levels
          \item Pipe parameters
          \item \dots
        \end{itemize}
      \item Internal states (many)
      \item Outputs (few):
        \begin{itemize}
          \item Water level or flow at marked outlet
        \end{itemize}
    \end{enumerate}
  \end{columns}

\end{frame}

\begin{frame}{Numerical models: common scenarios}
  \framesubtitle{Fan-out, fan-in}

  \centering
  \includegraphics[width=0.8\textwidth]{FanOutIn.png}

\end{frame}

\begin{frame}
  \frametitle{Numerical models: common scenarios}

  Applications:
  \begin{itemize}
  \item Preliminary and detailed engineering design
  \item Optimization
  \item \alert{System identification} (a.k.a. model calibration)
  \item \alert{Real-time control}
  \item \dots
  \end{itemize}

\end{frame}

\begin{frame}{System identification}

  \begin{columns}
    \column{.5\textwidth}
      \includegraphics[width=\textwidth]{f4.png}\\
      \source{GPML toolbox}

    \column{.5\textwidth}
      \begin{enumerate}
        \item Get data
        \item \alert{Find} parameters values (distributions) consistent with data
      \end{enumerate}
  \end{columns}
  \begin{center}
    Model as sensor (Soft-sensor)\\ \emph{of directly unmeasurable quantities}
  \end{center}

\end{frame}

\begin{frame}{Model predictive realtime control}
  \begin{tikzpicture}
      \only<1-4>{
      \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[width=\textwidth]{model_predictive.png}};
      }
      \begin{scope}[x={(image.south east)},y={(image.north west)}]
        \only<1>{\fill[white] (0.3,0) rectangle (1,1);}
        \only<2>{\fill[white] (0.529,0) rectangle (1,1);}
        \only<3>{\fill[white] (0.756,0) rectangle (1,1);}
      \end{scope}

  \end{tikzpicture}
\end{frame}

\begin{frame}{Model predictive realtime control}
  \centering
  \includegraphics[width=0.8\textwidth]{Network_Water.png}\\
  \source{RTC4Water}
  \begin{columns}[t]
    \column{.3\textwidth}
      1. Measure current state
    \column{.4\textwidth}
      2. \alert{Optimize} decisions based on prediction of future states
    \column{.3\textwidth}
      3. Recalibrate model
  \end{columns}

\end{frame}

%%----------------
\section{About the project}
\begin{frame}{The EmuMore project}
  \framesubtitle{\url{kakila.bitbucket.io/emumore/}}
  \centering
  \includegraphics[scale=1]{emu.png}
  \begin{itemize}
   \item Collaboration Eawag-HSR
   \item Aim: provide tools and support for simulation speed-up
   \item Strong Open Science aligment
   \item Strong Libre Software aligment
  \end{itemize}
\end{frame}

\begin{frame}{Status}
  \vspace{-2em}
  \centering
  \includegraphics[width=\textwidth]{emu_teaser.png}
  \begin{itemize}
   \item runnig since May 2017
   \item 14 \alert{emulation} collaborations (4+ with open datasets (to-be-released)).
   \item 1 conference article (accepted)
   \item 2 journal publications (in preparation)
   \item incoming hands-on workshop in November
  \end{itemize}
\end{frame}

%----------------
\section{Model order reduction and emulation}
\begin{frame}[t]{Simulation speed-up tools?}

  The project's software provides (goal):

  \begin{columns}[t]
    \column{0.5\textwidth}
      \begin{block}{Model order reduction (MOR)}
      New model with fewer states approximates original model, e.g. reduced basis methods, balanced truncation, cross Gramian, \ldots
      \footnote{\url{morwiki.mpi-magdeburg.mpg.de}}
      \end{block}
    \column{0.5\textwidth}
      \begin{block}{Emulation}
        Use input-output data to build an interpolant, e.g. Gaussian processes, neural networks, polynomial expansions, \ldots

        Can use details of the original model.
      \end{block}
  \end{columns}
  \vspace{1em}
  There is a gamut from emulation to MOR.
  Emulation $=$ extreme MOR.
  Emulation is "simpler" than MOR.
\end{frame}

\subsection{Model order reduction}
\begin{frame}{Model order reduction}
  \centering
  \only<1>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<2>{
    \includegraphics[width=0.8\textwidth]{MOR.png}

    Internal states can be recovered (aprox.)
  }
  \only<3>{
    \includegraphics[width=0.8\textwidth]{Snapshots.png}\\
    \includegraphics[width=0.6\textwidth]{Manifold.png}

    \source{Quarteroni et al.(2016), ISBN 978-3-319-15430-5, Springer}
  }

\end{frame}

\subsection{Emulation}
\begin{frame}{Emulation}
  \framesubtitle{TIMTOWTDI}
  \centering
  \only<1>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<2>{
    \includegraphics[width=0.8\textwidth]{EmulationIdea.png}

    Internal states are lost

    Another name: Input-Output relations of complex nonlinear systems (linear case: \emph{transfer functions})
  }

\end{frame}

\begin{frame}[t]{Emulation: some results}
    \only<1>{
    Emulation of 1D shallow water equations (Saint-Venant)
    \begin{gather*}
    2 c \pder{h}{t} + \pder{Q}{x} = 0\\
    \pder{Q}{t} + 2 \frac{Q}{A} \pder{Q}{x} - \left(2 \frac{Q^2}{A^2} c + g A\right)\pder{h}{x} + gA\left[S_f(Q,h) - S(x)\right] = 0
    \end{gather*}
    \centering
    \includegraphics[width=0.9\textwidth]{stvenant_schema.png}

    }
    \only<2>{
  \begin{table}[h]
    \centering
    \caption{Emulation errors for several models}
    \begin{tabularx}{\textwidth}{lp{2.165cm}>{\raggedright\arraybackslash}X}
      \toprule
      Model & RMSE(\%) [\# examples]& Application\\
      \midrule
      Wartegg I (SWMM)\footfullcite{Carbajal2016}   & $\sim 4\%$ [90] & Control\\
      Adliswil (SWMM)\footnotemark[\value{footnote}]    & $\sim 3\%$ [128] & Identification\\
      Wartegg II (SWMM)\footfullcite{Wani2017}  & $\sim 2\%$ [250] & Identification\\
      \bottomrule
    \end{tabularx}
  \end{table}
  }

\end{frame}

\begin{frame}{Emulation: Wartegg II}
  \framesubtitle{Speed-up x$1000$ ($1$ week into $10$ minutes \\+ assembly time $\sim 3$ hr)}

  \centering
  \includegraphics[width=0.8\textwidth]{wani_emulation.png}

\end{frame}

\section{Summary}
\begin{frame}
  \frametitle{Summary}
  \begin{center}
  \begin{tikzpicture}[node distance=2cm]

  \node (problem)[startstop]{Problem: ED, SI, RTC, \dots};
  \node (model)[decision, right of=problem,xshift=2cm]{Model};
  \node (modeling)[process, right of=model, xshift=2cm]{Build a model};
  \node (dataset)[decision, yshift=-0.5cm, below of=model]{Dataset};
  \node (dataseting)[process, right of=dataset, xshift=2cm]{Assemble dataset};
  \node (stop)[startstop, below of=dataset, yshift=-0.5cm]{Emulate};

  \draw [arrow] (problem) -- (model);
  \draw [arrow] (model) -- node[anchor=south]{no} (modeling);
  \draw [arrow] (model) -- node[anchor=east]{yes} (dataset);
  \draw [arrow] (dataset) -- node[anchor=south]{no} (dataseting);
  \draw [arrow] (dataset) -- node[anchor=east]{yes} (stop);
  \draw [arrow] (modeling) -- (dataseting);
  \draw [arrow] (dataseting) |- (stop);

  \end{tikzpicture}
  \end{center}
\end{frame}

%----------------
\section{Closing}

\begin{frame}[t]{Recap}
  \begin{itemize}
    \item General simulators $\notin$ simulation intensive applications
    \item Speed-up: general vs. specific (fan-out $\rightarrow$ fan-in)
    \item Detail vs. speed tradeoff is relaxed via emulation/MOR
    \item If general simulators needed (internal states) $\rightarrow$ MOR
  \end{itemize}
  \vspace{0.5em}
  \fullcite{Carbajal2016}

  \vspace{0.5em}
  \fullcite{Wani2017}
\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

%%----------------
\section{Emulation in urban water networks}
\begin{frame}{Urban water networks}
  \centering
  \includegraphics[height=0.7\textheight]{melbourne_network.png}\footfullcite{Davidsen2017}
\end{frame}

\begin{frame}{The CSO problem}
  \includegraphics[width=\textwidth]{cso.jpg}
\end{frame}

\end{document}
