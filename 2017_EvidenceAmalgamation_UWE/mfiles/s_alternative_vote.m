## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

nC = 5;    # number of options
nV = 100;   # number of voters

votes = zeros (nV, nC);
for i=1:nV
  votes(i,:) = randperm (nC);
endfor

function [winner, t, v, id] = alternative_vote_step (v, id)
  winner = [];
  nc     = size (v, 2);
  # Is there only one option left?
  if nc == 1
    winner = id(1);
    return
  endif

  # Does an option has majority?
  t   = mean (v == 1);
  idx = find (t > 0.5);
  if !isempty (idx)
    winner = id(idx);
    return
  endif

  # No winner, then drop worst and redistribute
  ## Drop
  [~,idx]       = min (t);
  tmp           = id(idx);
  id(idx:end-1) = id(idx+1:end);
  id(end)       = tmp;
  tmp           = v(:,idx);
  v(:,idx)      = [];
  ## Redistribute
  for i=1:max(tmp)
    v(tmp==i,:) -= 1 .* (v(tmp==i,:) > i);
  endfor

endfunction
