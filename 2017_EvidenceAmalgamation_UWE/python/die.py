'''
Copyright (C) 2017 - Juan Pablo Carbajal

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


@author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
'''

from vpython import *

def create_die (sc, data):

  depth = 0.1
  faces = [
            {'pos':vector(0,0,1+depth),  'bsl':vector(1,0,0)},
            {'pos':vector(1,0,0),  'bsl':vector(1,0,0), 'angle':pi/2, 'axis':vector(0,1,0)},
            {'pos':vector(0,0,-1), 'bsl':vector(1,0,0), 'angle':pi,  'axis':vector(0,1,0)},
            {'pos':vector(-1-depth,0,0), 'bsl':vector(0,0,1)},
            {'pos':vector(0,-1,0), 'bsl':vector(1,0,0), 'angle':pi/2,'axis':vector(1,0,0)},
            {'pos':vector(0,1+depth,0),  'bsl':vector(1,0,0), 'angle':-pi/2,'axis':vector(1,0,0)},
           ]

  dice = box(display=sc, pos=vector(0,0,0), length=2, width=2, height=2)

  face=6*[0]
  for i in range(len(faces)):
    face[i]=text(display=sc, text=data[i], spacing=0, align='center',
                 depth=-depth, color=color.black,axis=faces[i]['bsl'])

    face[i].pos -= face[i].height / 2 * vector(0,1,0);

    if 'angle' in faces[i].keys():
      face[i].rotate(angle=faces[i]['angle'],axis=faces[i]['axis'])

    face[i].pos += faces[i]['pos']

    sc.forward = -vector(1,1,1)
    sc.range   = 3

if __name__ == "__main__":
  import sys
  if len(sys.argv) > 1:
    w = int(sys.argv[1])
    h = int(sys.argv[2])
  else:
    w = h = 450

  # Dice
  data = [
          6*['3'],
          4*['4']+2*['0'],
          2*['5']+3*['1']+['5'],
          4*['2']+2*['.6']
         ]
  bckg = [
           vector(0,1,0),
           vector(0,1,0),
           vector(0,1,0),
           vector(0,1,0)
          ]

  for i in range(len(data)):
    sc = canvas(width=w, height=h, background=bckg[i])
    create_die(sc, data[i])
