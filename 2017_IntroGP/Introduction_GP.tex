\documentclass[10pt,xcolor={dvipsnames,svgnames}, english]{beamer}
\usetheme{Berkeley}
\usecolortheme[named=CornflowerBlue]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts}
\setbeameroption{hide notes}
\setbeamertemplate{note page}[plain]
\setlength\abovedisplayskip{0pt}

\input{../includes/beamer_basics.tex}

\usepackage[absolute,overlay]{textpos}

\usepackage{thmtools}
\usepackage{bbold}
\usepackage{abraces,mathtools}

\usepackage{minted} % Highlight source code using Pygments

%\usepackage{pgfplots}
%\pgfplotsset{every axis/.append style={line width=1pt}}

\setbeamercolor{section number projected}{bg=Gainsboro,fg=SlateGrey}
\setbeamertemplate{subsection in toc}{\leavevmode\leftskip=2em\inserttocsubsection\par}

\setbeamertemplate{caption}[numbered]
\renewcommand\mathfamilydefault{\rmdefault}

%% page number
\setbeamertemplate{footline}{%
    \raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}
          \scriptsize\insertframenumber}}}\hspace*{5pt}}

\graphicspath{{img/}}

%% blocks
\newenvironment{Fview}{%
  \begin{block}{Function view}}{\end{block}}
\newenvironment{Dview}{%
  \begin{exampleblock}{Data view}}{\end{exampleblock}}

\input{../includes/mathsymbols.tex}

\newcommand{\sm}{\scalebox{0.5}{-1}}

% Path to graphics
\graphicspath{{../img/}}

% Logo scaling and position manual$
%FIXME
\addtobeamertemplate{frametitle}{}{%
\begin{textblock*}{100mm}(\paperwidth-2.5cm,0.6cm)
\includegraphics[scale=0.7]{eawag_logo.pdf}
\end{textblock*}
}

\begin{document}
% Title data
\title[Intro to GP]{Gaussian Process Regression}
\subtitle[Tutorial]{An intuitive introduction}
\author[JuanPi Carbajal]{Juan Pablo Carbajal}
\institute[EAWAG]{
  Siedlungswasserwirtschaft\\
  Eawag - aquatic research\\
  D{\"u}bendorf, Switzerland\\[1ex]
  \texttt{juanpablo.carbajal@eawag.ch}
}
\date[November 2017]{November 24, 2017}

\begin{frame}[plain]
  \titlepage
\end{frame}

%----------- Machine Learning --------------------------------------------------%
\section{Learning from data}
\begin{frame}{The learning problem in a nutshell}
    \centering
    \includegraphics[height=0.8\textheight]{regress1.pdf}

    Data given $\big\lbrace\left(t_i,y_i\right)\big\rbrace = (\bm{t},\bm{y})$, what model to use?
\end{frame}

\input{../includes/learning_from_data.tex}


%----------- Polynomial interpolation --------------------------------------------------%
\section{Interpolation (polynomial)}
\begin{frame}{Data set}
    \centering
    \includegraphics[height=0.8\textheight]{regress1.pdf}

    Data given $\big\lbrace\left(t_i,y_i\right)\big\rbrace = (\bm{t},\bm{y})$, what model to use?
\end{frame}

\begin{frame}{Naive regression}
  \begin{columns}[c]
  \begin{column}{0.5\textwidth}
    \includegraphics[width=\linewidth]{regress2.pdf}
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{gather*}
    w_0 + w_1 t + w_2 t^2 + w_3 t^3 = y(t)\\
    \begin{bmatrix}1 & t & t^2 & t^3\end{bmatrix} \begin{bmatrix}w_0\\w_1\\w_2\\w_3\end{bmatrix} = y(t)\\
    \bm{\phi}(t)^\trp \bm{w} = y(t)\\
    \end{gather*}
  \end{column}
  \end{columns}

  \begin{equation*}
  \begin{bmatrix}1 & t_1 & t_1^2 & t_1^3\\1 & t_2 & t_2^2 & t_2^3\\1 & t_3 & t_3^2 & t_3^3\end{bmatrix} \begin{bmatrix}w_0\\w_1\\w_2\\w_3\end{bmatrix} = \begin{bmatrix}y_1\\y_2\\y_3\end{bmatrix}\quad
  \Phi(\bm{t})^\trp\bm{w} = \begin{bmatrix}\bm{\phi}(t_1)^\trp\\ \bm{\phi}(t_2)^\trp\\ \bm{\phi}(t_3)^\trp \end{bmatrix} \bm{w} = \bm{y}
  \end{equation*}

\end{frame}

\begin{frame}[t]{Pseudo-inverse}
  \begin{itemize}
    \item $\Phi^\trp$ is an $\stimes{n}{N}$ ($\stimes{3}{4}$: samples $\times$ basis functions) matrix with $N \geq n$, then $\rank \Phi^\trp \leq n$
    \item With a feature vector $\bm{\phi}$ "complex" enough we have that $\rank \Phi^\trp = n$, i.e.
    the $n$ row vectors of the matrix are linearly independent, $\exists \left(\Phi^\trp\Phi\right)^{-1}$
    \item $\Phi^\trp\Phi$ is called \emph{Gramian} matrix: the matrix of all scalar products.
  \end{itemize}

  \vspace{-1em}
  \begin{align*}
    \Phi^\trp\bm{w} = \bm{y} \; \rightarrow \;
    \rlap{$\overbrace{\phantom{\Phi^\trp\Phi \left(\Phi^\trp\Phi\right)^{-1}}}^{\mathclap{\bm{I}}}$}
    \Phi^\trp\underbrace{\Phi \left(\Phi^\trp\Phi\right)^{-1} \Phi^\trp \bm{w}}_{\mathclap{\bm{w}_*}} &= \bm{y}\\
    \Phi \left(\Phi^\trp\Phi\right)^{-1} \Phi^\trp \bm{w} = \underbrace{\Phi \left(\Phi^\trp\Phi\right)^{-1}}_{\mathclap{\text{\tiny Moore-Penrose pseudoinverse}}} \bm{y} &= \bm{w}_*
  \end{align*}
\end{frame}

\begin{frame}{A change of perspective}
  Instead of looking at the rows, look at the columns. These are l.i. functions $\psi_i(t)= t^{i-1}$. The model looks like

  \begin{equation*}
    y(t) = \sum_{i=0}^{N-1}\psi_i(t) w_i
  \end{equation*}

  and the regression problem now looks like

  \begin{equation*}
    \psi_i(t) = t^{i-1},\quad \Psi(\bm{t})\bm{w} = \begin{bmatrix}\psi_3(\bm{t}) & \psi_2(\bm{t}) & \psi_1(\bm{t}) & \psi_0(\bm{t})\end{bmatrix} \bm{w} = \bm{y}
  \end{equation*}

  Note that $\Psi = \Phi^\trp$.
\end{frame}

\begin{frame}{A change of perspective}
  \begin{itemize}
    \item $\Psi$ is an $\stimes{n}{N}$ ($\stimes{3}{4}$) matrix with $N \geq n$, then $\rank \Psi \leq n$
    \item If the $N$ column vectors of the matrix linearly independent, then $\exists \left(\Psi\Psi^\trp\right)^{-1}$
    \item $K=\Psi\Psi^\trp$ is called \emph{Covariance} matrix: $K_{ij} = \displaystyle{\sum_{k=0}^{N-1}} \psi_k(t_i)\psi_k(t_j)$.  \end{itemize}

  \begin{align*}
    \Psi\bm{w} = \bm{y} \; \rightarrow \;
    \rlap{$\overbrace{\phantom{\Psi\Psi^\trp \left(\Psi\Psi^\trp\right)^{-1}}}^{\mathclap{\bm{I}}}$}
    \Psi\underbrace{\Psi^\trp \left(\Psi\Psi^\trp\right)^{-1} \Psi \bm{w}}_{\mathclap{\bm{w}_*}} &= \bm{y}\\
    \Psi^\trp \left(\Psi\Psi^\trp\right)^{-1} \Psi \bm{w} = \underbrace{\Psi^\trp \left(\Psi\Psi^\trp\right)^{-1}}_{\mathclap{\text{\tiny Moore-Penrose pseudoinverse}}} \bm{y} &= \bm{w}_*
  \end{align*}
\end{frame}

\begin{frame}{Recapitulation: the problem}
  Given $n$ examples $\left\lbrace \left(t_i,y_i\right) \right\rbrace$,
  propose a model using $N \geq n$ l.i. functions (a.k.a. \alert{features}),
  \begin{equation*}
    \bm{f}(t) = \sum_i^N \psi_i(t)w_i
  \end{equation*}
  \noindent and find some "good" $\left\lbrace w_i \right\rbrace$.

  \begin{reference}{2mm}{4em}
  Hansen, Per Christian. Rank-deficient and discrete ill-posed problems: numerical aspects of linear inversion. Vol. 4. Siam, 1998.\\
  Wendland, Holger. Scattered data approximation. Vol. 17. Cambridge university press, 2004.
  \end{reference}
\end{frame}
%======%

\begin{frame}{Recapitulation: the solution}
  \begin{Dview}
    \setlength\abovedisplayskip{0pt}
    Think in terms of $n$ feature vectors $\bm{\phi}_i$ in a (high) dimensional space $\mathbb{R}^N$
    \begin{equation*}
      \bm{\phi}_j^\trp = \begin{bmatrix}\psi_1(t_j) & \ldots & \psi_N(t_j)\end{bmatrix}, \quad j=1,\ldots, n
    \end{equation*}
    The solution reads
    \begin{equation*}
      f(t) = \Phi(t)^\trp\bm{w}_* = \overbrace{\Phi(t)^\trp\Phi}^{\mathclap{\text{\tiny scalar product}}} \big(\underbrace{\Phi^\trp\Phi}_{\mathclap{\text{\tiny scalar product}}}\big)^{\sm} \bm{y}
    \end{equation*}
  \end{Dview}

  \begin{Fview}
    \setlength\abovedisplayskip{0pt}
    Think in terms of a $N$ dimensional function space $\mathcal{H}$ spanned by the $\psi_i(t)$. The solution reads
    \begin{equation*}
      f(t) = \Psi(t)\bm{w} = \overbrace{\Psi(t)\Psi^\trp}^{\mathclap{\text{\tiny covariance}}} \big(\underbrace{\Psi\Psi^\trp}_{\mathclap{\text{\tiny covariance}}}\big)^{\sm} \bm{y} = k(t,\bm{t}) \; k(\bm{t},\bm{t})^{-1}\bm{y}
    \end{equation*}
  \end{Fview}
\end{frame}
%======%

\begin{frame}{The Kernel trick}
To calculate the solutions we only need scalar products or covariances: \alert{we never use the actual $\left\lbrace \phi_i \right\rbrace$ or $\left\lbrace \psi_i \right\rbrace$}.
\begin{equation*}
\cov_{\Psi}(t,t') = k(t,t') = \Phi(t)\cdot\Phi(t')
\setlength\belowdisplayskip{0pt}
\end{equation*}

\begin{alertblock}{Infinite features}
Now we can use $N =  \infty$, i.e infinite dimensional features or infinite basis functions!
\end{alertblock}

By selecting valid covariance functions we implicitly select features for our model.

\alert{How to choose the covariance function?} \pause Prior knowledge about the solution.

\begin{reference}{2mm}{3em}
Rasmussen, C., \& Williams, C. (2006). Gaussian Processes for Machine Learning. \url{http://www.gaussianprocess.org/gpml/}
\end{reference}
\end{frame}

\begin{frame}{Digression: back to the solution}
  Lets call the pseudoinverse $\Psi^+$. The proposed solution

  \begin{equation*}
    \Psi^+ \bm{y} = \bm{w}_*, \, \Psi\bm{w}_* = \Psi\Psi^+ \bm{y} = \bm{y}\; \rightarrow \; y(t) = \Psi(t) \bm{w}_*
  \end{equation*}

  \noindent LHS of the arrow is the interpolation, RHS is the "intra-" or extra-polation.

  But with any random vector $\bm{\xi}$ we have

  \begin{align*}
    \bm{w}_* + \overbrace{\left(I - \Psi^+\Psi\right)}^{\nullsp \Psi}\bm{\xi} &= \hat{\bm{w}}_*\\
    \Psi \hat{\bm{w}}_* &= \bm{y} + \left(\Psi - \underbrace{\Psi\Psi^+}_{\mathclap{I}}\Psi\right)\bm{\xi} = \bm{y}
  \end{align*}

  \noindent $\hat{\bm{w}}_*$ also solves the interpolation problem. \alert{There are many solutions!} (unless $\Psi^+\Psi = I$, i.e. $\nullsp \Psi = 0$, i.e. invertible matrix: not our case).
\end{frame}

\begin{frame}{Digression: back to the solution}
    \centering
    \includegraphics[height=0.8\textheight]{regress3.pdf}

    $\bm{w}_* + \left(I - \Psi^+\Psi\right)\bm{\xi}$
\end{frame}

\begin{frame}{Gaussian Process}
    \centering
    \includegraphics[height=0.8\textheight]{gp.png}
\end{frame}

%%----------- Sampling Gaussian distributions --------------------------------------------------%
%\section {Multidimensional Gaussian distributions}
%\begin{frame}[fragile=singleslide]{Covariance as similarity}
%  \[
%    k(t,r) = a^2 \exp (-\frac{(t - r)^2}{2 \sigma^2})
%  \]
%
%  \begin{minted}{octave}
%    t  = linspace (-5,5,100).';
%    s2 = (0.2)^2;
%    K  = exp (- (t - t.').^2 / 2 / s2);
%  \end{minted}
%\end{frame}

%\begin{frame}{Many dimensions}
%  \[
%    k(\bm{x},\bm{y}) = a^2 \exp (-\frac{1}{2}\bm{x}^\trp \Sigma^{-1}\bm{z})
%  \]

%  \begin{minted}{octave}
%    t  = linspace (-5,5,100).';
%    s2 = (0.2)^2;
%    K  = exp (- (t - t.').^2 / 2 / s2);
%  \end{minted}

%\end{frame}

%%----------- Perspectives  --------------------------------------------------%
%\section[Perspectives]{Perspectives}
%\begin{frame}{Perspectives}
%\begin{itemize}
%\item Hilbert spaces: any inner product will do $\rightarrow$ FEM.
%\item Superposition of features is everywhere.
%\item Add knowledge about nonlinear systems.
%\end{itemize}
%\end{frame}

%----------- Closing  --------------------------------------------------%
\section*{}
\begin{frame}
\fontsize{20pt}{1}\selectfont
 \color{NavyBlue}{\begin{center}{\bf Thank you!}\end{center}}
\end{frame}

\end{document}
