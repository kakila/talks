## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

#Example of linear SDE
u   =@(t,tf) (1 + tanh (50 * t)) .* (1 - tanh (50 * (t - tf))) / 4;
ode =@(x,t,a,tf) -a*x + u(t,tf);

# Covaraince function
function k = K(t,r,a)
  t = t(:);    # first argument is rows
  r = r(:).';  # second argument is columns

  dT = t - r;
  tf = (dT >= 0);

  k = zeros (size (dT));

  k(tf)  = (r.*exp(-a*dT))(tf);
  k(!tf) = (t.*exp(a*dT))(!tf);
endfunction

randn ("state", double ("abracadabra"));

# Time vector
nT = 1e2;
T  = 2;
t  = linspace (0, T, nT).';
dt = t(2)-t(1);

# Observation times
n   = 5; # number of osbervations
idx = sort (randi ([10 nT-10], n, 1), "ascend");
to  = t(idx);

# Noise
sigma = 0.4;
dW    = randn (nT,1);

## Data
A  = 2;                    # Decay
tf = T * 0.65;                # End of actuation

#reference trajectories
y = yn = zeros (nT,1);        # Determinstic and stochastic solution
y(1)   = 1;                     # Inital condition
yn(1)  = y(1) + dt*sigma*dW(1);

y = lsode (@(x,t)ode(x,t,A,tf), y(1), t); # determinsitc: L^{-1}*u(t)

for i = 1:nT-1                          # stochastic
  yn(i+1) = yn(i) + dt * (-A*yn(i) + u(t(i),tf) + sigma*dW(i+1));
endfor

# observations
yo = yn(idx);

# Predictions
Koo = sigma^2*K (to,to,A);
Kto = sigma^2*K (t,to,A);
Ktt = sigma^2*K (t,t,A);
l   = sqrt(eps) + 0;           #regularization parameter
Kiv = cholinv (Koo + l * eye (size (Koo)));

yp  = Kto * Kiv * (yo - y(idx)) + y;  # predictive mean
dy  = diag (Ktt - Kto * Kiv * Kto.'); # variance

# Other kernel
L = 0.5;
G =@(t,r,a) exp (- (t(:)-r(:).').^2/2/a^2);
Goo = G (to,to,L);
Gto = G (t,to,L);
Gtt = G (t,t,L);
Giv = cholinv (Goo + l * eye (size (Goo)));

zp  = Gto * Giv * (yo - y(idx)) + y;  # predictive mean
dz  = diag (Gtt - Gto * Giv * Gto.'); # variance

figure (1);
clf;
h = plot(t, y, ':k;mean;', t, yn, '-k;actual;', to, yo,'r.;obs;');
set (h, "linewidth", 2, "markersize",12);
hold on

h = plot(t, zp, '-b', t, [dz -dz]+zp, '--b');
set (h, "linewidth", 2);

h = shadowplot(t, yp, dy);
set ([h.line.t h.line.b], "linestyle","none");
set (h.line.c, "linewidth",2, "color", [0 0.85 0]);
set (h.patch, "facecolor", [0.9 1 0.9])
hold off

axis ([0 T 0 y(1)+abs(sigma*dt*dW(1))]);
xlabel ("time");
set (gca,"ytick",0:0.1:y(1));

filename = "GPfromDS";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));

figure (2);
clf;
subplot(1,2,1)
M = K(t,t,A);
h = imagesc(t,t,M);
colormap(cubehelix)
xlabel ("time");
ylabel ("time");
subplot(1,2,2)
h = plot(t,M(:,round(nT/2)));
set(h,'linewidth',2);
xlabel ("time");
ylabel ("feature");
axis tight


filename = "KernelGPDS";
graphics_toolkit(2,"gnuplot");
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));
