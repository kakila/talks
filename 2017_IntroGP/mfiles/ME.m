## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

clear all

#Simpel mechansitic emulator
u   =@(t,tf) (1 + tanh (50 * t)) .* (1 - tanh (50 * (t - tf))) / 4;
ode =@(x,t,a,tf) -a*x + u(t,tf);

# Replicas
a  = [0.8 1.5 4];
ap = 2.1;
N  = length (a);
y0 = ones(1,N+1);
da = 0.8;
S  =@(x,y) exp (-(x(:)-y(:).').^2/2/da^2);   # coupling


# Covaraince function
function k = K(t,r,a,s)
  m = size (a,2);

  t = t(:);    # first argument is rows
  r = r(:).';  # second argument is columns

  dT = t - r;
  tf = (dT >= 0);
  [nt nr] = size (dT);

  k= [];
  for i=1:m
    tmp      = zeros (size (dT));
    tmp(tf)  = (r.*exp(-a(i,i)*dT))(tf);
    tmp(!tf) = (t.*exp(a(i,i)*dT))(!tf);
    k = blkdiag(k,tmp); # ordering time then coordinate
  endfor
  for i=1:nt
    ii = i+[0:nt:(m-1)*nt];
    k(ii,:) = s * k(ii,:);
  endfor

endfunction

randn ("state", double ("abracadabra"));

# Time vector
nT = 1e2;
T  = 2;
t  = linspace (0, T, nT).';
dt = t(2)-t(1);

# Observation times
n   = 5; # number of osbervations
idx = round(linspace(1,nT,n)).'; %sort (randi ([10 nT-10], n, 1), "ascend");
to  = t(idx);

# Noise
sigma = 0.4;
dW    = randn (nT,N+1);

## Data
A  = diag ([a ap]);                             # Decay
tf = T * 0.65;                                  # End of actuation
s = S([a ap],[a ap]);

#reference trajectories
y = yn = zeros (nT,N+1);        # Determinstic and stochastic solution
y(1,:)   = y0;                # Inital condition
yn(1,:)  = y(1,:) + dt*sigma*dW(1,:);

y = lsode (@(x,t)ode(x,t,A,tf), y(1,:), t); # determinsitc: L^{-1}*u(t)

for i = 1:nT-1                          # stochastic
  yn(i+1,:) = yn(i,:) + dt * (-A*(yn(i,:)).' + u(t(i),tf) + sigma*s*dW(i+1,:).').';
endfor

# observations
yo  = yn(idx,1:N);
dyo = yo - y(idx,1:N);

# Predictions over observed grid
Koo = sigma^2*K (to,to,A,s);

ii = 1:N*n;
jj = ii(end)+(1:n);
KNNo = Koo(ii,ii);
K1No = Koo(jj,ii);
K11o = Koo(jj,jj);

l   = sqrt(eps) + 0;           #regularization parameter

Kivo = cholinv (KNNo + l * eye (size (KNNo)));

ypo  = K1No * Kivo * dyo(:) + y(idx,N+1);  # predicted observations
vyo  = diag (K11o - K1No * Kivo * K1No.');           # variance

figure (1);
clf;
h = plot(t, y, ':k', t, yn, '-k', to, yo,'r.');
set (h, "linewidth", 2, "markersize",12);
legend (h(1:(N+1):end),{"mean","actual","obs"})
hold on

h = plot(to, ypo,'g.;pred;');
set (h, "linewidth", 2, "markersize",12);
hold off

#%axis ([0 T 0 y(1)+abs(sigma*dt*dW(1))]);
axis tight
xlabel ("time");
set (gca,"ytick",0:0.1:y(1));

[~, ii] = min(abs(t-0.25));
dx = t(ii+1)-t(ii);
dy = y(ii+1,:)-y(ii,:);
nh = [dy;dx*ones(1,N+1)].*[-1; 1]; nh ./= sqrt(sumsq(nh));
d = linspace (0.09, 0.02, N+1);
xt = 0.25 + d .* nh(1,:);
yt = yn(ii,:) + d .* nh(2,:);
for i=1:N+1
  text (xt(i),yt(i),num2str(diag(A)(i)));
endfor

filename = "MEfromDS";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));
