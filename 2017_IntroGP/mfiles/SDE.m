## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

#Example of linear SDE
u   =@(t,tf) (1 + tanh (50 * t)) .* (1 - tanh (50 * (t - tf))) / 4;
ode =@(x,t,a,tf) -a*x + u(t,tf);

randn ("state", double ("abracadabra"));

# Time vector
nT = 1e2;
T  = 2;
t  = linspace (0, T, nT).';
dt = t(2)-t(1);

N = 100;
# Noise
sigma = 1;
dW    = randn (nT,N);

## Data
A  = 3;                    # Decay
tf = 1;                # End of actuation

#reference trajectories
y = yn = zeros (nT,N);        # Determinstic and stochastic solution
y(1,:)   = 0.6;                     # Inital condition
yn(1,:)  = y(1,:) + dt*sigma*dW(1,:);

y = lsode (@(x,t)ode(x,t,A,tf), y(1,:), t); # determinsitc: L^{-1}*u(t)

for i = 1:nT-1                          # stochastic
  yn(i+1,:) = yn(i,:) + dt * (-A*yn(i,:) + u(t(i),tf) + sigma*dW(i+1,:));
endfor

figure (1);
clf;
h = plot(t, yn, '-',t,y,'-k');
set (h(1:end-1), "color", [0.8 0.8 0.8], "linewidth", 1);
set (h(end), "linewidth", 2);

axis tight;
xlabel ("time");

filename = "SDE";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));
