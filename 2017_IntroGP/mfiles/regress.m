## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


## Underdetermined polynomial regression
# Solving an interpolation problem using the pseudoinverse explicitly.
t   = sort ([0.1 0.3 0.5 0.9]).';
n   = length (t);                      # number of given inputs
pp  = [2 -0.5];
y   = polyval (pp, t) + 0.5 * randn (n,1); # observations
deg = n;
phi =@(x) x.^[deg:-1:0];

Phi  = phi(t);
N    = size (Phi, 2);                      # Number of l.i. functions
nPhi = null (Phi);
ndim = N - n;                              # dimension of nullspace
assert (all (ndim == [N-rank(Phi) size(nPhi,2)]));

w   = Phi \ y;
np  = 100;
tp  = linspace (t(1)-0.05*abs(t(end)), t(end)+0.05*abs(t(end)), np).';
yp  = phi(tp) * w;
ypn = phi(tp) * (w + nPhi * 100 * (2 * rand (ndim, np) - 1) );

## Plot of direct solution
#
figure (1);
clf;
h = plot(t,y,'ob', tp, ypn, '-r', tp, yp,'-k');
set (h(1), "markersize", 9, 'markerfacecolor', 'b');
axis tight;
set(h(2:end),"visible",false);
xlabel ("t");
ylabel ("y")
grid on

## Export PDFs
#
filename = "regress1";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));

set(h(end),"visible",true,'linewidth', 3);
filename = "regress2";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));

set(h(2:end-1),"visible",true);
filename = "regress3";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));

  
## GP interpolation
#
pkg load gpml
cf = {@covPoly,'iso',3};
lf = @likGauss;
hyp.lik=-7;

nc = 25;
hc = linspace (-3,2,nc);
ym = zeros (np,nc);
for i=1:nc
  hyp.cov=[hc(i);0;-0];
  #prior.lik = {@priorClamped};
  #infe = {@infPrior, @infExact, prior};
  #hyp = minimize(hyp,@gp,-100,infe,[],cf,lf,t,y);
  [ym(:,i) dy2] = gp (hyp,@infExact,[],cf,lf,t,y,tp);
endfor
figure(1)
clf
#[yo o] = sort (ym, 'ascend');
#plg   = [tp(o) yo-sqrt(dy2(o));flipud(tp(o)) flipud(yo+sqrt(dy2(o)))];
#patch (plg(:,1),plg(:,2), 0.8*ones(1,3), 'edgecolor', 'none')
#hold on
h = plot(t,y,'ob', tp, ypn, '-r', tp, yp,'-k',tp,ym,'-g');
set (h(1), "markersize", 9, 'markerfacecolor', 'b');
set(h(end-nc),'linewidth', 3);
axis tight;
xlabel ("t");
ylabel ("y")
grid on

filename = "regress4";
print('-dsvg',[filename '.svg']);
unix (sprintf('inkscape --without-gui --vacuum-defs --verb=FitCanvasToDrawing --verb=FileSave --verb=FileClose -l %s.svg',filename));
unix (sprintf('inkscape --without-gui --file="%s.svg" --export-pdf="%s.pdf"',filename,filename));

## Move files to img folder
#
movefile ('*.pdf', '../../img/');
