\documentclass{beamer}

% --- use packages
\usepackage[ansinew]{inputenc}
\usepackage{textcomp}
\usepackage{lmodern} % German related symbols
\usepackage{csquotes}
\usepackage[T1]{fontenc}
\usepackage[USenglish]{babel}		%

\usepackage{graphicx} 			% to include figures

\usepackage{amsmath}    %formulas matematicas lindas
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}

\usepackage{booktabs}                   % nice formated tables
\usepackage{tabularx}			% for tables

\usepackage{hyperref}

%\usepackage{multimedia}		%include video or sound files

\usepackage{ellipsis}			% correct [...] with \dots

% Plots
\usepackage{pgfplots}
\pgfplotsset{every axis/.append style={line width=1pt}}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@online{Osgood07,
  author = {Brad Osgood},
  title = {The Fourier Transform And Its Applications},
  year = 2007,
  url = {https://see.stanford.edu/Course/EE261},
  urldate = {2017-05-21}
}
@online{3blue1brown,
  author = {3Blue1Brown},
  title = {Essence of linear algebra},
  year = 2016,
  url = {https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab},
  urldate = {2017-05-21}
}
@book{Courant1989,
  author = {R. Courant and D. Hilbert},
  volume = 1,
  publisher = {John Wiley \& Sons, Inc.},
  year = 1989,
  title = {Methods of Mathematical Physics}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

\graphicspath{{img/}}

% Customs commmands
% Calculus
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\rot}[1]{\nabla \times {#1}}
\newcommand{\diver}[1]{\nabla \cdot {#1}}
\newcommand{\definter}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\inter}[2]{\int {#1}\ud {#2}}
\newcommand{\braket}[2]{\langle {#1} , {#2} \rangle}

% Misc
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{#1}\!\times\!{#2}}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}
\newcommand{\source}[1]{{\tiny source:\thinspace{\itshape {#1}}}}

% Operators
\DeclareMathOperator*{\err}{err}
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\linspan}{span}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\sinc}{sinc}



% Logicals
\newcommand{\suchthat}{\big \backslash \;}
% Comments
\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}
% Line
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% -----------
% Set Eawag style

%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{From signals to vectors and back again}
\subtitle{A primer on input compression}

\author[JPi Carbajal]{Juan Pablo Carbajal\\\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science
  and Technology}

\date{May 22, 2017}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load backgound for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load backgound for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page


%----------------
\section{Motivation for the talk}

\begin{frame}[t]{Motivation}
  \only<1>{
    Given a continuous signal (and maybe more regular), i.e. described with $\infty$ point values.
    Can we compress it? i.e. describe it with a set of finite values.
  }
  \only<2>{
    A usual representation uses samples of the signal.
    The signal $f(t)$ with $\infty$ values becomes a $N$-dimensional vector
    \begin{equation*}
      \bm{\mathrm{f}} = \begin{bmatrix} f(t_1) & f(t_2) & \ldots & f(t_N) \end{bmatrix}^\trp
    \end{equation*}
  }
  \begin{figure}[b]
    \centering
    \begin{tikzpicture}
      \begin{axis}[
                xmax=360,ymax=1,
                axis lines=middle,
                xticklabels=\empty,
                xlabel={$t$},
                ylabel={$f(t)$},
                enlarge y limits = true,
                enlarge x limits = true,
                height=0.6\textheight,
                samples=100,
                ]
      \only<1>{
        \addplot[blue, domain=0:360]
        {0.3*sin(x) - 0.1*sin(9.7*x) + 0.1*sin(4.3*x) - 0.3*cos(1.1*x) + 0.2*cos(7*x)};
      }
      \only<2>{
        \addplot[blue!20!white, dashed, domain=0:360]
        {0.3*sin(x) - 0.1*sin(9.7*x) + 0.1*sin(4.3*x) - 0.3*cos(1.1*x) + 0.2*cos(7*x)};
        \addplot[red, samples=20, domain=0:360, only marks, mark=o]
        {0.3*sin(x) - 0.1*sin(9.7*x) + 0.1*sin(4.3*x) - 0.3*cos(1.1*x) + 0.2*cos(7*x)};
      }
      \end{axis}
    \end{tikzpicture}
  \end{figure}
\end{frame}

%----------------
\section{Examples}

\begin{frame}[t]{The sampling theorem}

  \only<1>{
    \begin{theorem}[Nyquist-Shannon sampling theorem]
      If a function $f(t)$ contains no frequencies higher or equal to $\frac{\mathtt{f}_{\text{nyq}}}{2}$ hertz,
      it is completely determined by giving its values at a set of $\bset{t_i}$ points spaced $\Delta t = \frac{1}{\mathtt{f}_{\text{nyq}}}$ seconds apart.
    \end{theorem}

    \begin{figure}[b]
      \centering
      \begin{tikzpicture}
        \begin{axis}[
                  xmin=-1,xmax=1,ymax=0.6,
                  axis lines=middle,
                  xtick={-1,1},
                  xticklabels={-$\frac{\mathtt{f}_{\text{nyq}}}{2}$,$\frac{\mathtt{f}_{\text{nyq}}}{2}$},
                  yticklabels=\empty,
                  xlabel={$\mathtt{f}$},
                  ylabel={$\Vert F(f)(\mathtt{f})\Vert$},
                  enlarge y limits = true,
                  enlarge x limits = true,
                  height=0.4\textheight,
                  samples=100,
                  ]
        \addplot[blue, domain=-1:1]
        {(1-x)*(x+1)*(0.55+0.1*abs(sin(360*2*x))+0.2*sin(360*abs(x))-0.5*cos(360*0.5*x))};
        \end{axis}
      \end{tikzpicture}
    \end{figure}
  }

  \begin{alertblock}{\dots}
     How to reconstruct the signal?
  \end{alertblock}

  \only<2>{
    We use a set of functions
    \begin{align*}
      \varphi_i(t) &= \sinc\left(\mathtt{f}_{\text{nyq}} \left(t - t_i\right)\right)\\
      f(t) &= \sum_{i=1}^{N} f(t_i) \varphi_i(t) = \bm{\Phi}(t)\bm{\mathrm{f}}
    \end{align*}
    This grants \alert{perfect recovery}! All unseen points are perfectly interpolated (see Osgood's 17th lecture\footfullcite{Osgood07}).

    Check \texttt{s\_badlimit\_recovery.m} for an example.
  }

  \only<3>{
    \begin{equation*}
      f(t) = \sum_{i=1}^{N} f(t_i) \sinc\left(\mathtt{f}_{\text{nyq}} \left(t - t_i\right)\right)
    \end{equation*}
    \begin{figure}[b]
      \centering
      \begin{tikzpicture}[declare function={ sinc(\x) = sin(\x)/(pi * \x /180);},]
        \begin{axis}[
                  grid=both,
                  minor tick num=5,
                  grid style={line width=.1pt, draw=gray!30},
                  xmin=-36,xmax=108,
                  axis lines=middle,
                  xticklabels=\empty,yticklabels=\empty,
                  xlabel={$t$},
                  ylabel={$f(t)$},
                  enlarge y limits = true,
                  enlarge x limits = true,
                  height=0.5\textheight,
                  samples=100,
                  ]
        \addplot[red!20, domain=-36:108] {0.1*sinc(5*x)};
        \addplot[blue!20, domain=-36:108] {0.8*sinc(5*(x-36))};
        \addplot[green!40!gray!50, domain=-36:108] {0.3*sinc(5*(x-72))};
        \addplot[yellow!50!gray!50, domain=-36:108] {sinc(5*(x-108))};
        \addplot[only marks, color=black,mark=o] coordinates {
          (0,0.1)
          (36,0.8)
          (72,0.3)
          (108,1)
          };
        \addplot[red, domain=-36:108] {0.1*sinc(5*x) + 0.8*sinc(5*(x-36)) + 0.3*sinc(5*(x-72)) + sinc(5*(x-108))};
        \end{axis}
      \end{tikzpicture}
    \end{figure}
  }

\end{frame}

\begin{frame}[t]{Polynomial interpolation}
    \only<1>{
      Zeroth  order hold, a.k.a. piece-wise constant
      \begin{equation*}
        \varphi_i(t) = \begin{cases} 1 & \text{if } t_i \leq t < t_{i+1} \\
                                     0 & \text{otherwise}
                       \end{cases}
      \end{equation*}
    }
    \only<2>{
      Linear interpolation
      \begin{equation*}
        \varphi_i(t) = \begin{cases}
          \frac{t - t_{i-1}}{t_{i}-t_{i-1}} & \text{if } t_i \leq t \leq 0 \\
          \frac{t_{i+1} - t}{t_{i+1}-t_{i}} & \text{if } t_i < t < t_{i+1} \\
          0 & \text{otherwise}
        \end{cases}
      \end{equation*}
    }
    \begin{figure}[b]
      \centering
      \begin{tikzpicture}[
      declare function={
                        phi0(\x,\y) = and(\x > 0, \x <= \y);
                        phi1(\x,\y) = and(\x > -\y, \x <= 0) * (\x + \y) / \y +
                                      and(\x > 0, \x < \y) * (\y - \x) / \y;
                      },]
        \begin{axis}[
                  grid=both,
                  minor tick num=5,
                  grid style={line width=.1pt, draw=gray!30},
                  xmin=0,xmax=0.75,
                  axis lines=middle,
                  xticklabels=\empty,yticklabels=\empty,
                  xlabel={$t$},
                  ylabel={$f(t)$},
                  enlarge y limits = true,
                  enlarge x limits = true,
                  height=0.5\textheight,
                  samples=100,
                  domain=0:0.76,
                  ]
        \only<1>{
          \addplot[red!20] {0.1*phi0(x,0.25)};
          \addplot[blue!20] {0.8*phi0(x-0.25,0.25))};
          \addplot[green!40!gray!50] {0.3*phi0(x-0.5,0.25))};
          \addplot[yellow!40!gray!50] {phi0(x-0.75,0.25))};
          \addplot[red, dashed] {0.1*phi0(x,0.25) + 0.8*phi0(x-0.25,0.25)) + 0.3*phi0(x-0.5,0.25)) + phi0(x-0.75,0.25))};
        }
        \only<2>{
          \addplot[red!20] {0.1*phi1(x,0.25)};
          \addplot[blue!20] {0.8*phi1(x-0.25,0.25))};
          \addplot[green!40!gray!50] {0.3*phi1(x-0.5,0.25))};
          \addplot[yellow!40!gray!50] {phi1(x-0.75,0.25))};
          \addplot[red] {0.1*phi1(x,0.25) + 0.8*phi1(x-0.25,0.25)) + 0.3*phi1(x-0.5,0.25)) + phi1(x-0.75,0.25))};

        }
        \addplot[only marks, color=black,mark=o] coordinates {
          (0,0.1)
          (0.25,0.8)
          (0.5,0.3)
          (0.75,1)
          };
        \end{axis}
      \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}[t]{Fourier series}

  The vector of values $\bm{\mathrm{f}}$ is not anymore a sample of $f(t)$ (band-limited).

  \begin{align*}
    f(t) &= \sum_{i=1}^{N} \hat{f}(\mathtt{f}_i) \sin (2\pi\mathtt{f}_it + \phi_i) \\
    \bm{\mathrm{f}} &= \begin{bmatrix} \hat{f}(\mathtt{f}_1) & \hat{f}(\mathtt{f}_2) & \ldots & \hat{f}(\mathtt{f}_N) \end{bmatrix}^\trp \; \text{(DFT)}\\
    \varphi_i(t) &= \sin (2\pi\mathtt{f}_it + \phi_i)
  \end{align*}

\end{frame}

\begin{frame}[t]{Orthogonal polynomials}

  \begin{block}{More general \dots}
    Series expansion of an arbitrary function in terms of a given set of functions
  \end{block}
  (see part II of~\cite{Courant1989}\footfullcite{Courant1989} (original is 1953 in German), old but almost no jargon)
  \begin{equation*}
    f(t) = \sum_{n=1}^{\infty} c_n P_n(t)
  \end{equation*}
  When $c_n$ decrease with $n$, can be truncated up to $N$ terms.

  \begin{exampleblock}{Example: Legendre polynomials}
    \begin{equation*}
      P_n(t) = \frac{1}{2^n n!}\tder{^n}{t^n}\left[\left(x^2 - 1\right)^n\right]
    \end{equation*}
    \vspace{-1em}
  \end{exampleblock}

\end{frame}

\begin{frame}[t]{Gaussian process regression}
    The mean of the posterior distribution (solution to the regression problem) is:
    \begin{equation*}
      f(t) = k(t,T) \; \overbrace{\left[\bm{K} + \sigma \bm{I} \right]^{-1} \left[ f(T) - m(T) \right]}^ {\bm{\mathrm{f}}} \; + \; m(t)
    \end{equation*}

    Which can be organized as follows:
    \begin{eqnarray*}
      f(t) = \sum_{i=1}^{N+1} \mathrm{f}_i \varphi_i(t) &\\
      \varphi_i(t) = k(t,t_i) & \text{rows of cov matrix}\\
      \varphi_{N+1}(t) = m(t) & \text{mean function}\\
      \bm{\mathrm{f}} = \left[\bm{K} + \sigma \bm{I} \right]^{-1} \left[ f(T) - m(T) \right] & \text{learned weights}\\
      \mathrm{f}_{N+1} = 1 &
    \end{eqnarray*}

\end{frame}

\begin{frame}
The list of examples is much larger, to mention a few more important cases:
\begin{itemize}
\item Finite Element method (\textbf{FEM}): solutions to PDEs are approximated by projections on function spaces.
\item Compressed sensing (\textbf{CS}): similar to the sampling theorem but reduces the amount of sampling by considering sparse representations. See \emph{basis pursuit}, \emph{matching pursuit}, \emph{sparse approximation}, \emph{sparse encoding}, \emph{PCA/POD}
\item Wavelets: like Fourier transfrom, but in time as well as frequency. See \emph{spectrogram}.
\end{itemize}

\end{frame}
\begin{frame}[t]{Intermediate summary}

  The case is made for the signal representation:
  \begin{alertblock}{Function approximation}
    \vspace{-1em}
    \begin{align*}
      f(t) \approx \sum_{i=1}^{N} \mathrm{f}_i \varphi_i(t) &= \bm{\Phi}(t)\bm{\mathrm{f}}\\
      \bm{\Phi}(T) &\in \mathbb{R}^{\stimes{|T|}{N}}\\
      \bm{\mathrm{f}} &\in \mathbb{R}^{N}
    \end{align*}
    \vspace{-2em}
  \end{alertblock}
  It is \emph{pervasive}!

  The vector $\bm{\mathrm{f}}$ represents the signal in an $N$-dimensional vector space.

  Smaller $N$ $\rightarrow$ more compression.

\end{frame}

% ----------------
\section{Abstract vector spaces}

\begin{frame}{Abstract vector spaces}
  \onslide<1->{How do you plot a vector like: $\begin{bmatrix}a\\b\end{bmatrix}$ }
  \onslide<2->{$\begin{bmatrix}a\\b\\c\end{bmatrix}$ }
  \onslide<3>{$\begin{bmatrix}a\\b\\\vdots\\z\end{bmatrix}$ }
  \vspace{1em}

  For an intuitive introduction using properties of functions, see chapter 11 of: \fullcite{3blue1brown}
\end{frame}

\begin{frame}[t]{From vectors to functions}
  $\bm{\mathrm{v}} = \begin{bmatrix}\mathrm{v}_1 & \mathrm{v}_2 & \ldots & \mathrm{v}_n & \ldots\end{bmatrix}$
  \begin{figure}[b]
  \centering
  \begin{tikzpicture}
    \begin{axis}[
              grid=both,
              minor tick num=5,
              grid style={line width=.1pt, draw=gray!30},
              xmin=0,xmax=10,
              axis lines=middle,
              xticklabels=\empty,yticklabels=\empty,
              xlabel={$n$},
              ylabel={$\mathrm{v}_n$},
              enlarge y limits = true,
              enlarge x limits = true,
              height=0.5\textheight,
              mark size = 0.5pt,
              mark= *,
              ]
      \addplot[only marks, black] coordinates {
        (0,0.752)
        (1,-0.0952)
        (2,2.92)
        (3,1.25)
        (4,0.486)
        (5,-0.816)
        (6,-0.1)
        (7,-0.924)
        (8,-0.938)
        (9,0.161)
        (10,-0.29)
        };
     \only<2->{
      \addplot[only marks, black] coordinates {
        (0.5,-0.966)
        (1.5,1.74)
        (2.5,2.38)
        (3.5,0.784)
        (4.5,-0.269)
        (5.5,-0.51)
        (6.5,-0.377)
        (7.5,-1.14)
        (8.5,-0.392)
        (9.5,0.327)
        };
      }
    \only<3->{
      \addplot[only marks, black] coordinates {
        (0.25,-0.532)
        (0.75,-0.753)
        (1.25,0.804)
        (1.75,2.52)
        (2.25,2.83)
        (2.75,1.78)
        (3.25,0.936)
        (3.75,0.674)
        (4.25,0.144)
        (4.75,-0.63)
        (5.25,-0.744)
        (5.75,-0.25)
        (6.25,-0.16)
        (6.75,-0.662)
        (7.25,-1.09)
        (7.75,-1.09)
        (8.25,-0.691)
        (8.75,-0.0917)
        (9.25,0.317)
        (9.75,0.141)
        };
      }
      \end{axis}
    \end{tikzpicture}
  \end{figure}

  \only<3>{Intuition: continuing ad infinitum, we get the graph of a function.}
\end{frame}

\begin{frame}[t]{Leaps of faith}
  Lets generalize!
  \begin{align*}
    \bm{\mathrm{f}} \cdot \bm{\mathrm{g}} &= \sum_{i=1}^{N} \mathrm{f}_i\mathrm{g}_i
    \onslide<2>{\\\braket{f(t)}{g(t)} &= \definter{0}{T}{f(\tau)g(\tau)}{\tau}}
  \end{align*}

  \begin{eqnarray*}
    \bm{\mathrm{f}} \perp \bm{\mathrm{g}} &\rightarrow& \bm{\mathrm{f}} \cdot \bm{\mathrm{g}} = 0 \qquad\qquad \text{orthogonal vectors}
    \onslide<2>{\\f(t) \perp g(t) &\rightarrow& \braket{f(t)}{g(t)} = 0 \quad \text{orthogonal functions}}
  \end{eqnarray*}

  \only<2>{There are many more inner products for function spaces\dots your minds (and the properties of inner products) are the limit!}
\end{frame}

% ----------------
\section{Conclusion}

\begin{frame}[t]{Conclusions}
  \begin{enumerate}
  \item\label{it:vectorsp} $f(t) = \bm{\Phi}(t)\bm{\mathrm{f}}$, when finite (or truncated), provides function $\rightarrow$  finite dimensional vector (linear algebra!)
  \item\label{it:hilbert} Their properties\footfullcite{3blue1brown} make functions (infinite dimensional) vectors. Linear algebra can be generalized! (Hilbert spaces).
  \end{enumerate}

  \begin{alertblock}{Signal compression}
  A function (in~\ref{it:hilbert}.) can been seen as a finite dimensional vector (in~\ref{it:vectorsp}.). Fewer dimensions $\rightarrow$ more compression.
  \end{alertblock}

  The compressibility of a signal is limited by its regularity (``smoothness''), i.e. by the complexity of the process generating it.

\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}
\end{document}
