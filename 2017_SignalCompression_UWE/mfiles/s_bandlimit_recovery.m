# Copyright (C) 2017 - Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
#
# Author: Juan Pablo Carbajal ajuanpi+dev@gmail.com
#

## The Nyquist-Shannon sampling theorem
#
# If a function $f(t)$ contains no frequencies higher or equal to
# $\frac{\mathtt{f}_{\text{nyq}}}{2}$ hertz (band-limited), it is completely
# determined by giving its values at a set of $\lbrace t_i\rbrace$ points spaced
# $\Delta t = \frac{1}{\mathtt{f}_{\text{nyq}}}$ seconds apart.
#
# Here we show a simple example demonstrating perfect interpolation using the
# <octave:sinc sinc> function.
#

## Band-limited function
#
# We create a band-limited function by filtering Brownian motion.
# There are more ways to create a band-limited signal, choose your favorite.
# We use a Butterworth low-pass filter filter to limit the frequency content of
# the signal.
#

##
# Create Brownian motion in an interval |T| using function <octave:wienrnd wienrnd>.
# We call this the seed signal.
#
T     = 2;                        # Time interval of observation [seconds]
seed  = wienrnd (T);
t     = seed(:,1);                # 1st column is the time vector
nT    = length (t);
seed  = seed(:,2);                # 2nd column is the signal
seed -= mean (seed);              # Remove mean
fc    = 25;                       # Cut-off frequency for filter [Hz]
fs    = 1 / (t(2)-t(1));          # Sampling frequency [Hz]
printf ("Sampling frequency: %.1f [Hz]\nCut-off frequency: %.1f [Hz]\n", fs, fc);

##
#
# We filter the seed signal to create the data singal, using the function
# <https://octave.sourceforge.io/signal/function/butter.html butter> from the
# <https://octave.sourceforge.io/signal/index.html signal package>.
#
# If you do not have it installed run |pkg install -forge signal|
# and follow the instructions.
#
pkg load signal
[b a] = butter (5, fc / (fs / 2));
data  = filter (b, a, seed);

## Power spectrum of band-limited real signal
#
# We calculate and plot the power spectrum of the signal. The spectrum is
# symmetric (real signal) and it is practically zero beyond the cut-off
# frequency |fc| of the filter. This implies that if we sample the signal at |2*fc| we will
# get an almost exact recovery.
#
NFFT  = 2 ^ nextpow2(nT);
Fdata = fftshift (fft (data, NFFT) / nT);     # Fast Fourier transform
freq  = (fs / 2) .* linspace (-1, 1, NFFT).'; # Frequency vector

figure(1)
plot(freq, abs (Fdata))
%grid on
axis tight
v = axis ();
axis ([[-fc fc]*2 v(3:4)])
line ([-fc fc; -fc fc], [v([3 3]); v([4 4])]);
set (gca, "xtick",-fc*2:10:fc*2)
xlabel ("Frequency [Hz]")
ylabel ("||F(f)||")

## Sampled signal
#
# The signal is now subsampled by selecting a subset of values.
#
i = 1;
for k=15:15:45
  idx = 1:k:nT;
  sdata{i} = data (idx);                # subsampled signal
  ts{i}    = t(idx);                    # subsampled time vector
  fss(i)   = 1 / (ts{i}(2)-ts{i}(1));   # sampling frequency
  i++;
endfor
nfss =  length (fss);
fmt  = repmat ("%.1f ", 1, nfss);
printf (
 ["Sampling frequency: " fmt " [Hz]\nPerfect recovery frequency: %.1f [Hz]\n"],
       fss, 2*fc);

## Reconstruction of the signal
#
# The reconstruction of the signal is obtained by linearly combining a set
# of translated <octave:sinc sinc> functions scaled by the sampled values of the
# signal.
#
# $$ f(t) = \sum_{i=1}^{N} f(t_i) \operatorname{sinc}\left[\mathtt{f}_s \left(t - t_i \right)\right] $$
#
# For a very nice introduction and demonstration see
# <https://see.stanford.edu/Course/EE261 Lecture 17> of:
#
#   Brad Osgood (2007). The Fourier Transform And Its Applications.
#   https://see.stanford.edu/Course/EE261 (visited on 05/21/2017).
#
# The following plot shows the graph of the <octave:sinc sinc> function
# which forms th ebasis for inter/intrapolation.
#
figure(2)
clf
plot (t-mean(t), sinc(20*(t-mean(t))), "linewidth", 2);
axis tight
xlabel ('Time [s]')
ylabel ('sinc')
grid on

##
#
# Next we proceed to plot the recovered signal and the error
#
figure(3)
clf
ax1 = subplot (2,1,1);
plot(t, data,'-k;signal;')
legend ('Location','NorthEastOutside');
hold on
ax2 = subplot (2,1,2);
hold on
for i=1:nfss
  rec  = sinc (fss(i) * (t - ts{i}.')) * sdata{i};           # recovered signal
  h = plot(ax1, ...
       %ts{i}, sdata{i}, sprintf ("o;sampled@%.1f;",fss(i)), ...
       t,     rec,      sprintf (".;recovered@%.1f;",fss(i)));
  cc  = get(h(1),'color');
%  set (h(2), 'color', cc);
  h = plot(ax2, t(1:end-15), (data - rec)(1:end-15), ...
           sprintf ("-;recovered@%.1f;",fss(i)), 'color', cc);
  err(i) = sqrt(sumsq (data - rec) / nT);
endfor
axes (ax1)
hold off
axis tight
ylabel ("Signal")
xlabel ("Time [s]")
axes (ax2)
hold off
axis tight
ylabel ("Error")
xlabel ("Time [s]")
legend ('Location','NorthEastOutside');

fmt = repmat ("%.3g@%.1f ", 1, nfss);
printf (["Reconstruction error (L2): " fmt "\n"], [err;fss](:));
