\documentclass[xcolor=dvipsnames]{beamer}

\input{../includes/beamer_basics.tex}

%% --- use packages
\usepackage{abraces,mathtools}
\usetikzlibrary{patterns}
% for notes with pdfpc
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
\setbeameroption{hide notes}

%% Math symbols
\input{../includes/mathsymbols.tex}
\newcommand{\sm}{\scalebox{0.5}{-1}}
\newcommand{\ccov}[1]{{\color{red}k}\left(#1\right)}
\newcommand{\cmean}[1]{{\color{blue}m}\left(#1\right)}
\newcommand{\cgreen}[1]{{\color{red}G}\left(#1\right)}
\newcommand{\cnull}[1]{{\color{blue}n}\left(#1\right)}

\DeclareMathOperator*{\GP}{\mathcal{G}\!\mathcal{P}} % Gaussian process

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@article{Poggio1990,
author = {Poggio, Tomaso and Girosi, F.},
doi = {10.1109/5.58326},
issn = {00189219},
journal = {Proceedings of the IEEE},
number = {9},
pages = {1481--1497},
title = {{Networks for approximation and learning}},
volume = {78},
year = {1990}
}
@article{Steinke2008,
author = {Steinke, Florian and Sch\"olkopf, Bernhard},
doi = {10.1016/j.patcog.2008.06.011},
issn = {00313203},
journal = {Pattern Recognition},
month = {11},
number = {11},
pages = {3271--3286},
title = {{Kernels, regularization and differential equations}},
volume = {41},
year = {2008}
}
@article{Sarkka2013,
author = {S\"arkk\"a, Simo and Solin, Arno and Hartikainen, Jouni},
doi = {10.1109/MSP.2013.2246292},
issn = {1053-5888},
journal = {IEEE Signal Processing Magazine},
month = {7},
number = {4},
pages = {51--61},
title = {{Spatiotemporal Learning via Infinite-Dimensional Bayesian Filtering and Smoothing: A Look at Gaussian Process Regression Through Kalman Filtering}},
volume = {30},
year = {2013}
}
@article{Nickisch2018,
archivePrefix = {arXiv},
arxivId = {1802.04846},
author = {Nickisch, Hannes and Solin, Arno and Grigorievskiy, Alexander},
eprint = {1802.04846},
month = {2},
title = {{State Space Gaussian Processes with Non-Gaussian Likelihood}},
url = {http://arxiv.org/abs/1802.04846},
year = {2018}
}
@article{OHagan78,
author = {O'Hagan, Anthony and Kingman, JFC},
doi = {10.2307/2984861},
issn = {00359246},
journal = {Journal of the Royal Statistical Society. Series B (Methodological)},
number = {1},
pages = {1--42},
title = {{Curve Fitting and Optimal Design for Prediction}},
volume = {40},
year = {1978}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% -----------
% Set Eawag style
%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{A walk through the intersection between machine learning and mechanistic modeling}

\author[JPi Carbajal]{Juan Pablo Carbajal
\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{Data Science Seminar, EURECOM, France. May 3, 2018}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load backgound for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load backgound for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

%----------------
\section{Speaker's profile}
\begin{frame}
  \frametitle{Background}
  \centering
  \includegraphics[width=\textwidth]{intro.png}
\end{frame}

\begin{frame}[t]{Machine Learning and Mechanistic modeling}
  \centering
    \begin{tikzpicture}[scale=0.9]
      \draw (0,0) circle (2cm) [fill=blue!50] node [above left, text width=4em, xshift=0.3cm]{Machine Learning};
      \draw (-60:1.0cm) circle (1.0cm) [fill=green!50] node {Emulation};
      \draw (0:2.5cm) circle (2cm) [fill=red, fill opacity=0.5] node [right, text width=5em, fill opacity=1,xshift=-0.3cm] {Mechanistic modeling};
    \end{tikzpicture}

    \vspace{1em}
    \begin{columns}[t]
      \column{0.35\textwidth}
        {\color{blue} Machine Learning}
        {\scriptsize
        \begin{itemize}
          \item Discover patterns from data (prediction)
          \item Minimal processes knowledge
        \end{itemize}
        }
      \column{0.35\textwidth}
        {\color{green} Emulation}
        {\scriptsize
        \begin{itemize}
          \item Discover "useful" approximation of numerical model from data
          \item Partial/Fuzzy (sub)processes knowledge
        \end{itemize}
        }
      \column{0.35\textwidth}
        {\color{red} Mechanistic modeling}
        {\scriptsize
        \begin{itemize}
          \item Interpret data/model using model/data
          \item Detailed (sub)processes knowledge
        \end{itemize}
        }
    \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{More motivation}
\begin{frame}{Environemental science and engineering}
  \framesubtitle{Model calibration, Fast (extreme) event prediction, \& Design}
  \centering
  \includegraphics[width=0.8\textwidth]{flood_bridge.jpg}

  \source{https://www.dailyrecord.co.uk/news/scottish-news/storm-frank-rescuers-brave-storm-7096067}
\end{frame}

\begin{frame}{Engineered complex systems}
  \framesubtitle{Optimization, Control, \& Maintenance}
  \centering
  \includegraphics[width=0.8\textwidth]{WindFarm_Fluvanna_2004.jpg}

  \source{Leaflet, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=5704247}}
\end{frame}

\begin{frame}{Data fusion/interpretation}
  \centering
  \includegraphics[height=0.4\textheight]
  {PDESmoothing_2_Azzimonti.png}
  \includegraphics[height=0.3\textheight]
  {PDESmoothing_1_Azzimonti.png}\\
  \source{Azzimonti et al. 10.1080/01621459.2014.946036}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{What is emulation?}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{What is emulation?}}\\
    \Large{How is emulation even possible?}
  \end{frame}
  }

  \begin{frame}{Alien researcher}
    \centering
    \begin{columns}[T]
      \column{.5\textwidth}
        \includegraphics[height=0.7\textheight]{molecule_spring.png}
      \column{.5\textwidth}
        \includegraphics[height=0.6\textheight]{HookesLaw.png}\\
        \source{Svjo, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=25795521}}
    \end{columns}
  \end{frame}

  \begin{frame}[t]{From molecules to fluids}
    \centering
    \only<1>
    {
      \href{run:../img/Pseudofluids_Chipmunk.avi?autostart&loop&noprogress}%
       {\includegraphics[height=0.8\textheight]{pseudofluids.png}}
    }
    \only<2>
    {
      \includegraphics[height=0.75\textheight]{coarse_graining.png}\\
      \source{doi:10.1088/0965-0393/12/6/R01}
    }
  \end{frame}

  \begin{frame}{Emulation}
  \framesubtitle{many names \& similarities ...}
    \begin{tikzpicture}[overlay,remember picture,shift=(current page.north west)]
      \pgfmathsetseed{4}
      \foreach [count=\count] \word in {
                                        Surrogate modeling,
                                        Metamodeling,
                                        Coarse graining,
                                        Model order reduction,
                                        Inter-polation,
                                        Phenom. modeling,
                                        Extra-polation,
                                        Response surface,
                                        Empiric. modeling
                                       }
      {
        \pgfmathparse{round(70+20*rand)}
        \edef\tr{\pgfmathresult}
        \pgfmathparse{round(50+25*rand)}
        \edef\tg{\pgfmathresult}
        \pgfmathparse{round(50+50*rand)}
        \edef\tb{\pgfmathresult}
        \node [
            anchor=west,
            text width=2.5cm,
            xshift=1.5cm,
            yshift=-3cm,
            xshift={mod(\count-1,3)*(\textwidth-2cm)/2},
            yshift={floor((\count-1)/3)*(-(0.9\textheight-4cm))/2},
            xshift=rand*2cm,
            yshift=rand*1cm,
            rotate=rand*15,
            text={rgb:red,\tr;green,\tg;blue,\tb},
            font=\bfseries\large] {\word};
      }
    \end{tikzpicture}

  \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Types of models}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{Types of simulators}}
  \end{frame}
  }

  \input{../includes/models.tex}

  \begin{frame}{Combination of models}
    \framesubtitle{Dimensional heterogeneous models}

    \begin{columns}[c]
      \column{0.5\textwidth}
        \centering
        \includegraphics[height=0.4\textheight]{heterogeneous_hemo.png}\\
        \includegraphics[height=0.4\textheight]{crack3D.png}

      \column{0.5\textwidth}
        {\small
        \begin{itemize}
        \item Some details are less relevant
        \item "Simplify" those
          \begin{itemize}
          \item 1D phenomenological
          \item 1D Emulator of detailed model
          \item Reduced model
          \end{itemize}
        \item Coupling is challenging
        \end{itemize}
        }
    \end{columns}

  \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Technical background}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
{\Large Technical background}
\end{frame}
}

\section{Terminology}
\input{../includes/intra_extra_inter.tex}

%----------------
\section{Regularization and Constraints}
\input{../includes/constrained_regularized_regression.tex}

%----------------
\part{Regression, Differential Operators, GPs, and Kalman filters}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
{\Large Regularized Regression\\Linear Differential Operators\\Gaussian Processes\\Kalman Filters}\\
\end{frame}
}

\input{../includes/RR_GP_KF.tex}

\begin{frame}[t]{Summary: Linear operators}
  \begin{itemize}
  \item KF, GP and RR are unified (linear, stationary, convex loss-function): interpretability, algorithmic versatility
  \item GP $\rightarrow$ KF: GP inference linear in the number of time samples
  \item RR $\rightarrow$ GP: non-parametric curve fitting
  \end{itemize}

  \textbf{Conclusion:}
  information provided as linear operators can be merged with ML!

  \vspace{1em}
  Technical challenges: optimal algorithms, fast implementations,
  (sample) convergence rates, language abstractions, optimized hardware
\end{frame}

%----------------
\part{Nonlinear operators}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
{\Large Nonlinear operators}\\
\only<2>{{\Large {\textbf Here it gets sketchy!}}}
\end{frame}
}

\section{Surrogate trajectory method}
\begin{frame}{Surrogate trajectory}

Differential operators that are "linear on the differential part"

\[
\mathcal{D}(x) = \mathcal{L}x - f(x, \theta) = 0
\]

We will use a surrogate for $x$

\[
\mathcal{D}(\phi) \simeq 0
\]

How to find the surrogate?

One alternative
\[
\phi_{n+1} = L^{-1} f(\phi_{n}, \theta)
\]

\[
\phi_n = \bm{x} \cdot \bm{\varphi}
\]

\end{frame}

% Filippone 2018
% Gonzales 2014
% Carbajal 2012

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

%----------------
\appendix
  \begin{frame}[c]{GP to KF}
  Stationary covariance matrices can be converted to state-space models, where KF can be applied\footfullcite{Sarkka2013}, via Wiener-Khinchin theorem.
  \[
  \kappa(t-t') \propto \int{S(\omega)e^{\mathrm{i}\omega \left(t-t'\right)} \ud \omega}
  \]
  \end{frame}

  \begin{frame}[c]
    \frametitle{Concretely}
    \begin{columns}[c]
      \column[c]{0.4\textwidth}
        Spatio-temporal GP representation
        \begin{align*}
        f(\bm{r},t) &\sim \GP(0, \kappa(\bm{r},t;\bm{r}', t'))\\
        y_k &= \mathcal{H}_k f(\bm{r},t_k) + \epsilon_k
        \end{align*}

      \column[c]{0.2\textwidth}
        \centering
        \Huge$\Rightarrow$%\hbox{\scalebox{2}{}}

      \column[c]{0.4\textwidth}
        Linear stochastic partial differential equation
        \begin{align*}
        \pder{\bm{\mathrm{f}}(\bm{r},t)}{t} & = \bm{\mathcal{F}}\bm{\mathrm{f}}(\bm{r},t) + \bm{L}\bm{\mathrm{w}}(\bm{r},t) \\
        y_k &= \bm{\mathcal{H}_k} \bm{\mathrm{f}}(\bm{r},t_k) + \varepsilon_k
        \end{align*}
      \end{columns}
      \vspace{1em}
      \begin{columns}
        \centering
        \column[t]{0.5\textwidth}
        $O((Tn)^3)$ general matrix inversion
        \column[t]{0.5\textwidth}
        $O(Tn^3)$ infinite-dimensional Kalman filtering and smoothing
      \end{columns}

      \vspace{1em}
      \centering
      {\small
      \begin{enumerate}
      \item Compute spectral density (SD) via Fourier transform of covariance
      \item Approximate SD with a rational function (if necessary)
      \item Find stable transfer function (poles in upper half complex plane)
      \item Transform to SS using control theory methods
      \end{enumerate}
      }
    \end{frame}

  \begin{frame}[t]
    \frametitle{Example: isotropic Mat\'ern covariance function}
    Temporal process
    \begin{gather*}
    \kappa \left(t-t'\right) = \sigma^2 \lambda \left(t-t'\right) K_1\left[\lambda \left(t-t'\right)\right]\\
    \pder{\bm{\mathrm{f}}(t)}{t} = \begin{bmatrix}0 & 1\\ -\lambda & -2\sqrt{\lambda}\end{bmatrix}\bm{\mathrm{f}}(t) + \begin{bmatrix}0\\1\end{bmatrix}\bm{\mathrm{w}}(x,t)
    \end{gather*}
    Spatio-temporal process
    \begin{gather*}
    r = \Vert(x,t)-(x',t')\Vert\\
    \kappa (r) = \sigma^2 \lambda r K_1\left(\lambda r\right)\\
    \pder{\bm{\mathrm{f}}(x,t)}{t} = \begin{bmatrix}0 & 1\\ \pder{^2}{x}-\lambda & -2\sqrt{\lambda - \pder{^2}{x}}\end{bmatrix}\bm{\mathrm{f}}(x,t) + \begin{bmatrix}0\\1\end{bmatrix}\bm{\mathrm{w}}(x,t)
    \end{gather*}
  \end{frame}

  \begin{frame}{Surrogate trajectory}
    Adomain polynomials
    RKHS
    Regularization
  \end{frame}

  \begin{frame}{Other methods}
    Volterra-Wiener kernels
  \end{frame}

\end{document}
