rm *.bib *.bcf *.aux* *.bbl *.nav
pdflatex $1.tex
biber $1
pdflatex $1.tex
