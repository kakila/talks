\documentclass[xcolor=dvipsnames, english]{beamer}

\input{../includes/beamer_basics.tex}
\usetheme{Madrid}
\usecolortheme[named=CornflowerBlue]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts}
%\setbeameroption{hide notes}
%\setbeamertemplate{note page}[plain]

\usepackage{dirtree}

\usepackage{minted} % Highlight source code using Pygments
\renewcommand{\listingscaption}{Code}

\definecolor{LightGray}{gray}{0.95}
\newcommand{\octcli}[1]{%
% Bug in minted bgcolor triggered since TexLive2016.
% Fixed on minted 2.5.1 https://tex.stackexchange.com/questions/388796/compilation-fails-using-pygment-minted-on-texlive-2017/389058
%\colorbox{LightGray}{\texttt{octave> }\mint{octave}|#1|} %
\colorbox{LightGray}{\texttt{octave> }\mintinline{octave}|#1|} %
}
\newcommand{\incli}[2]{%
% Bug in minted bgcolor triggered since TexLive2016.
% Fixed on minted 2.5.1 https://tex.stackexchange.com/questions/388796/compilation-fails-using-pygment-minted-on-texlive-2017/389058
%\colorbox{LightGray}{\texttt{octave>}}\mint[bgcolor=LightGray]{octave}|#1|
\colorbox{LightGray}{\texttt{#1> }\mintinline{octave}|#2|} %
}

\newcommand{\octv}[1]{%
\mintinline{octave}{#1}%
}


% Comments
\newcommand{\jpi}[1]{{\scriptsize\color{Magenta}#1}}

%----------------
% title informations

\title{Scientific computing with GNU Octave}
\subtitle{Free your numbers}

\author[JPi Carbajal]{Juan Pablo Carbajal\\\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{Feb 26, 2018}
% ====================================================================

\begin{document}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

\begin{frame}{About the speaker}
  \includegraphics[width=\textwidth]{intro.png}
\end{frame}

\begin{frame}[t]{Polyglotism}
{Understand the problem, then choose the language}

  Programming knowledge $\neq$ Syntax

  \vspace{1em}
  \centering
  \includegraphics[width=0.15\textwidth]{GNUOctave_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.25\textwidth]{Julia_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{Netlogo_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{R_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.1\textwidth]{Java_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.2\textwidth]{Haskell_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{Cpp_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.35\textwidth]{Python_logo.png}

  {\Huge \dots}
\end{frame}

\begin{frame}[t]{GNU Octave}
\only<1>
{
  \begin{columns}[T]
    \column{0.5\textwidth}
      \includegraphics[width=0.4\textwidth]{GNUOctave_logo.png}\raisebox{0.25\textheight}{\url{www.octave.org}}
    \column{0.5\textwidth}
      \includegraphics[width=\textwidth]{octave_example_mesh.png}
  \end{columns}
  \vspace{0.5em}
  
  High-level language intended for numerical computations

  1988 - \dots

  \vspace{-1.5em}
  \[
  \bm{c} = \left(A^\top A + \lambda I\right)^{-1}A^\top \bm{y}
  \]

  \octv{I = eye (size (A));}

  \octv{c = ( A' * A + l * I) \ (A' * y)}
}

\only<2>
{
  \begin{columns}[t]
  \column{0.5\textwidth}
    \textbf{Uses and features}
    \begin{itemize}
      \item Data analysis
      \item Simulations
      \item (non)Linear optimization
      \item Object oriented programming
      \item Function handles (pointers)
      \item Packages
      \item \dots
    \end{itemize}

  \column{0.5\textwidth}
    \textbf{Not meant for} (still can do)
    \begin{itemize}
      \item Symbolic algebra
      \item Meta programming
      \item Advanced string processing
      \item File system manipulations
      \item Functional programming
      \item \dots
    \end{itemize}
  \end{columns}
  
  \vspace{1em}
  List of publications using Octave: \url{https://wiki.octave.org/Publications_using_Octave}

  \vspace{1em}
  Missing features: \url{www.gnu.org/software/octave/missing.html}
 
  You can help! E.g. summer of code programs
}
\end{frame}

\begin{frame}{GNU Octave: showcase}
  \framesubtitle{Data analysis \& visualization}
  \centering
  \includegraphics[width=0.4\textwidth]{GPfromDS.pdf}~
  \includegraphics[width=0.4\textwidth]{gp.png}
  \includegraphics[width=0.4\textwidth]{terrain.png}~
  \includegraphics[width=0.4\textwidth]{scatter_contour.png}
\end{frame}

\begin{frame}[t,fragile]{GNU Octave: showcase}
  \framesubtitle{Dynamical systems}
  \begin{minted}[fontsize=\small,framesep=2mm]{octave}
n     = 15;
y0    = logspace (-2, log10 (5), n);
v0    = zeros (1, n);
dydt  =@(t,y) [y(n+(1:n)); (1-y(1:n).^2)*y(n+(1:n))-y(1:n)];
opt   = odeset ('Abstol', 1e-8, 'RelTol', 1e-8);
[t y] = ode45 (dydt, [0,1], [y0 v0], opt);
plot (y(:,1:10), y(:,11:20), '-k');
axis tight
  \end{minted}
  \begin{columns}[c]
  \column{.5\textwidth}
  \vspace{-2em}
  \begin{align*}
    \dot{y}_1 &= y_2\\ 
    \dot{y}_2 &= \left(1 - y_1^2\right) y_2 - y_1
  \end{align*}
  \column{.4\textwidth}
  \includegraphics[height=0.4\textheight]{Van_der_Pol.png}
  \end{columns}
\end{frame}

\begin{frame}{GNU Octave: showcase}
  \framesubtitle{Dynamical systems}
  \begin{columns}[c]
  \column{.5\textwidth}
    \begin{itemize}
      \item Biochemical reactions
      \item Interconnected tanks
      \item Population dynamics
      \item Mechanics (Lagrangean/Hamiltonian)
      \item \dots
    \end{itemize}

  \column{.5\textwidth}
    \href{run:../img/TankChain.avi?autostart&loop&noprogress}
    {\includegraphics[width=\textwidth]{TankChain-0.png}}

  \end{columns}
\end{frame}

\begin{frame}[t]{GNU Octave: packages}

  \url{https://octave.sourceforge.io/packages.php}

  Community or user maintained libraries for specific functionality
  
  \vspace{0.5em}
  \includegraphics[width=\textwidth]{OF_packages.png}
\end{frame}

\begin{frame}[t, fragile]{GNU Octave: packages}
  Share your developments!

  \begin{columns}[T]
    \column{0.45\textwidth}
  \dirtree{%
.1 \textbf{\color{Blue}mfile-minimal}.
.2 COPYING.
.2 DESCRIPTION.
.2 \alert{CITATION}.
.2 \textbf{\color{Blue}inst}.
.3 example\_mfile.m.
}
  \column{0.55\textwidth}
    \begin{enumerate}
    \item Zip it (.zip) or tar it (.tar.gz): mypkg-0.1.0.zip
    \item start octave
    \item \octv{pkg install mypkg-0.1.0.zip}
    \end{enumerate}
  \end{columns}

  \vspace{1em}
  \incli{octave:1}{pkg load mypkg # add package to path}
  \incli{octave:2}{help example_mfile # use the package files}
  \incli{octave:3}{pkg unload mypkg # remove the package from path}

  \vspace{1em}
  \textbf{\alert{Enjoy!}}
\end{frame}

\begin{frame}{Want to know more?}
    \begin{itemize}
      \item Online manual \small(\url{www.gnu.org/software/octave/doc/interpreter})
      \item Wiki \small(\url{wiki.octave.org})
      \item Help mailing list \href{mailto:help-octave@gnu.org}{\small\texttt{help-octave@gnu.org}}
      \item G+ (\href{https://plus.google.com/communities/112976184309608583944}{\small\texttt{GNU Octave on G+}})
      \item IRC channel \#octave at irc.freenode.net \small(\url{webchat.freenode.net})
      \item Twitter (\href{https://twitter.com/theoctaveguy}{\small\texttt{@theoctaveguy}}, \href{https://twitter.com/juanpicarbajal}{\small\texttt{@JuanPiCarbajal}}, \dots)
    \end{itemize}

    \textbf{Support Octave's development:}\\
    \url{www.gnu.org/software/octave/get-involved.html}\vspace{1em}\\
    Donations are also helpful:\\
    \url{www.gnu.org/software/octave/donate.html}
\end{frame}

{
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Libre (Free) software?}\\
\Huge{FLOSS, FOSS, \dots}
\end{frame}
}

\input{../includes/libre_software_table.tex}

\begin{frame}[t]{Libre software: perspetives}
  \only<1>
  {
    \framesubtitle{Student}
    \begin{columns}
      \column{0.3\textwidth}
      \includegraphics[width=\textwidth]{studying.png}
      \column{0.7\textwidth}
      Scenario: low budget, plenty of time
      \begin{itemize}
      \item Many programs different functionality. How to connect them?
      \item New OS, Old hardware
      \item Crazy ideas?
      \item Time spent, reuse
      \end{itemize}
    \end{columns}
  
  \vspace{1em}
  Anecdotes: data analysis, cas, compiler extensions


  \includegraphics[width=0.15\textwidth]{Maxima_logo.png}\hspace{0.5cm}
  \raisebox{0.8em}{\includegraphics[width=0.3\textwidth]{Sage_logo.png}}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{Geogebra_logo.png}\hspace{0.5cm}

  }
  
  \only<2>
  {
    \framesubtitle{Researcher}
    \begin{columns}
      \column{0.3\textwidth}
      \includegraphics[width=\textwidth]{thinking_on_brain.png}
      \source{\copyright Shun Iwasawa, ISBN: 9780262162395}
      \column{0.7\textwidth}
      Scenario: work demands combine well with software development
      \begin{itemize}
      \item Collaboration!
      \item Portability
      \item Reuse, reproduce
      \item Understanding algorithms
      \item Crazy ideas!
      \item Customization
      \end{itemize}

    \end{columns}

  }

  \only<3>
  {
    \framesubtitle{Entrepreneur}
    \begin{columns}
      \column{0.3\textwidth}
      \includegraphics[width=\textwidth]{entrepreneur.png}
      \column{0.7\textwidth}
      Scenario: work demands hinder software development, good budget, freedom

      \begin{itemize}
        \item Scalability
        \item Customization
        \item Transparency
        \item Auditability
        \item Portability
        \item Freedom!
      \end{itemize}
    \end{columns}
\vspace{1em}
  Myth: proprietary software X is liable. Read the disclaimer! Read the terms of use!
\vspace{1em}\\
  Anecdote: Canton vs. Ing. bureau

  }
\end{frame}

\begin{frame}[t]{Libre software in brief}
\begin{columns}[T]
  \column{0.5\textwidth}
  \begin{itemize}
    \item Longevity of your code
    \item Sharing
    \item Visibility
    \item Impact
    \item Extensions
    \item Maintenance
    \item Community
  \end{itemize}
  \column{0.5\textwidth}
  \begin{itemize}
    \item Independence
    \item Avoid vendor lock-in
    \item Prevent hidden subsidies (public sector)
    \item Boost customization (clients, projects, etc.)
    \item Societal capitalization
  \end{itemize}
\end{columns}
\vfill

{\Large Libre data? $\rightarrow$ Open data}
\end{frame}

\begin{frame}[t]{Polyglotism}
  \centering
  \includegraphics[width=0.15\textwidth]{GNUOctave_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.25\textwidth]{Julia_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{Netlogo_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{R_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.1\textwidth]{Java_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.2\textwidth]{Haskell_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{Cpp_logo.png}\hspace{0.5cm}
  \includegraphics[width=0.35\textwidth]{Python_logo.png}
  \includegraphics[width=0.15\textwidth]{Maxima_logo.png}\hspace{0.5cm}
  \raisebox{0.8em}{\includegraphics[width=0.3\textwidth]{Sage_logo.png}}\hspace{0.5cm}
  \includegraphics[width=0.15\textwidth]{Geogebra_logo.png}\hspace{0.5cm}

  \vspace{0.5em}
  Choose the language that suits you \dots
  
  \vspace{0.5em}
  {\Large \textbf{keep it Libre!}}
\end{frame}

\begin{frame}{GNU Octave course @ CAE}

  \textbf{Dates}: On March 7th, 21st, and 28th\vspace{1em}\\
  \textbf{Registration}: \url{https://goo.gl/jJWtRJ}\vspace{1em}\\
  \textbf{Fee}: 30 CHF\vspace{1em}\\
  \alert{Limited seats!}\vspace{1em}\\

  Note: 07/03 is an introduction to programming

\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}
\end{document}
