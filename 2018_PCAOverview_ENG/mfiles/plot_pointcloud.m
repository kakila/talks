## Copyright (C) 2018 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

% plot cloud points
function h = plot_pointcloud (xy, str='')
  htmp = plot (xy(:,1), xy(:,2), 'o', ...
    'color',[0.5 0.5 0.5], 'markersize', 3, 'markerfacecolor', 'auto');
  axis equal
  box off
  set (gca, 'xaxislocation', 'origin', 'yaxislocation', 'origin');
  
  cf  = corr (xy(:,1), xy(:,2));
  str = sprintf ('%s\nCorr. coeff.: %.2f\n', str, cf);
  title (str);
  
  if nargout > 0; h = htmp; endif
end

