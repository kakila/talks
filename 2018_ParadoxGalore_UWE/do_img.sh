#!/bin/bash

GVFILES=(simpsonscollider_CG.gv simpsonsconfounder_CG.gv simpsonscolliderInd_CG.gv)
IMGPATH=../img/

for f in ${GVFILES[@]}; do
  dot -Tsvg $IMGPATH$f > ${f%"gv"}svg
done
