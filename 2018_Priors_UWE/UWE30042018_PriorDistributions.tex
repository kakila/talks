\documentclass[xcolor=dvipsnames, english]{beamer}

\input{../includes/beamer_basics.tex}
\input{../includes/mathsymbols.tex}

% local packages
\usepackage{enumitem}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@book{Jaynes2003,
author = {Jaynes, Edwin T. and Bretthorst, G. L.},
isbn = {9781139435161},
publisher = {Cambridge University Press},
title = {{Probability Theory: The Logic of Science}},
year = {2003}
}
@article{Jaynes1973,
author = {Jaynes, E.T.},
doi = {10.1007/BF00709116},
journal = {Foundations of Physics},
month = {12},
number = {4},
pages = {477--492},
title = {{The well-posed problem}},
volume = {3},
year = {1973}
}
@article{Jaynes1968,
author = {Jaynes, E.T.},
doi = {10.1109/TSSC.1968.300117},
journal = {IEEE Transactions on Systems Science and Cybernetics},
number = {3},
pages = {227--241},
title = {{Prior Probabilities}},
volume = {4},
year = {1968}
}
\end{filecontents}

\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% Comments
\newcommand{\jpi}[1]{{\scriptsize\color{Magenta}#1}}

% -----------
% Set Eawag style

%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Prior probabilities: subjective?}

\author[JPi Carbajal]{Juan Pablo Carbajal\\\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{Apr 30, 2018}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load backgound for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load backgound for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page


%----------------
\begin{frame}{Resources}
  \begin{columns}
    \column{0.25\textwidth}
      \includegraphics[width=\textwidth]{cover_the_logic_of_science_Jaynes.jpg}
    \column{0.75\textwidth}
      \fullcite{Jaynes2003}
    \end{columns}
\vspace{0.5em}
Historical\footfullcite{Jaynes1968}\footfullcite{Jaynes1973}
\end{frame}

% ---------------------
\section{Motivation and desiderata}

\begin{frame}[t]{Desiderata}
  \vspace{-1em}
  \begin{enumerate}[label=(\Alph*)]
    \item Prior probabilities represent \emph{information} \textrightarrow determined by \emph{logical analysis} (not introspection)
    \item Conclusions depend on data and prior information: prior must be fully specified (just like data)
    \item "Objective" inference: {\color{BrickRed} two persons with the same prior information must assign the same prior probabilities}
  \end{enumerate}

  \only<1>{
    \begin{quote}
    "\dots If one fails to specify the prior information, a problem of inference is just as ill-posed as if one had failed to specify the data. \dots it is typical that we have cogent prior information, \dots to fail to take it into account is to commit the most obvious inconsistency of reasoning, and it may lead to absurd or dangerously misleading results."\footfullcite{Jaynes2003}
    \end{quote}
  }
  \only<2>{\jpi{Key question:} How do we find the prior probability representing "complete ignorance"?}

\end{frame}

% ---------------------
\section{Transformations groups}
\begin{frame}[t]{Scale parameter}
  \framesubtitle{Two problems}
  \vspace{-1em}
  Sampling distribution: $p(x|\sigma) \ud x = \phi(x,\sigma) \ud x$

  \begin{block}{Problem: observer A}
    Given a sample $X=\left\lbrace x_1, \dots, x_n\right\rbrace$, estimate $\sigma$
  \end{block}

  We need the prior probability:
  \[
  p(\sigma|X)\ud\sigma \propto p(X|\sigma) {\color{BrickRed}\bm{p(\sigma)}}\ud\sigma
  \]

  Change the scale (e.g. units): $0<a<\infty,\; \sigma' = a \sigma, \; x' = a x$

  \begin{block}{Problem: observer B}
    Given a sample $X'=\left\lbrace x'_1, \dots, x'_n\right\rbrace$, estimate $\sigma'$
  \end{block}

  {\bf How different are the problems? Does B know more than A?}
\end{frame}

\begin{frame}[t]{Scale parameter}
  \framesubtitle{Invariant sampling distribution}
  \vspace{-1em}
  Sampling distribution: $p(x|\sigma) \ud x = \phi(x,\sigma) \ud x$

  Change the scale: $\sigma' = a \sigma, \; x' = a x$
  \begin{align*}
    p(x |\sigma) \ud x &= p(x'|\sigma')\ud x'\\
    \phi(x, \sigma) \ud x &= \varphi(x', \sigma') \ud x'\\
    \phi(x, \sigma) &= a \varphi(x', \sigma')
  \end{align*}

  Asking invariance: $\varphi \equiv \phi$
  \begin{align*}
    \phi(x, \sigma) &= a\phi(ax, a\sigma) \; \forall a \\
    \phi(x, \sigma) &= \frac{1}{\sigma} h\left(\frac{x}{\sigma}\right)
  \end{align*}
  e.g. $h=\frac{x}{\sigma}$, $h=e^{-\frac{x}{\sigma}}$, $h=\sin\left(\frac{\sigma}{x}\right)$, \dots
\end{frame}

\begin{frame}[t]{Scale parameter}
  \framesubtitle{Invariant prior}
  \vspace{-1em}
  Meaning of "complete ignorance" of $\sigma$? (we now know it is a scale)
  \begin{align*}
    p(\sigma) \ud \sigma &= p(\sigma') \ud \sigma'\\
    f(\sigma) &= a g(\sigma')
  \end{align*}

  If problem changes with units, then the choice of units is informed:
  complete ignorance \textrightarrow invariance under scale change

  \vspace{0.4em}
  Prior must be scale invariant: $f \equiv g$, i.e. \fbox{$f(\sigma) = a f(a \sigma) \; \forall a$}
  \only<1>{
    \[
      f(\sigma) = C\neq a C = a f(a \sigma)
    \]

  Ideas?
  }
  \only<2>{
    \[
      f(\sigma) = \frac{C}{\sigma}
    \]
  }
\end{frame}

\section{Conclusions}
\begin{frame}[c]{Summary}

  \begin{itemize}[label=$\bullet$]
    \item "complete ignorance" alone, doesn't help much to determine the prior.
    \item we need mathematical specification, e.g. a list of transformations that map equivalent problems
    \item if enough transformations are given, the prior dist. is fully determined
    \item the prior represents a state of knowledge, not an opinion.
  \end{itemize}

  \begin{quote}
  Ignorance is preferable to error.
  Those who believe nothing are less remote from truth than those who believe what is wrong.

  \hfill (from Thomas Jefferson (1781))
  \end{quote}

\end{frame}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}
\end{document}
