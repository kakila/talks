\documentclass[xcolor=dvipsnames, english]{beamer}

\input{../includes/beamer_basics.tex}

%% --- use packages
\usepackage{abraces,mathtools}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@online{INI,
  author = {Isaac Newton Institute for Mathematical Sciences},
  title = {[UNQW02] Surrogate models for UQ in complex systems},
  year = 2018,
  url = {https://www.newton.ac.uk/event/unqw02},
  urldate = {2018-02-15}
}
\end{filecontents}

\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

\graphicspath{{../img/}}

\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{#1}\!\times\!{#2}}
\newcommand{\trp}{\top}
\newcommand{\sm}{\scalebox{0.5}{-1}}

\DeclareMathOperator*{\armin}{arg\,min}

% Comments
\newcommand{\jpi}[1]{{\scriptsize\color{Magenta}#1}}

% -----------
% Set Eawag style

%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Surrogate models for UQ in complex systems}
\subtitle{Observer-participant summary of the Feb'18 Woprkshop at INI, Cambridge, UK}

\author[JPi Carbajal]{Juan Pablo Carbajal\\\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{Feb 19, 2018}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load backgound for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load backgound for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page


%----------------
\section{Overview of topics and themes}
\begin{frame}{Resources}
\fullcite{INI}
\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\begin{frame}[plain]
  % This isn't included in the repo
  \hbox{\hspace{-3em}
  \only<1>{
  \includegraphics[width=\paperwidth]
  {/home/juanpi/Documents/IsaacNewton_Workshop/UNQW02_group_photo.jpg}
  }
  }
  \only<2>{
  \includegraphics[width=\paperwidth]
  {blackboard_unusual.jpg}
  }
\end{frame}
}


\begin{frame}[t]{Index}
  {\small \url{https://www.newton.ac.uk/webseminars} Event code: UNQW02}
  
  \vspace{-2em}
  \begin{columns}[t]
    \column{0.5\textwidth}
      \begin{itemize}
      \item Stochastic FEM (sp. coding)\\
      {\scriptsize Powell, Tamelllini, Zhang, Tran, Eigel, Tempone}
      \item Emulators\\
      {\scriptsize Goldstein, Smith, Shoemaker, Spiller, \textbf{Teckentrup}, Gramacy, Williamson}
      \item Experimental design\\
      {\scriptsize Eigel, Shoemaker, Teckentrup, Adamou, Gramacy}
      \item Polynomial approximations\\
      {\scriptsize Powell, Tamellini, Adcock, Tempone, Ernst}
      \end{itemize}
  
  \column{0.5\textwidth}
      \begin{itemize}
      \item Gaussian proccesses (app.)\\
      {\scriptsize Shoemaker, Spiller, Williamson, Goldstein, Gosling (disc.), Roustant (cat.), Gramacy}
      \item Gaussian proccesses (theory)\\
      {\scriptsize \textbf{Ginsbourger}, Teckentrup, Roustant (cat.)}
      \item Deep structures\\
      {\scriptsize Filippone, Schwab (asymp.)}
      \item Convergence results (theory)\\
      {\scriptsize Schwab, Teckentrup, Tempone, Eigel}
      \end{itemize}
  \end{columns}
\end{frame}

\section{Forward problems}
\begin{frame}[t]{FEM}
\vspace{-2em}
\onslide<1-3>{
\[
\mathcal{L}_{\bm{x}}(\bm{\lambda})u = f \quad \bm{x} \in \Omega \quad u \in \mathcal{H}(\mathbb{R})
\]\\\vspace{-1em}\jpi{and BC}\\
e.g.
\vspace{-1em}
\[
\lambda \nabla^2 u = \exp(-x^2), \quad u[\partial\Omega] = 0
\]
}
\onslide<2-3>{
\[
u = \bm{\Phi}^\trp \bm{u} \quad \phi_i \in \mathcal{H}_h(\mathbb{R})
\]

\includegraphics[width=0.7\textwidth]
{fem_basis.png}~
\source{brickisland.net/cs177/?p=309}
}
\onslide<3>{
\[
L(\bm{\lambda}, h) \bm{u} = \bm{f} \quad \bm{u},\bm{f} \in \mathbb{R}^N \quad L \in \mathbb{R}^{\stimes{N}{N}}
\]
}
\end{frame}

\begin{frame}[t]{Stochastic FEM}
\vspace{-2em}
\onslide<1-2>{
\[
\mathcal{L}_{\bm{x}}(\bm{\lambda})u = f \quad \bm{x} \in \Omega \quad u \in \mathcal{H}(\mathbb{R})
\]\\\vspace{-1em}\jpi{and BC}\\
e.g.
\vspace{-1em}
\[
\nabla \cdot (\lambda(\bm{x},\bm{\theta}) \nabla u) = f(\bm{x})
\]
with $\lambda(\bm{x},\bm{\theta})$ a random field (stochasticity represents unknowns: sources, domain, \dots)
}
\onslide<2>{
\[
\lambda(\bm{x},\bm{\theta}) = \lambda_0(\bm{x}) + \sum_{i=1}^{{\color{Red}\bm{\infty}}} \lambda_i(\bm{x}) \psi_i(\bm{\theta})
\]

\[
\hat{u}(\bm{x},\bm{\theta}) = \sum_{i=1}^N\sum_{j=1}^M u_{ij}\phi_i(\bm{x})\psi_j(\bm{\theta})
\]

Size of $L$ is now $\stimes{NM}{NM}$!
}

\end{frame}

\begin{frame}[t]{Sparse representation}
\vspace{-2em}
Avoid explosion of $M$ $\rightarrow$ optimize representation random field.
\only<2-3>{
  Idea:
  \[
   p(x) = a + b x + c x^8 \rightarrow \begin{cases} \begin{bmatrix}c & 0 & 0 & 0 & 0 & 0 & 0 & b & a\end{bmatrix} \\ \left(\begin{bmatrix}8 & 1 & 0\end{bmatrix}, \begin{bmatrix}c & b & a\end{bmatrix}\right)\end{cases}
  \]
}
\only<3-4>{
  Scary wording: finite subset $J_M$ of $M$ finitely supported sequences
  \[
  J_M \subset \bset{\bm{\alpha} = \left(\alpha_1, \alpha_2, \ldots\right) \in \mathbb{N}_0^\mathbb{N}, \vert\bm{\alpha}\vert < \infty}
  \]
}

\only<4>{
  compact notation $\bm{\alpha}_i$ $i = 1, \ldots,M$
  \[
  \bm{\alpha}_1 = (3,1,0,0,10,0,\ldots) \quad \psi_{\bm{\alpha}_1} (\bm{\theta}) = \psi_3(\theta_1) \psi_1(\theta_2) \psi_{10}(\theta_5)
  \]
  \vspace{-1em}

  \[
  \bm{\alpha}_{2i} = \begin{cases}2 & i=3,7\\25 & i=4 \end{cases}\quad \psi_{\bm{\alpha}_2} (\bm{\theta}) = \psi_2(\theta_3) \psi_2(\theta_7) \psi_{25}(\theta_4)
  \]
}

\only<5>{
  \[
  \phi_{\bm{\alpha}}(\bm{\theta}) = \prod_{k=1}^{{\color{Red}\bm{\infty}}} \psi_{\alpha_k}\left(\theta_k\right), \quad \bm{\alpha} \in J_M
  \]
}

\end{frame}

\section{Emulation}
\begin{frame}{Emulation mindset}
  \begin{itemize}
  \item \textbf{Eclectic}: implicit irrelevant prior information (ML)
    \begin{itemize}
      \item Universal approximators?
      \item What can be represented?
    \end{itemize}
  \item \textbf{Informed}: explicit expert prior information
    \begin{itemize}
      \item Regularization/Smoothing
      \item Mainly kernel methods: how to encode?
    \end{itemize}
  \item \textbf{Constrained}: exact explicit properties of regressor/interpolant
    \begin{itemize}
      \item How to encode constraints? 
      \item Results on kernel methods: linear operators
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[t]{Constrained emulator}
  \only<1>{
    Exact properties of samples and conditioned GP

    E.g. mass conservation, symmetries, \dots
    
    \vspace{0.5em}
    \begin{enumerate}
    \item Properties as null space of linear operators $T(f) = 0$
    \item There is a unique operator $\mathcal{T}k(\cdot,x^\prime)(x)$ with samples in the null of $T$
    \end{enumerate}

    \begin{center}
    \vspace{-1em}
    \includegraphics[width=0.5\textwidth]
    {symm_SE_1D.png}~
    \includegraphics[width=0.5\textwidth]
    {symm_SEm0p5_2D.png}
    \end{center}
  }
  \only<2>{
  \centering
  \includegraphics[width=\textwidth]
  {invariantGP_cteIntegral_Ginsbourger.png}\\
  \source{Ginsbourger et al. arXiv:1308.1359}
  }

  \only<3>{
  \centering
  \begin{columns}[c]
    \column{0.3\textwidth}
    \includegraphics[height=0.7\textheight]
    {invariantGP_symmetry1_Ginsbourger.png}
    \column{0.7\textwidth}
    \includegraphics[width=\textwidth]
    {invariantGP_symmetry2_Ginsbourger.png}\\
  \end{columns}
  \source{Ginsbourger et al. 10.1016/j.jspi.2015.10.002}
  }

  \only<4>
  {
  Regression
  \[
  f(\bm{x}) = \armin_{f \in \mathcal{H}} \sum_{i=1}^N \Vert y(\bm{x}_i) - f(\bm{x}_i)\Vert^2 \textrm{ s.t. } T(f) {\color{red} \bm{=}} 0
  \]

  Interpolation
  \begin{align*}
  f(\bm{x}_i) &= y(\bm{x}_i) \quad i = 1, \ldots, N \\ T(f) &{\color{red} \bm{=}} 0
  \end{align*}
}
\end{frame}

\begin{frame}{Informed emulation}
\only<1>{
  Regularized regression
  \[
  f(\bm{x}) = \armin_{f \in \mathcal{H}} \sum_{i=1}^N \Vert y(\bm{x}_i) - f(\bm{x}_i)\Vert^2 + \lambda \Vert T(f) \Vert
  \]

  Interpolation
  \[
  f(\bm{x}) = \armin_{f \in \mathcal{H}} \Vert T(f) \Vert \textrm{ s.t. } f(\bm{x}_i) = y(\bm{x}_i)
  \]

  E.g. stationary anisotropic convection-diffusion equation 
  \[
   T(f) = -\nabla \cdot \left(\bm{K}\nabla f\right) + \nabla \cdot \left(\bm{b} f\right) + c f
  \]

}
\only<2>{
  \centering
  \includegraphics[height=0.4\textheight]
  {PDESmoothing_2_Azzimonti.png}
  \includegraphics[height=0.3\textheight]
  {PDESmoothing_1_Azzimonti.png}\\
  \source{Azzimonti et al. 10.1080/01621459.2014.946036}
}
\end{frame}

\begin{frame}{Emulation of likelihood}
\framesubtitle{Bayesian inverse problems}
\onslide<1->{
  Given $y$ find $u$, knowing that
  \[
  y = G(u) + \eta
  \]\\
  %\vspace{-1em}
  }
\only<1>{
  \begin{itemize}
  \item $G$ is generally given by a expensive to compute nonlinear mapping\\
  \item $u$ is a vector of parameters\\
  \item $G$ can't be inverted: unknown $\eta$, $G^{-1}$ ill-posed
  \end{itemize}
}
\onslide<2->{
Consider $u$ random var. (pdf $\pi_0(u)$) and $\eta \sim N(0, K)$ \jpi{(!)}
  }
  
\onslide<3->{
  \vspace{0.5em}
  Likelihood of the data
  \[
  P(y|u) \propto \exp\left(-\frac{1}{2}\Vert y - G(u)\Vert^2_{K^{-1}}\right) := \exp(-\Phi(u))
  \]\\
  \vspace{-1em}
  \jpi{(given by dist. of $\eta$)}

  \vspace{0.5em}
  Posterior of $u$
  \[
  \pi^y(u) = \frac{1}{Z}\exp(-\Phi(u)) \pi_0(u), \quad Z = \mathbb{E}_{\pi_0}\left[\exp(-\Phi(u))\right]
  \]\\
  \vspace{-1em}
  \jpi{(link to regularization!)}
}

\onslide<4>{
  Idea: emulate negative log-likelihood $\Phi(u)$ \jpi{(norm residuals)} using $N$ samples.
}
\end{frame}

\begin{frame}{Emulation of likelihood}
  Hand waving!
  \begin{align*}
  d\left(\pi^y, \pi^y_{N}\right) &\leq \Vert\Phi - \Phi_N\Vert\\
  \operatorname{Std}\left[d\left(\pi^y, \pi^y_{N}\right)\right] &\leq \Vert\operatorname{Std}\left[\Phi - \Phi_N\right]\Vert\\
  \end{align*}\\
  \vspace{-3em}
  \jpi{(accuracy, precision)}
  \vspace{0.5em}

  Choose GP (fixed cov. fun.) $\rightarrow$ rhs explicit bounds (fun. of $N$)
  \[
  \frac{(\log N) ^ {k(\nu, d)}}{N^\nu}
  \]
  
  See Aretha Teckentrup's "Surrogate Models in Bayesian Inverse Problems"
\end{frame}

\section{Discussions}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Discussion}
\end{frame}
}

\begin{frame}[t]{Wrong assumptions, bad results}
\onslide<1-3>{
"Method XX outperforms kernel methods"
\jpi{GP case: GP $\equiv$ SE cov. fun.}
}\\ \vspace{0.5em}

\onslide<2-3>{
XX turns out to be a kernel method!
}\\ \vspace{0.5em}

\onslide<3>{
"Assumptions made by XX lead to better results"

"A conformal XX method"
\\ \vspace{0.5em}
Better priors $\rightarrow$ fewer data, better optimization, \dots

\begin{center}
\raisebox{0.3\height}{\includegraphics[width=0.5\textwidth]{table_Voronoi_Gosling.png}}~
\includegraphics[width=0.3\textwidth]{voronoiGP_Gosling.png}
\source{Gosling et al. "Modeling discontinuities using Voronoi tessellations"}
\end{center}
}

\end{frame}

\begin{frame}[t]{XX vs. kernels (GP)}
\onslide<1-2>{
"Method XX outperforms kernel methods" 
\\ \vspace{0.5em}
XX turns out to be a kernel method! (e.g. extremely randomized forests, Geurts et al. 2006)
}\\ \vspace{0.5em}

\onslide<2>{
"Implementation XX leads to better results"

Fewer resources $\rightarrow$ more evaluations, better optimization, \dots

\begin{alertblock}{Implementation vs. mathematics}
"\textit{\dots it is one thing not to be able to perform a certain feat, but quite another to prove that it cannot be done.}"

{\hfill \small The Canterbury Puzzles and Other Curious Problems,} \\
{\hfill \small H. E. Dudeney, 1919}
\end{alertblock}
}
\end{frame}

\begin{frame}[t]{Orthonormal expansion vs. kernels (GP)}
  Kernel Interpolation \jpi{(regression)}
  \[
  \hat{y}(x) = k(x,\bm{X}) k(\bm{X},\bm{X})^{-1} \bm{Y}
  \]

  Polynomial expansion w. interpolation
  \begin{align*}
  \hat{y}(x) & := \Psi(x, \bm{X}) \bm{a}\\
  \bm{Y} &= \Psi(\bm{X}, \bm{X}) \bm{a}
  \end{align*}
  \vspace{-1em}
  \[
    \hat{y}(x) = \overbrace{\Psi(x, \bm{X})\Psi^\trp}^{\mathclap{\scriptstyle k(x,\bm{X})}} \big(\underbrace{\Psi\Psi^\trp}_{\mathclap{\scriptstyle k(\bm{X},\bm{X})}}\big)^{\sm} \bm{Y}
  \]

  Given a sampling method and a basis, what's the difference?

\end{frame}

\begin{frame}
\begin{itemize}
\item Schwab's results on DNN \jpi{(mindeset!)}
\item Optimal sampling (experiment design) (Tuckentrup)
\item IGA (Tamellini)
\item Emulators of stochastic simulators \& opt. sampling (Gramacy)
\end{itemize}
\end{frame}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}
\end{document}
