\documentclass[xcolor=dvipsnames]{beamer}

\input{../includes/beamer_basics.tex}

%% --- use packages
\usepackage{abraces,mathtools}
\usetikzlibrary{patterns}
% for notes with pdfpc
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
\setbeameroption{hide notes}
\usepackage{ulem}     % gives ther \sout command


%% Math symbols
\input{../includes/mathsymbols.tex}
%\newcommand{\sm}{\scalebox{0.5}{-1}}
%\newcommand{\ccov}[1]{{\color{red}k}\left(#1\right)}
%\newcommand{\cmean}[1]{{\color{blue}m}\left(#1\right)}
%\newcommand{\cgreen}[1]{{\color{red}G}\left(#1\right)}
%\newcommand{\cnull}[1]{{\color{blue}n}\left(#1\right)}
%\newcommand{\tens}[1]{\bm{\mathtt{#1}\,}}

\newcommand{\calert}[1]{{\color{BrickRed}\textbf{#1}}}

\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand{\footnotesize}{\tiny}

% -----------
% Set Eawag style
%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Beyond emulation of water networks}

\author[JPi Carbajal]{Juan Pablo Carbajal
\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{UWE meeting, Eawag, Switzerland. March 11, 2019}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load background for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load background for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------

\begin{frame}{Engineered complex systems}
  \framesubtitle{Optimization, Control, \& Maintenance}
  \centering
  \includegraphics[width=0.8\textwidth]{WindFarm_Fluvanna_2004.jpg}

  \source{Leaflet, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=5704247}}
\end{frame}

\begin{frame}{Data fusion/interpretation}
  \centering
  \includegraphics[height=0.4\textheight]
  {PDESmoothing_2_Azzimonti.png}\\
  \includegraphics[height=0.3\textheight]
  {PDESmoothing_1_Azzimonti.png}\\
  \source{Azzimonti et al. 10.1080/01621459.2014.946036}
\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
  \centering
  \Large{\textbf{What is emulation?}}\\
  \vspace{1em}
  {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
  learned from simulation results at selected inputs.\\
  \vspace{1em}
  \Large{How is emulation even possible?}

  \vspace{1em}
  \small{Simulator: a model providing quantitative results, e.g. numerical model}

\end{frame}
}

\begin{frame}{Cooking recipe}
  A simple procedure for emulation:

  {
    \scriptsize
    \begin{enumerate}
    \item \label{dataset} Build a dataset by running simulations on relevant inputs
    \item \label{hypothesis} Select a model to "fit" the dataset (e.g. GP, ANN, etc.). \calert{Hypothesis set}
    \item \label{split} Split your dataset into \calert{training} and \calert{test} set
    \item \label{train} "Fit" the data in the training set with your model (not an estimation of performance)
    \item \label{test} Test the model in the test set (this is an estimation of performance)
    \item Replace your simulator with the emulator, and solve the task
    \item Verify
    \end{enumerate}
  }
  Usually we iterate steps \ref{hypothesis}~--~\ref{test} (resampling), we need a third
  set for performance estimation that we never (\calert{ever}) use in the
  iteration: \calert{validation} set.

  \vspace{0.5em}
  Step \ref{dataset} might be included in the iteration: optimal design of (numerical) experiments.
\end{frame}


\begin{frame}{Guessing from samples}
  \centering
  \begin{columns}[T]
    \column{.5\textwidth}
      \only<1-2>{\includegraphics[height=0.7\textheight]{horse_learning_1.png}}
      \only<3>{\includegraphics[height=0.7\textheight]{horse_learning_2.png}}
    \column{.5\textwidth}
      \begin{itemize}
        \item<1-3> What's in the picture?
        \item<2-3> Is anybody riding it?
      \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}[t]{Reduction: From molecules to fluids}
  \centering
    \href{run:../img/Pseudofluids_Chipmunk.avi?autostart&loop&noprogress}%
     {\includegraphics[height=0.8\textheight]{pseudofluids.png}}
\end{frame}

\input{../includes/intra_extra_inter.tex}

\begin{frame}[t]{Extrapolation}
\framesubtitle{Emulator behavior "away" from the data?}
  \vspace{-0.5em}
  \centering
  \only<1>{\includegraphics[height=0.8\textheight]{mist.jpg}
  }
  \only<2>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_data.pdf}}
  \only<3-4>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_gp.pdf}\\
  }
  \only<3>{Which one is the best?}
  \only<4>{Which one is \sout{the best} compatible with your beliefs?}
\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
  \centering
  \Large{\textbf{Examples}}
\end{frame}
}

\begin{frame}[t]{Multi-stage compressors}
  \begin{columns}
    \column{0.5\paperwidth}
      \centering
      \includegraphics[width=0.8\textwidth]{MANcompressor.png}\\
      \includegraphics[height=0.5\textheight]{impellercfd.png}
    \column{0.5\paperwidth}
    \begin{itemize}
      \item Thermodynamic model: $P_{\text{out}} = P_{\text{in}} \left(1 + \frac{T_{\text{out}} - T_{\text{in}}}{T_{\text{in}}}\right)^{\frac{\gamma}{\gamma -1}}$
      \item Replace impellers by an informed (mechanistic) surrogate model: dimensionally heterogeneous model
      \item Challenge: Model based realtime monitoring (soft-sensing, sensor fusion)
      \item Challenge: Realtime prediction
      \item (working) Solution: Gaussian process regression of CFD results
      \item Estimated x 5'000 speed-up
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}[t]{From windmills to windfarms}
  \framesubtitle{\url{https://engrxiv.org/s43gr/}}
  \centering
  \only<1>{
    \includegraphics[height=0.5\textheight]{windturbine.png}~
    \includegraphics[height=0.5\textheight]{windfarms.png}\\
    \includegraphics[width=0.8\textwidth]{windfarmPOW.png}
  }
  \only<2>{
    \includegraphics[height=0.7\textheight]{windpower_optimal.pdf}
  }
\end{frame}

\begin{frame}[t]{Mechanistic example: marker conservation}
  \vspace{-2em}
  \hspace{-2em}\includegraphics[height=0.3\textheight]{example1.png}

  \vspace{-5.5em}
  \centering
  \[
  M = \int_{0}^T {\dot{m} (t) \ud t} \equiv \texttt{constant}
  \]

  Data: $\bset{t_i, \dot{m}_i - M/T}$

  \includegraphics[width=0.8\textwidth]{invariantGP_cteIntegral_Ginsbourger.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

\end{document}
