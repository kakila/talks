\documentclass[xcolor=dvipsnames]{beamer}

\input{../includes/beamer_basics.tex}

%% --- use packages
\usepackage{abraces,mathtools}
\usetikzlibrary{patterns}
\usetikzlibrary{bayesnet}
\usepackage{adjustbox}
% for notes with pdfpc
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
\setbeameroption{hide notes}
\usepackage{ulem}     % gives ther \sout command


%% Math symbols
\input{../includes/mathsymbols.tex}
\newcommand{\sm}{\scalebox{0.5}{-1}}
\newcommand{\ccov}[1]{{\color{red}k}\left(#1\right)}
\newcommand{\cmean}[1]{{\color{blue}m}\left(#1\right)}
\newcommand{\cgreen}[1]{{\color{red}G}\left(#1\right)}
\newcommand{\cnull}[1]{{\color{blue}n}\left(#1\right)}
\newcommand{\tens}[1]{\bm{\mathtt{#1}\,}}

\DeclareMathOperator*{\gaussian}{\mathcal{N}} % Gaussian distribution
\DeclareMathOperator*{\GP}{\mathcal{G}\!\mathcal{P}} % Gaussian process
\DeclareMathOperator*{\Loss}{Loss} % Loss function

\newcommand{\calert}[1]{{\color{BrickRed}\textbf{#1}}}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@article{Carbajal2016,
archivePrefix = {arXiv},
arxivId = {1609.08395},
author = {Carbajal, Juan Pablo and Leit{\~{a}}o, Jo{\~{a}}o Paulo and Albert, Carlo and Rieckermann, J{\"{o}}rg},
doi = {10.1016/j.envsoft.2017.02.006},
eprint = {1609.08395},
journal = {Env. Mod. {\&} Soft.},
title = {{Appraisal of data-driven and mechanistic emulators of nonlinear simulators: The case of hydrodynamic urban drainage models}},
volume = {92},
year = {2017}
}
@article{Wani2017,
author = {Wani, Omar and Scheidegger, Andreas and Carbajal, Juan Pablo and Rieckermann, J{\"{o}}rg and Blumensaat, Frank},
doi = {10.1016/j.watres.2017.05.038},
journal = {Water Research},
month = {sep},
title = {{Parameter estimation of hydrologic models using a likelihood function for censored and binary observations}},
volume = {121},
year = {2017}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% -----------
% Set Eawag style
%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Emulation (surrogate modeling)}

\author[JPi Carbajal]{Juan Pablo Carbajal
\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{Swisscom brainstorming, Eawag, Switzerland. January 28, 2019}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load background for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load background for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Motivation}
\begin{frame}{Environmental science and engineering}
  \framesubtitle{Model calibration, Fast (extreme) event prediction, \& Design}
  \centering
  \includegraphics[width=0.8\textwidth]{flood_bridge.jpg}

  \source{https://www.dailyrecord.co.uk/news/scottish-news/storm-frank-rescuers-brave-storm-7096067}
\end{frame}

\begin{frame}{Engineered complex systems}
  \framesubtitle{Optimization, Control, \& Maintenance}
  \centering
  \includegraphics[width=0.8\textwidth]{WindFarm_Fluvanna_2004.jpg}

  \source{Leaflet, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=5704247}}
\end{frame}

\begin{frame}{Data fusion/interpretation}
  \centering
  \includegraphics[height=0.4\textheight]
  {PDESmoothing_2_Azzimonti.png}\\
  \includegraphics[height=0.3\textheight]
  {PDESmoothing_1_Azzimonti.png}\\
  \source{Azzimonti et al. 10.1080/01621459.2014.946036}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
% HERE SLIDES WILL BE INSERTED

\begin{frame}{Population driven (observed) inflow}
  \centering
  \includegraphics[width=0.8\textwidth]{DWF.png}\\
  \source{Troxler, P. Msc thesis. ETH Z\"urich, August 22, 2018}
\end{frame}

\begin{frame}{Population driven inflow model}
  \begin{columns}[c]
      \column{.3\textwidth}
        \begin{adjustbox}{width=\textwidth}
        \begin{tikzpicture}
          % Define nodes
          \node[obs]          (q)   {$Q(t)$}; %
          \node[latent, above =of q]          (qp)   {$q_{\text{phys}}$}; %
          \node[latent, below = of q]          (qpo)   {$q_{\text{pop}}$}; %
          \node[obs, right =1 of q]          (c)   {$C(t)$}; %
          % Physical
          \node[latent, above left=1 of qp]  (S)   {$q_{\text{snow}}$}; %
          \node[obs, above right=1 of qp] (T)   {$T$}; %
          \node[obs, above= of qp] (R)   {$q_{\text{rain}}$}; %
          % Weather
          \node[obs, left= of q] (W)   {weather}; %
          % Population
          \node[latent, below= of qpo]  (H)   {hotel}; %
          \node[latent, below right=1 of qpo] (C)   {phone}; %
          \node[latent, below left=1 of qpo] (Ss)   {season}; %
          \node[latent, below =0.5 of H] (E)   {events}; %

          % Connect w and x to the dot node
          \edge[->] {S,T,R} {qp} ;
          \edge[->] {T} {S} ;

          \edge[->,dashed] {H,C} {qpo,c} ;
          \edge[-,dashed] {H,Ss} {C} ;
          \edge[-,dashed] {Ss} {H} ;
          \edge[-,dashed] {E} {H,C} ;

          \edge[->] {qp,qpo} {q} ;
          \edge[->] {q} {c} ;

          \edge[-] {Ss} {W} ;
          \edge[-] {W} {S} ;

        \end{tikzpicture}
        \end{adjustbox}
    \column{.5\textwidth}
      \includegraphics[height=0.8\textwidth]{DWF.png}\\
    \end{columns}
\end{frame}

\begin{frame}{Why are models important}
    \textbf{Probabilistic inference} (traditional statistics / machine learning)
    \begin{itemize}
      \item Associative analysis
      \item Models the distribution of the data: stork population and human birth are correlated
      \item Focus on predicting consequences of observations: predict births based on observation of storks
    \end{itemize}

    \textbf{Causal inference}
    \begin{itemize}
      \item Cause(s)-Effect(s) analysis
      \item Models the mechanism that generates the data: storks do not cause human births
      \item Also allows to predict results of interventions: if we change the number of storks, what will happen with human birth rate?
    \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{What is emulation?}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{What is emulation?}}\\
    \vspace{1em}
    {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
    learned from simulation results/observations at selected inputs.\\
    \vspace{1em}
    \Large{How is emulation even possible?}

    \vspace{1em}
    \small{Simulator: a model providing quantitative results, e.g. numerical model}

  \end{frame}
  }

  \begin{frame}{Emulation}
  \framesubtitle{many names \& similarities ...}
    \begin{tikzpicture}[overlay,remember picture,shift=(current page.north west)]
      \pgfmathsetseed{4}
      \foreach [count=\count] \word in {
                                        Surrogate modeling,
                                        Metamodeling,
                                        Coarse graining,
                                        Model order reduction,
                                        Inter-polation,
                                        Phenom. modeling,
                                        Extra-polation,
                                        Response surface,
                                        Empiric. modeling
                                       }
      {
        \pgfmathparse{round(70+20*rand)}
        \edef\tr{\pgfmathresult}
        \pgfmathparse{round(50+25*rand)}
        \edef\tg{\pgfmathresult}
        \pgfmathparse{round(50+50*rand)}
        \edef\tb{\pgfmathresult}
        \node [
            anchor=west,
            text width=2.5cm,
            xshift=1.5cm,
            yshift=-3cm,
            xshift={mod(\count-1,3)*(\textwidth-2cm)/2},
            yshift={floor((\count-1)/3)*(-(0.9\textheight-4cm))/2},
            xshift=rand*2cm,
            yshift=rand*1cm,
            rotate=rand*15,
            text={rgb:red,\tr;green,\tg;blue,\tb},
            font=\bfseries\large] {\word};
      }
    \end{tikzpicture}

  \end{frame}

\begin{frame}{Numerical model}
  \only<1-2>
  {
   \framesubtitle{Simulator}
  }
  \only<3>
  {
   \framesubtitle{Emulator (TIMTOWTDI)}
  }

  \only<1>{
    \begin{columns}[c]
      \column{.5\textwidth}
        \includegraphics[width=\textwidth]{example1.png}

      \column{.5\textwidth}
      \begin{enumerate}
        \item Inputs (several):
          \begin{itemize}
            \item Rain, e.g. Hyetograph
            \item Initial water levels
            \item Pipe parameters
            \item \dots
          \end{itemize}
        \item Internal states (many)
        \item Outputs (few):
          \begin{itemize}
            \item Water level or flow at marked outlet
          \end{itemize}
      \end{enumerate}
    \end{columns}
  }
  \centering
  \only<2>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<3>{
    \includegraphics[width=0.8\textwidth]{EmulationIdea.png}

    Internal states are lost!

    Another name: Input-Output relations of complex nonlinear systems (linear case: \emph{transfer functions})
  }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Machine Learning}

\input{../includes/learning_from_data.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
% Commented out
%------------------------------------------------------------------------------
\iffalse

\part{Mechanistic emulation}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
{\Large Mechanistic emulation}
\end{frame}
}

\begin{frame}{Mechanistic emulation}
  Mechanistic emulator: fast interpolation/approximation of a slow simulator, learned from simulation results at selected inputs, \calert{that respects given properties} \textbf{related to underlying mechanisms}.

    \begin{itemize}
    \item Energy/Impulse/Mass conservation (e.g. rotor/divergence free fields)
    \item Transformation invariance (e.g. symmetries)
    \item Monotonicity (increasing/decreasing function), Positiveness, \dots
    \item Smoothness, Regularity
    \item Differential equation~\footfullcite{Carbajal2016}
    \end{itemize}
\end{frame}

\input{../includes/intra_extra_inter.tex}

\begin{frame}[t]{Intrapolation}
  \framesubtitle{Emulator behavior "within" the data?}

  \centering
  \only<1>{
  \vspace{-0.5em}
  \includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_data.pdf}
  \\Interpolation or approximation?
  }
  \only<2>{
  \vspace{-0.5em}
  \includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_poly.pdf}
  \\Which one is the best?
  }
  \only<3>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_all.pdf}
  \\\vspace{-3.5em}{\tiny spline $11.7\%$}\\\vspace{-0.5em}{\tiny sinc $1.5\%$}
  }
\end{frame}

\begin{frame}[t]{Extrapolation}
\framesubtitle{Emulator behavior "away" from the data?}
  \vspace{-0.5em}
  \centering
  \only<1>{\includegraphics[height=0.8\textheight]{mist.jpg}
  }
  \only<2>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_data.pdf}}
  \only<3-4>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_gp.pdf}\\
  }
  \only<3>{Which one is the best?}
  \only<4>{Which one is \sout{the best} compatible with your beliefs?}
  %\only<5>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_all.pdf}}
\end{frame}

\begin{frame}{Summary}
   \begin{itemize}
    \item without knowledge (assumptions) about the process, there is no "best" interpolant
    \item knowledge about the process is the "mechanistic" part
    \item mechanistic knowledge "shrinks" the hypothesis set
    \item \dots
    \item intra-/extra- depends on the correlation length!
    \item "within" and "away" from data is unclear in higher dimensions
   \end{itemize}

   Popular methods to include mechanistic knowledge are \textbf{kernel methods}.
   Among them \textbf{Gaussian processes} (krigging and co-krigging)

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Summary}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{Summary}}
  \end{frame}
  }

  \input{../includes/ML_and_mechanistic_modeling.tex}

  \begin{frame}[t]{Emulation at a glance}
    \vspace{-2.5em}
    \begin{center}
      \includegraphics[height=0.8\textheight]{emulation_errorvscost.png}
    \end{center}
    \begin{reference}{2mm}{2.5em}
   Carbajal, JP (2018), Computational effort and emulation error. EmuMore project blog. 
   \url{https://goo.gl/5fTMWs}
  \end{reference}

  \end{frame}
\fi
%------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

\end{document}
