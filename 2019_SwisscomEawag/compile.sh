SOURCE=SwisscomEawag
# Compile
rm *.bib *.bcf *.aux* *.bbl *.nav
pdflatex ${SOURCE}.tex
biber ${SOURCE}
pdflatex ${SOURCE}.tex

# Mix documents
pdftk A=${SOURCE}.pdf B=Troxler_pages.pdf cat A1-4 B A5-end \
output ${SOURCE}_extended.pdf
