% !TEX program = xelatex
\PassOptionsToPackage{usenames,dvipsnames,svgnames}{xcolor}
\documentclass[USenglish]{beamer}
%\documentclass[aspectratio=169,20pt, USenglish]{beamer
%\usetheme{ost}

\usetheme{Madrid}
\usecolortheme[named=MediumVioletRed]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts}

\input{../includes/beamer_basics.tex} % PACKAGES
\input{../includes/mathsymbols.tex}   % SYMBOLS

% Change math fonts
\usefonttheme[onlymath]{serif}

% Show notes
\usepackage{pgfpages}
%\setbeameroption{show notes on second screen=bottom}
\setbeamertemplate{note page}[plain]
\makeatletter
\def\beamer@framenotesbegin{% at beginning of slide
     \usebeamercolor[fg]{normal text}
      \gdef\beamer@noteitems{}%
      \gdef\beamer@notes{}%
}
\makeatother
% do not color links
\hypersetup{colorlinks=false}

% To show videos
\usepackage{multimedia}
\usepackage{graphicx}


\graphicspath{{../img/}}

\title[digital water]{Digital twins \& digitalization of water systems}
\subtitle{OST - Ostschweizer Fachhochschule}
\date{26.10.2021}
\author[JPi Carbajal]{Juan Pablo Carbajal}
\institute[IET]{Institute for Energy Technology}

\begin{document}

    \begin{frame}
        \titlepage
    \end{frame}

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 90 min
    \section{Introduction}
    \begin{frame}{About me ...}
      \centering
      \includegraphics[width=0.9\textwidth]{intro.png}
      \note<1>{
        \begin{quote}
        A narrow expert of the vast generality.\\
        -- Daniel Córdoba, 1996
        \end{quote}

        \begin{quote}
          Nature uses only the longest threads to weave her patterns, so each small
          piece of her fabric reveals the organization of the entire tapestry.\\
          -- Richard P. Feynman
        \end{quote}

      }
    \end{frame}

    \begin{frame}{My past at Eawag}
        \begin{columns}[T]
            \column{0.5\textwidth}
            \begin{itemize}
                \item EmuMore Project: emulation everywhere!
                \item Programming support: FOSS
                \item Inter-dep. collaboration
                \item Training: PCA, git, etc.
            \end{itemize}
            \column{0.5\textwidth}
            \begin{itemize}
                \item Jörg Rieckermann: emulation of flows in pipes
                \item Kris Villez: MOR, PCA classes
                \item Joao Paulo Leitao: optimization sewer networks
                \item Mariane Schneider: SBR sensors ML
            \end{itemize}
        \end{columns}
        \vspace{2em}

        \includegraphics[width=0.13\textwidth]{emu.png}%
        ~\includegraphics[width=0.2\textwidth]{EmulationIdea.png}%
        ~\includegraphics[width=0.35\textwidth]{Intra_extra_inter_polation.png}%
        ~\includegraphics[width=0.27\textwidth]{RC_schema.jpg}
    \end{frame}

    \section{WABEsense}
    \interludeframe{
    {\Huge Digital twins and digitalization}}

    \begin{frame}{Digitial twins}
        \begin{quote}
        Scalable model (data size) with online calibration and data fusion,
        that might capture the target reality at multiple temporal and spatial scales.
        -- JuanPi Carbajal, 2019
        \end{quote}

        models $\subset$ digital twin: it makes reference to \textbf{how} the model(s) is(are) used and implemented.

        The calibrated model $\rightarrow$ a) predictions, b) postdictions, c) counterfactuals.

        Data-driven (observational) models only only do a) and b), physics-based (or causal) models also c), e.g. control, design, etc.

        \vspace{2em}

        \includegraphics[width=0.4\textwidth, trim=3cm 1cm 3cm 0cm, clip]{KVA.png}%
        ~\includegraphics[width=0.3\textwidth]{MANCompressor.png}%
        ~\includegraphics[width=0.25\textwidth]{WindFarm_Fluvanna_2004.jpg}%

        \note<1>{
        It makes no sense to try to differentiate "model" and "digital twin", as the latter encompasses the former.
        That is, "A digital twin has/needs models".
        The digital twin label is linked to how these models are used, how their are calibrated/validated (usually
        online data, note... not all model can or are designed to handle online data) and the relation between the
        digital twin and the reality their are trying to "twin" (neologism through verbalization here!).
        One could also add yet another dimension: scale; digital twins usually "twin" their target at multiple temporal
        and spatial scales, but this can be misleading, as one can have digital twins of very small parts (e.g. predictive maintenance).
        }
    \end{frame}

    \begin{frame}{WABEsense: digitalization of alpine spring captures}
        \framesubtitle{\url{https://hsr-iet.gitlab.io/wabesense/blog/}}
        \centering
        \only<1>{
            \includegraphics[width=0.9\textwidth]{WABE_overview.png}
        }

        \only<2>{
        Open-hardware logger

        \includegraphics[width=0.6\textwidth]{datalogger_zeroSeries_PCB_Bottom_annotated.png}%
        \href{run:/home/juanpi/Scratch/Oberriet_overflow.mp4?loop&noprogress}{
            \includegraphics[height=0.45\textheight]{WABESense_installation.jpg}}
        }


    \end{frame}

    \begin{frame}{WABEsense: CFD}
        \centering
        \only<1>{
        %\movie[loop]{\includegraphics[height=0.7\textheight]{multiview_animation_v1.png}}{/home/juanpi/Scratch/multiview_animation_v1.avi}
        \href{run:/home/juanpi/Scratch/multiview_animation_v1.avi?autostart&loop&noprogress}{
            \includegraphics[width=\textwidth]{multiview_animation_v1.png}}
        }
        \only<2>{
        \includegraphics[width=\textwidth]{WABE_Experiment_vs_Simulation.png}
        }
    \end{frame}

    \begin{frame}{Qsense: emulation of discharges}
        \framesubtitle{\url{https://hsr-iet.gitlab.io/wabesense/cfd_qsense/}}
           Python tool to make easy the simulation of weirs and similar water works.
           \centering
           \includegraphics[width=0.4\textwidth]{Qsense_lateral_weir_annotated.png}%
           \includegraphics[width=0.4\textwidth]{Qsense_DischargeCase.png}

           Example: {\scriptsize\url{https://hsr-iet.gitlab.io/wabesense/cfd_qsense/auto_examples/plot_DischargeCase.html}}
    \end{frame}

    \section{Collaborations}
    \interludeframe{
    {\Huge OST \& Eawag}}

    \begin{frame}{OST/IET \& Eawag/ENG-SWW}
        \begin{columns}[T]
            \column{0.5\textwidth}
            Present:
            \begin{itemize}
                \item Mariane Schneider: Dynamic system modelling SBR equations
                \item Moritz Lürig: clustering of ecological signals
                \item João Leitão, Max Maurer, Mario Schirmer: master thesis
            \end{itemize}
            \column{0.5\textwidth}
            Future:
            \begin{itemize}
                \item Education: complex systems modelling, data science, ML
                \item CFD/PDE modelling and simulation
                \item Scientific ML
            \end{itemize}
        \end{columns}
        \vspace{2em}
        Latest workshop: {\scriptsize \url{http://sciceg.pages.gitlab.ost.ch/teaching/online_learning/}}
    \end{frame}

    \interludeframe{
    {\Huge Thank you for your attention! \\\vspace{2em}
    Q\&A}}


\end{document}
