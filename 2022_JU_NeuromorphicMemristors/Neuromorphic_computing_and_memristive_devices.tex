\RequirePackage[dvipsnames]{xcolor}  % regression in bemer 3.65, 3.66 fixe sit

\documentclass[xcolor=dvipsnames, spanish]{beamer}

\input{../includes/beamer_basics.tex}
\input{../includes/mathsymbols.tex}

\usetheme{Madrid}
\definecolor{OSTpink}{RGB}{140,25,95}
\usecolortheme[named=OSTpink]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts}

\usepackage{pgfpages}
%\setbeameroption{show notes}
% for notes with pdfpc
\setbeameroption{show notes on second screen=right}
\setbeamertemplate{note page}[plain]

\usepackage{multimedia}

% Comments
\newcommand{\jpi}[1]{{\scriptsize\color{Magenta}#1}}

%----------------
% title information

\title[Neuromorph. Comp. \& Memristors]{Neuromorphic Computing and Memristive Devices}
\subtitle{The Krakow Condensed Matter Seminar}

\author[juanpablo.carbajal@ost.ch]{Juan Pablo Carbajal\\\small{\href{mailto:juanpablo.carbajal@ost.ch}{juanpablo.carbajal@ost.ch}}}

\institute[OST-IET-SCE]{Eastern Switzerland University of Applied Sciences OST\\
Institute for Energy Technology IET\\
Scientific Computing and Engineering Group SCE\\
Scientific Machine Learning and Complex Systems SMLCE\\\vspace{0.5em}
The Krakow Condensed Matter Seminar}

\date{December 21, 2022}
% ====================================================================

\begin{document}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
    \vspace{-1em}
    \includegraphics[width=90px]{OSTlogo.png}~%
    \includegraphics[width=50px]{SCE_logo_xborders_acronym.png}\hfill
  \end{frame}
}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

\section{Introduction}

\begin{frame}{Introduction}
\setcounter{tocdepth}{2}
\tableofcontents
\end{frame}

\begin{frame}{About me ...}
  \centering
  \includegraphics[width=\textwidth]{intro.png}
  \note<1>{
    \begin{quote}
    A narrow expert of the vast generality.\\
    -- Daniel Córdoba, 1996
    \end{quote}

    \begin{quote}
      Nature uses only the longest threads to weave her patterns, so each
      piece of her fabric reveals the organization of the entire tapestry.\\
      -- Richard P. Feynman
    \end{quote}
  }
\end{frame}

\section{Challenges for transistors}
\input{../includes/transistor_limits.tex}

\section{Analog Computation}
\interludeframe{\textbf{\Huge Analog Computation}\\\vspace{2em}

  \begin{quote}
  \color{white}{Turing Machine theory is not wrong but is irrelevant.\\
  -- B.J. MacLennan, 2001}
  \end{quote}
  
  \note<1>{
  In order to pin down the concept of analog computation we compare analog and digital computers,
assuming that the latter is familiar to most readers.
The principal distinction between analog and digital computers is that digital operates on discrete representations in discrete steps, while analog operates on continuous representations, i.e. discrete vs. continuous computation.

  In the current digital computer, computation is carried out using strings of symbols that bare no physical relationship to the quantities of interest.
  In analog computers the latter are directly related to the physical quantities used in the computation.
  }
}

\input{../includes/Turing_machine.tex}

\input{../includes/von_neumann_arch.tex}

\interludeframe{
    \movie[loop]{\includegraphics[width=\textwidth]{game_of_life.png}}{../img/game_of_life.avi}
    \source{\url{www.youtube.com/watch?v=-FaqC4h5Ftg}}
    \note<1>{
    El juego de la vida de Conway ("game of life") es una máquina de Turing que, a pesar de la simpleza de sus reglas, puede producir comportamientos complicados (cualquier función!)

    \begin{enumerate}
    \item Una célula muerta con exactamente 3 células vecinas estará viva en el turno siguiente.
    \item Una célula viva con 2 o 3 células vecinas vivas sigue viva
    \item Toda célula viva en otra condición muere y toda célula muerta en otra condición permanece muerta.
    \end{enumerate}
    }
}

\input{../includes/analog_computers_mechanical.tex}

\input{../includes/analog_computers_electr.tex}

\input{../includes/analog_computers_diffanal_shannon.tex}

\input{../includes/natural_contrived_systems.tex}

\section{Memristors}

\interludeframe{\textbf{\Huge Memristors}

\begin{reference}{0.5cm}{2cm}
\color{white}
\flushleft
Caravelli , F. \& Carbajal, J. P. (2018). Memristors for the Curious Outsiders. DOI: 10.3390/technologies6040118
\end{reference}

}

\input{../includes/memristors_types.tex}

\input{../includes/memristors_hp_strukov.tex}

\input{../includes/memristors_systems.tex}

\section{Neuromorphic hardware \& Memristors}

\interludeframe{
\textbf{\Huge Neuromorphic hardware}

\begin{reference}{0.5cm}{2cm}
\color{white}
\flushleft
Mead, C. A. (1989). Analog VLSI and Neural Systems. Reading, MA: Addison-Wesley.

Mead, C. A. (1990). Neuromorphic electronic systems. Proc. IEEE 78, 1629–1636.

\end{reference}

\note<1>{The term "neuromorphic" was coined by Carver Mead in the late 80s to refer to artificial neural systems whose architecture and design principles are based on those of biological nervous systems


The observation that the brain operates on analog principles of the physics of neural computation that are fundamentally different from digital principles in traditional computing, initiated the investigations in the field of neuromorphic engineering.

The analogies between biological neuronal systems and electronic circuits are manifold: conservation of charge, thresholding behavior, and integration to mention just a few. 
For instance, diffusion of calcium across the membrane can be mapped to diffusion in electronic components, and a computational role associated to the circuit design. 
}
}

\input{../includes/neuromorphic_silicon_neurons.tex}

\input{../includes/memristors_galore.tex}

\input{../includes/memristors_edge_computing.tex}

\section{Supervised ML}
\interludeframe{
\textbf{\Huge Supervised\\Machine Learning}
}

\input{../includes/cardgame_en.tex}

\input{../includes/computation_Carbajal.tex}

\input{../includes/computation_Horsman_single.tex}

\input{../includes/memristors_mistakes.tex}

\input{../includes/memristors_mistakes_results.tex}

\input{../includes/ml_reservoir_diagram.tex}

\input{../includes/ml_input_output_maps.tex}

\input{../includes/ml_reservoir.tex}

\section{Summary \& Outlook}

\begin{frame}{Summary \& Outlook}
  \begin{itemize}
    \item Analog computers revival: physical limits, silicon neurons, memristors
    \item En(De)coding is fundamental: "natural basis of computation", "language of the computer".\\
      Given a physical system, what's the best encoding to solve problems?
    \item Un(Semi)supervised ML: Hopfield nets, SOM \textrightarrow F. Caravelli
    \item Why would nature prefer "linear algebra"? intrinsic non-linear methods
    \item Algorithms that exploit device variability and noise
  \end{itemize}
\end{frame}

\interludeframe{
{\Huge Thank you!\\\vspace{1em}Q\&A}
}

\end{document}
