#!/bin/bash

pdflatex NeuromorphicMemristors.tex
pdflatex NeuromorphicMemristors.tex

# handout
pdftk NeuromorphicMemristors.pdf cat 1-8 13-18 22-end output NeuromorphicMemristors_handout.pdf
