#!/usr/bin/env bash

# Set loopback ot record microphone and spoeakers
pactl load-module module-loopback

# Set noice cancellation
pactl unload-module module-echo-cancel
pactl load-module module-echo-cancel aec_method=webrtc source_name=echocancel sink_name=echocancel1
pacmd set-default-source echocancel
pacmd set-default-sink echocancel1

# Launch screen recorder
simplescreenrecorder &

# Launch presenter
/opt/pdfpc/bin/pdfpc --windowed=both -n right NeuromorphicMemristors.pdf

# Remove loopback
pactl unload-module module-loopback
# Remove noice cancellation
pactl unload-module module-echo-cancel
