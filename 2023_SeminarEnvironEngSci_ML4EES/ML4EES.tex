\documentclass[usenames,dvipsnames,USenglish]{beamer}
\input{../includes/beamer_basics.tex} % PACKAGES
\input{../includes/mathsymbols.tex}   % SYMBOLS

\usetheme{Madrid}
\definecolor{OSTpink}{RGB}{140,25,95}
\usecolortheme[named=OSTpink]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts}

\usepackage{pgfpages}
%\setbeameroption{show notes}
% for notes with pdfpc
\setbeameroption{show notes on second screen=right}
\setbeamertemplate{note page}[plain]

\usepackage{multimedia}

%----------------
% title information
\title{Machine Learning methods}
\subtitle{Seminar Environmental Engineering and Science}

\author[juanpablo.carbajal@ost.ch]{Juan Pablo Carbajal\\\small{\href{mailto:juanpablo.carbajal@ost.ch}{juanpablo.carbajal@ost.ch}}}

\institute[OST-IET-SCE]{Eastern Switzerland University of Applied Sciences OST\\
Institute for Energy Technology IET\\
Scientific Computing and Engineering Group SCE\\
Scientific Machine Learning and Complex Systems SciML\&CS}

\date[23.03.2023]{March 23, 2022}
% ====================================================================

\begin{document}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
    \vspace{-1em}
    \includegraphics[width=90px]{ost-logo-en.png}~%
    \includegraphics[width=90px]{iet_logo.jpg}~
    \includegraphics[width=40px]{sce_logo_xborders_acronym.png}
  \end{frame}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------
\section{Introduction}

  \input{../includes/JPi_profile.tex}

\subsection{Challenges}
  \input{../includes/challenges_in_ESE.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%----------------
\section{Learning from data}
  \interludeframe{
    \Large{\textbf{Learning from data}}\\
    \Large{Machine learning (supervised)}
  }
  \input{../includes/learning_from_data.tex}

\subsection{intra-, extra-polation}
  \input{../includes/intra_extra_inter.tex}
  \input{../includes/intrapolation.tex}
  \input{../includes/extrapolation.tex}

\subsection{polynomial interpolation}
  \input{../includes/polynomial_interpolation.tex}
  \input{../includes/polynomial_interpolation_samples.tex}

\subsection{regression}
  \input{../includes/regularized_regression.tex}
  \input{../includes/statistical_learning.tex}
  \input{../includes/regularization_operator.tex}
  \input{../includes/gp_regression.tex}
  \input{../includes/gp_rr_comparison.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%----------------
\section{(Sci)ML}
  \interludeframe{
    \Large{\textbf{Machine learning: functional relations in data (regression)}}\\
    \Large{\textbf{Where is the \alert{science} (models)?}}
  }
  \input{../includes/guessing_sample_model.tex}
  \input{../includes/sce_logo.tex}
  \input{../includes/sciml_infographic.tex}
  \input{../includes/sciml_approaches.tex}
  \input{../includes/sciml_approaches_example.tex}
  \input{../includes/models.tex}
  \input{../includes/constrained_regularized_regression.tex}
  \input{../includes/sciml_topics.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[t]{Recap: so far ...}
    \centering
    \begin{itemize}
      \item Machine learning (data driven) finds relations in the data
      \item Constrains and regularization reduce the effective size of the hypothesis set (function class, CV dim.):
            less data, better generalization (for the same distribution, i.e. intrapolation)
      \item Regularized regression and GP are strongly related
      \item Scientific constrains and regularization, i. e. models (SciML):
            interpretability, easier DoE, better extrapolation
    \end{itemize}

    What about the challenges?
  \end{frame}

\section{Challenges revisited}
  \interludeframe{\Large{\textbf{Challenges revisited}}
    \vspace{1em}
    \Large{complex problems, sparse/scarce data, simulation-hungry solutions}
  }

  \input{../includes/model-predictive-realtime-control.tex}
  \input{../includes/system-identification.tex}
  \input{../includes/emulation_optimization.tex}
  \input{../includes/models_combinations.tex}

  \input{../includes/emulation_shallow_water.tex}
  \input{../includes/emulation_shallow_water_plot_wartegg.tex}

%%----------------
\section{Emulation}
  \interludeframe{
    \Large{\textbf{What is emulation?}}\\
    \vspace{1em}
    {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
    learned from simulation results at selected inputs.\\
    \vspace{1em}
    \Large{How is emulation even possible?}

    \vspace{1em}
    \small{Simulator: a model providing quantitative results, e.g. numerical model}
  }

  \input{../includes/molecular_spring.tex}
  \input{../includes/from-molecules-to-fluids.tex}
  \input{../includes/emulation_names.tex}

\subsection{Speeding up simulators}
  \interludeframe{\Large{\textbf{Speeding-up simulators}}}

  \input{../includes/emulation_numerical_model.tex}
  \input{../includes/emulation_numerical_model_states.tex}

  \input{../includes/simulation-speed-up-tools.tex}

  \input{../includes/emulation_numerical_model_states.tex}
  \input{../includes/model-order-reduction.tex}
  \input{../includes/model-order-reduction_constriction.tex}

  \input{../includes/emulation_numerical_model_states.tex}
  \input{../includes/emulation_idea.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%----------------
\section{pre-Emulation}
  \interludeframe{\Large{\textbf{Before jumping into emulation}}}

  \input{../includes/models_dimensionless.tex}
  \input{../includes/exploratory-data-analysis.tex}
  \input{../includes/emulation_task.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%----------------
\section{Summary}
  \interludeframe{\Large{\textbf{Emulation summary}}
  }

  \input{../includes/emulation_recap.tex}
  \input{../includes/emulation_decision_flowchart.tex}
  \input{../includes/emulation_error_vs_cost.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%----------------
\section{Conclusion}
  \interludeframe{\Large{\textbf{Thank you!}}\\\Large{\textbf{Q \& A}}
  }

\section{Further material}
\subsection{GP, RR, and Kalman filter}
  \input{../includes/gp_rr_relation.tex}
  \input{../includes/kf_regression.tex}
  \input{../includes/gp_kf_comparison.tex}

\end{document}
