\RequirePackage[dvipsnames]{xcolor}  % regression in bemer 3.65, 3.66 fixe sit

\documentclass[xcolor=dvipsnames,german]{beamer}

\input{../includes/beamer_basics.tex}
\input{../includes/mathsymbols.tex}

\usetheme{Madrid}
\definecolor{OSTpink}{RGB}{140,25,95}
\usecolortheme[named=OSTpink]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts}

\usepackage{pgfpages}
\setbeameroption{show notes}
% for notes with pdfpc
\setbeameroption{show notes on second screen=right}
\setbeamertemplate{note page}[plain]

\usepackage{multimedia}

% Comments
\newcommand{\jpi}[1]{{\scriptsize\color{Magenta}#1}}

%----------------
% title information

\title[Einfach KI]{Konzeptueller Überblick der künstlichen Intelligenz}
\subtitle{Chatbots: Vorteile und Risiken}

\author[juanpablo.carbajal@ost.ch]{Juan Pablo Carbajal\\\small{\href{mailto:juanpablo.carbajal@ost.ch}{juanpablo.carbajal@ost.ch}}}

\institute[OST-IET-SCE]{OST Ostschweizer Fachhochschule\\
IET Institut für Energietechnik\\
SCE Scientific Computing and Engineering Group\\
SMLCE Scientific Machine Learning and Complex Systems\\\vspace{0.5em}
Soroptimistinnen Abendessen}

\date{Januar 16, 2024}
% ====================================================================

\begin{document}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
    \vspace{-1em}
    \center
    \includegraphics[width=120px]{ost-logo-en.png}~%
    \includegraphics[width=110px]{iet_logo.jpg}~%
    \includegraphics[width=50px]{SCE_logo_xborders_acronym.png}%
  \end{frame}
}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

\section{Was ist KI? Was ist I?}

\begin{frame}{Was ist KI? Was ist I?}
  \vspace{-1em}
  \begin{center}
   \includegraphics[height=0.4\textheight]{thinking_on_brain.png}
  \end{center}
  \vspace{-1em}
  
  \begin{block}{Künstliche Intelligenz (KI)}
   Intelligenz, die von Maschinen oder Software gezeigt wird.
  \end{block}
  \begin{alertblock}{Künstliche allgemeine Intelligenz (AGI)}
  Ein hypothetischer intelligenter Agent, der jede intellektuelle Aufgabe, die Menschen oder Tiere ausführen können, verstehen oder erlernen kann.
  \end{alertblock}
  
  Was ist Intelligenz? Immer noch eine offene Frage.

  Wichtig: AGI gibt es noch nicht.


\end{frame}

\input{../includes/cardgame_de.tex}

\interludeframe{
  {\Huge Die Kodierung ist sehr wichtig!}
  \vspace{1em}
  
  Eine schlechte Kodierung (Sprache/Darstellung) kann das Problem schwieriger machen, als es sein könnte.

  \vspace{1em}
  Kodierung|Enkoder \textrightarrow Einbettung (engl. Embbeding)
}

\input{../includes/character_identity_de.tex}

\input{../includes/sentence_meaning_de.tex}

\interludeframe{
  {\Huge Der Kontext ist sehr wichtig!}
  \vspace{1em}
  
  Der Kontext bestimmt das Verständnis einer Äusserung mit.

  \vspace{1em}
  Kontext \textrightarrow Kontextfenster (z.B. GPT-4 $\sim$ 12 Seiten auf Englisch)
}

\section{LLM chatBots}

\interludeframe{
  \centering
  {\Huge LLM (Grosse Sprachmodelle) Chatbots}
}

\begin{frame}{LLM ChatBots}
  Kategorie: Natürliche Sprachverarbeitung
  
  \begin{alertblock}{Symbolisch (1950er - 1990er)}
  Komplexe manuell erstellte Regelwerke, die durch linguistische Modelle definiert sind.
  
  - viel manuelle Arbeit; + nachvollziehbar; - unflexibel;
  \end{alertblock}
  
  \begin{block}{Statistisch (1990er - 2010er)}
  Wortstatistiken, um die Wahrscheinlichkeit von Wortfolgen zu erzeugen.
  
  - viel manuelle Arbeit; $\pm$ nachvollziehbar; $\pm$ flexibel
  \end{block}
  
  \begin{exampleblock}{(Künstliche) neuronale Netze (aktuell)}
  Wortstatistiken und Kontextbehandlung (Relevanz, Fokus, \dots), um die Wahrscheinlichkeit von Wortfolgen zu erzeugen.
  
  + Automatisiert; - nachvollziehbar; + flexibel;
  \end{exampleblock}

\end{frame}

\subsection{Künstliche neuronale Netze}

\begin{frame}{Künstliche neuronale Netze}{Das Konzept}

  \begin{block}{Künstliche neuronale Netze, kurz: KNN (engl. artificial neural network, ANN)}
  Komplexe mathematische Funktion, die lose auf der bekannten Funktionsweise biologischer Neuronen basiert.
  \end{block}
  
  Aus vielen Elementen zusammengesetzt:
  \vspace{0.5em}
  
   \includegraphics[width=0.5\textwidth,trim=37px 190px 150px 0px, clip]{Memneuron_2.png}~%
   \includegraphics[width=0.5\textwidth]{NeuronModel_deutsch.png}\\\vspace{-0.5em}
   \hfill\source{\href{https://commons.wikimedia.org/wiki/File:NeuronModel_deutsch.svg}{Wikimeadia Commons}}

  Erste Implementierung $\sim$ 1958 (Perceptron), "unendliche" Variationen seither.

\end{frame}

\begin{frame}{Künstliche neuronale Netze}{Grafische Darstellung}
  \begin{columns}
  \column{0.5\textwidth}
   \includegraphics[width=\textwidth]{ANN_image_recognition.png}\\\vspace{-0.5em}
  \source{\href{https://commons.wikimedia.org/w/index.php?curid=61785275}{Wikimeadia Commons file By Cyberbotics Ltd.}}
  
  \column{0.5\textwidth}
  \includegraphics[height=0.8\textheight]{NeuralNetworkZoo.png}
  \end{columns}
\end{frame}

\begin{frame}{Künstliche neuronale Netze}{GPT Modelle}
  \centering
  \url{https://bbycroft.net/llm}

  \includegraphics[height=0.7\textheight]{uGPT.png}
  
  GPT-3 > 100'000'000'000 Parameter
  
\end{frame}

\section{Vorteile und Risiken}

\interludeframe{
  \centering
  {\Huge Vorteile und Risiken \vspace{0.5em}\\ bei der Nutzung von LLMs}
}


%\begin{frame}{Vorhersehbares existenzielles Risiko}
%control and aligment
%paper clip
%\end{frame}

\begin{frame}{Nutzung von LLMs}{menschliche Fähigkeiten}
  Deskilling (Dequalifizierung) oder Qualifikationsänderung:
  \vspace{1em}

  \begin{quote}
      \dots Woher wissen Sie, dass das wahr ist? Das frage ich meine Schüler. Weil ChatGPT es gesagt hat, antworten Sie.\\-- D. Neal. "Oh No, Not Another Essay On ChatGPT". 3quarksdaily.com, Mai 2023
  \end{quote}

  \begin{center}
  \includegraphics[width=0.5\textwidth]{augmented_gerd.png}\\\vspace{-0.7em}
  \source{Gerd Leohnard}
  \end{center}
\end{frame}


\interludeframe{
  \centering
  \includegraphics[width=\textwidth]{chatgpt_n_letter.png}
}

\begin{frame}{Nutzung von LLMs}{Routineaufgaben}
  \emph{Nie wieder langweilige Aufgaben!}
  \begin{itemize}
  \item Bewerbungsschreiben verfassen
  \item Jobangebote, die Ihren Interessen entsprechen
  \item Inhalten für E-Commerce-Website
  \item E-Mail-Marketing (Spam!)
  \item Persönlicher Reiseplaner und Reiseführer
  \item Durchführen einer Grammatikprüfung
  \item \dots
  \end{itemize}

  \emph{Andere Aufgaben (Achtung)}
  \begin{itemize}
  \item Als Berater für Start-up fungieren
  \item Einfache Beantwortung von Kundenbeschwerden
  \item Telemedizinische Sofort-Beratung rund um die Uhr
  \item \dots
  \end{itemize}
\end{frame}

\interludeframe{
  \centering
  \includegraphics[width=\textwidth]{Midjourney_prompt.png}
}



\begin{frame}{Nutzung von LLMs}{Interessenkonflikt und Freiheit}
\centering
\only<1>{
\includegraphics[width=0.7\textwidth]{monetize_AI.png}
}

\only<2>{
\includegraphics[height=0.7\textheight]{KI_NZZ.png}
\source{www.nzz.ch/meinung/chatgpt-wer-die-ki-kontrolliert-beherrscht-kuenftig-die-politik-ld.1734342}
}

\end{frame}


\begin{frame}{Nutzung von LLMs}{Umwelt}
  \begin{center}
    \includegraphics[width=0.4\textwidth]{AI_energy_impact.png}
  \end{center}

  Erstellung ein LLM $\sim$ \textbf{300 Tonnen CO2} 
  
  vgl. \textbf{5 Tonnen CO2} pro Person im Jahr.
  \vspace{1em}
  
  Ist es das wert? Sollte jeder den Preis für ein lustiges Bild oder einen cleveren Text zahlen?
  \vspace{1em}

  {\scriptsize
  \begin{itemize}
  \item M. Heikkiläarchive (2023). "Making an image with generative AI uses as much energy as charging your phone". MIT Technology Review, www.technologyreview.com
  \item A. Kumar, T. Davenport (2023). "How to Make Generative AI Greener". Hardward Business Review, hbr.org
  \end{itemize}
  }
\end{frame}

\interludeframe{
  \center
  \textbf{\Huge Vielen Dank!}
  \vspace{0.5em}
  
  \textbf{\Large für Ihre Zeit und Aufmerksamkeit}
}

\end{document}
