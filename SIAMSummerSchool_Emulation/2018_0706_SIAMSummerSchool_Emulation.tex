\documentclass[xcolor=dvipsnames]{beamer}

\input{../includes/beamer_basics.tex}

%% --- use packages
\usepackage{abraces,mathtools}
\usetikzlibrary{patterns}
% for notes with pdfpc
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
\setbeameroption{hide notes}
\usepackage{ulem}     % gives ther \sout command


%% Math symbols
\input{../includes/mathsymbols.tex}
\newcommand{\sm}{\scalebox{0.5}{-1}}
\newcommand{\ccov}[1]{{\color{red}k}\left(#1\right)}
\newcommand{\cmean}[1]{{\color{blue}m}\left(#1\right)}
\newcommand{\cgreen}[1]{{\color{red}G}\left(#1\right)}
\newcommand{\cnull}[1]{{\color{blue}n}\left(#1\right)}
\newcommand{\tens}[1]{\bm{\mathtt{#1}\,}}

\DeclareMathOperator*{\gaussian}{\mathcal{N}} % Gaussian distribution
\DeclareMathOperator*{\GP}{\mathcal{G}\!\mathcal{P}} % Gaussian process
\DeclareMathOperator*{\Loss}{Loss} % Loss function

\newcommand{\calert}[1]{{\color{BrickRed}\textbf{#1}}}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@article{Carbajal2016,
archivePrefix = {arXiv},
arxivId = {1609.08395},
author = {Carbajal, Juan Pablo and Leit{\~{a}}o, Jo{\~{a}}o Paulo and Albert, Carlo and Rieckermann, J{\"{o}}rg},
doi = {10.1016/j.envsoft.2017.02.006},
eprint = {1609.08395},
journal = {Env. Mod. {\&} Soft.},
title = {{Appraisal of data-driven and mechanistic emulators of nonlinear simulators: The case of hydrodynamic urban drainage models}},
volume = {92},
year = {2017}
}
@article{Wani2017,
author = {Wani, Omar and Scheidegger, Andreas and Carbajal, Juan Pablo and Rieckermann, J{\"{o}}rg and Blumensaat, Frank},
doi = {10.1016/j.watres.2017.05.038},
journal = {Water Research},
month = {sep},
title = {{Parameter estimation of hydrologic models using a likelihood function for censored and binary observations}},
volume = {121},
year = {2017}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% -----------
% Set Eawag style
%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Emulation (surrogate modeling)}

\author[JPi Carbajal]{Juan Pablo Carbajal
\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{SIAM Summer School, Eawag, Switzerland. June 7, 2018}
%\date{\today}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load background for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load background for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

%%----------------
%\part{Speaker's profile}
%\begin{frame}
%  \frametitle{Background}
%  \centering
%  \includegraphics[width=\textwidth]{intro.png}
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Motivation}
\begin{frame}{Environmental science and engineering}
  \framesubtitle{Model calibration, Fast (extreme) event prediction, \& Design}
  \centering
  \includegraphics[width=0.8\textwidth]{flood_bridge.jpg}

  \source{https://www.dailyrecord.co.uk/news/scottish-news/storm-frank-rescuers-brave-storm-7096067}
\end{frame}

\begin{frame}{Engineered complex systems}
  \framesubtitle{Optimization, Control, \& Maintenance}
  \centering
  \includegraphics[width=0.8\textwidth]{WindFarm_Fluvanna_2004.jpg}

  \source{Leaflet, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=5704247}}
\end{frame}

\begin{frame}{Data fusion/interpretation}
  \centering
  \includegraphics[height=0.4\textheight]
  {PDESmoothing_2_Azzimonti.png}\\
  \includegraphics[height=0.3\textheight]
  {PDESmoothing_1_Azzimonti.png}\\
  \source{Azzimonti et al. 10.1080/01621459.2014.946036}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{What is emulation?}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{What is emulation?}}\\
    \vspace{1em}
    {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
    learned from simulation results at selected inputs.\\
    \vspace{1em}
    \Large{How is emulation even possible?}

    \vspace{1em}
    \small{Simulator: a model providing quantitative results, e.g. numerical model}

  \end{frame}
  }

  \begin{frame}{Guessing from samples}
    \centering
    \begin{columns}[T]
      \column{.5\textwidth}
        \only<1-2>{\includegraphics[height=0.7\textheight]{horse_learning_1.png}}
        \only<3>{\includegraphics[height=0.7\textheight]{horse_learning_2.png}}
      \column{.5\textwidth}
        \begin{itemize}
          \item<1-3> What's in the picture?
          \item<2-3> Is anybody riding it?
        \end{itemize}
    \end{columns}
  \end{frame}

  \begin{frame}{Molecular spring}
    \centering
    \begin{columns}[T]
      \column{.5\textwidth}
        \includegraphics[height=0.7\textheight]{molecule_spring.png}
      \column{.5\textwidth}
        \includegraphics[height=0.6\textheight]{HookesLaw.png}\\
        \source{Svjo, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=25795521}}
    \end{columns}
  \end{frame}

  \begin{frame}[t]{From molecules to fluids}
    \centering
      \href{run:../img/Pseudofluids_Chipmunk.avi?autostart&loop&noprogress}%
       {\includegraphics[height=0.8\textheight]{pseudofluids.png}}
  \end{frame}

  \begin{frame}{Emulation}
  \framesubtitle{many names \& similarities ...}
    \begin{tikzpicture}[overlay,remember picture,shift=(current page.north west)]
      \pgfmathsetseed{4}
      \foreach [count=\count] \word in {
                                        Surrogate modeling,
                                        Metamodeling,
                                        Coarse graining,
                                        Model order reduction,
                                        Inter-polation,
                                        Phenom. modeling,
                                        Extra-polation,
                                        Response surface,
                                        Empiric. modeling
                                       }
      {
        \pgfmathparse{round(70+20*rand)}
        \edef\tr{\pgfmathresult}
        \pgfmathparse{round(50+25*rand)}
        \edef\tg{\pgfmathresult}
        \pgfmathparse{round(50+50*rand)}
        \edef\tb{\pgfmathresult}
        \node [
            anchor=west,
            text width=2.5cm,
            xshift=1.5cm,
            yshift=-3cm,
            xshift={mod(\count-1,3)*(\textwidth-2cm)/2},
            yshift={floor((\count-1)/3)*(-(0.9\textheight-4cm))/2},
            xshift=rand*2cm,
            yshift=rand*1cm,
            rotate=rand*15,
            text={rgb:red,\tr;green,\tg;blue,\tb},
            font=\bfseries\large] {\word};
      }
    \end{tikzpicture}

  \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{How-to Emulate}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{How to emulate?}}\\
    \vspace{1em}
    {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
    learned from simulation results at selected inputs.\\
  \end{frame}
  }

\section{Recipes}
\begin{frame}{pre-Emulation flowchart}
  \begin{center}
  \begin{tikzpicture}[node distance=2cm]

  \node (problem)[startstop]{Task: DoE, SI, RTC, \dots};
  \node (model)[decision, right of=problem,xshift=2cm]{Simulator?};
  \node (modeling)[process, right of=model, xshift=2cm]{Build a simulator};
  \node (dataset)[decision, yshift=-0.5cm, below of=model]{Dataset?};
  \node (dataseting)[process, right of=dataset, xshift=2cm]{Assemble dataset};
  \node (stop)[startstop, below of=dataset, yshift=-0.5cm]{Emulate};

  \draw [arrow] (problem) -- (model);
  \draw [arrow] (model) -- node[anchor=south]{no} (modeling);
  \draw [arrow] (model) -- node[anchor=east]{yes} (dataset);
  \draw [arrow] (dataset) -- node[anchor=south]{no} (dataseting);
  \draw [arrow] (dataset) -- node[anchor=east]{yes} (stop);
  \draw [arrow] (modeling) -- (dataseting);
  \draw [arrow] (dataseting) |- (stop);

  \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Numerical model}
  \only<1-2>
  {
   \framesubtitle{Simulator}
  }
  \only<3>
  {
   \framesubtitle{Emulator (TIMTOWTDI)}
  }

  \only<1>{
    \begin{columns}[c]
      \column{.5\textwidth}
        \includegraphics[width=\textwidth]{example1.png}

      \column{.5\textwidth}
      \begin{enumerate}
        \item Inputs (several):
          \begin{itemize}
            \item Rain, e.g. Hyetograph
            \item Initial water levels
            \item Pipe parameters
            \item \dots
          \end{itemize}
        \item Internal states (many)
        \item Outputs (few):
          \begin{itemize}
            \item Water level or flow at marked outlet
          \end{itemize}
      \end{enumerate}
    \end{columns}
  }
  \centering
  \only<2>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<3>{
    \includegraphics[width=0.8\textwidth]{EmulationIdea.png}

    Internal states are lost!

    Another name: Input-Output relations of complex nonlinear systems (linear case: \emph{transfer functions})
  }
\end{frame}

\begin{frame}{Cooking recipe}
A simple procedure for emulation:

{
  \scriptsize
  \begin{enumerate}
  \item \label{dataset} Build a dataset by running simulations on relevant inputs
  \item \label{hypothesis} Select a model to "fit" the dataset (e.g. GP, ANN, etc.). \calert{Hypothesis set}
  \item \label{split} Split your dataset into \calert{training} and \calert{test} set
  \item \label{train} "Fit" the data in the training set with your model (not an estimation of performance)
  \item \label{test} Test the model in the test set (this is an estimation of performance)
  \item Replace your simulator with the emulator, and solve the task
  \item Verify
  \end{enumerate}
}
Usually we iterate steps \ref{hypothesis}~--~\ref{test} (resampling), we need a third
set for performance estimation that we never (\calert{ever}) use in the
iteration: \calert{validation} set.

\vspace{0.5em}
Step \ref{dataset} might be included in the iteration: optimal design of (numerical) experiments.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Machine Learning}

\input{../includes/learning_from_data.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Mechanistic emulation}
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
{\Large Mechanistic emulation}
\end{frame}
}

\begin{frame}{Mechanistic emulation}
  Mechanistic emulator: fast interpolation/approximation of a slow simulator, learned from simulation results at selected inputs, \calert{that respects given properties} \textbf{related to underlying mechanisms}.

    \begin{itemize}
    \item Energy/Impulse/Mass conservation (e.g. rotor/divergence free fields)
    \item Transformation invariance (e.g. symmetries)
    \item Monotonicity (increasing/decreasing function), Positiveness, \dots
    \item Smoothness, Regularity
    \item Differential equation~\footfullcite{Carbajal2016}
    \end{itemize}
\end{frame}

\input{../includes/intra_extra_inter.tex}

\begin{frame}[t]{Intrapolation}
  \framesubtitle{Emulator behavior "within" the data?}

  \centering
  \only<1>{
  \vspace{-0.5em}
  \includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_data.pdf}
  \\Interpolation or approximation?
  }
  \only<2>{
  \vspace{-0.5em}
  \includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_poly.pdf}
  \\Which one is the best?
  }
  \only<3>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_all.pdf}
  \\\vspace{-3.5em}{\tiny spline $11.7\%$}\\\vspace{-0.5em}{\tiny sinc $1.5\%$}
  }
\end{frame}

\begin{frame}[t]{Extrapolation}
\framesubtitle{Emulator behavior "away" from the data?}
  \vspace{-0.5em}
  \centering
  \only<1>{\includegraphics[height=0.8\textheight]{mist.jpg}
  }
  \only<2>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_data.pdf}}
  \only<3-4>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_gp.pdf}\\
  }
  \only<3>{Which one is the best?}
  \only<4>{Which one is \sout{the best} compatible with your beliefs?}
  %\only<5>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_all.pdf}}
\end{frame}

\begin{frame}{Summary}
   \begin{itemize}
    \item without knowledge (assumptions) about the process, there is no "best" interpolant
    \item knowledge about the process is the "mechanistic" part
    \item mechanistic knowledge "shrinks" the hypothesis set
    \item \dots
    \item intra-/extra- depends on the correlation length!
    \item "within" and "away" from data is unclear in higher dimensions
   \end{itemize}

   Popular methods to include mechanistic knowledge are \textbf{kernel methods}.
   Among them \textbf{Gaussian processes} (krigging and co-krigging)

\end{frame}

\input{../includes/mvnormal_distribution.tex}

\input{../includes/gp_regression.tex}

%\input{../includes/constrained_regularized_regression.tex}
\section{Examples}

\begin{frame}[t]{Mechanistic example: marker conservation}
  \vspace{-2em}
  \hspace{-2em}\includegraphics[height=0.3\textheight]{example1.png}

  \vspace{-5.5em}
  \centering
  \[
  M = \int_{0}^T {\dot{m} (t) \ud t} \equiv \texttt{constant}
  \]

  Data: $\bset{t_i, \dot{m}_i - M/T}$

  \includegraphics[width=0.8\textwidth]{invariantGP_cteIntegral_Ginsbourger.png}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Summary}
  {
  \setbeamertemplate{background}{}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \usebeamercolor[fg]{normal text}
  \begin{frame}[plain]
    \centering
    \Large{\textbf{Summary}}
  \end{frame}
  }

  \input{../includes/ML_and_mechanistic_modeling.tex}

  \begin{frame}[t]{Emulation at a glance}
    \vspace{-2.5em}
    \begin{center}
      \includegraphics[height=0.8\textheight]{emulation_errorvscost.png}
    \end{center}
    \begin{reference}{2mm}{2.5em}
   Carbajal, JP (2018), Computational effort and emulation error. EmuMore project blog. 
   \url{https://goo.gl/5fTMWs}
  \end{reference}

  \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

\end{document}
