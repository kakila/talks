\documentclass[xcolor=dvipsnames]{beamer}

\input{../includes/beamer_basics.tex}

%% --- use packages
\usepackage{abraces,mathtools}
\usetikzlibrary{patterns}
% for notes with pdfpc
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
\setbeameroption{hide notes}
\usepackage{ulem}     % gives ther \sout command


%% Math symbols
\input{../includes/mathsymbols.tex}
\newcommand{\sm}{\scalebox{0.5}{-1}}
\newcommand{\ccov}[1]{{\color{red}k}\left(#1\right)}
\newcommand{\cmean}[1]{{\color{blue}m}\left(#1\right)}
\newcommand{\cgreen}[1]{{\color{red}G}\left(#1\right)}
\newcommand{\cnull}[1]{{\color{blue}n}\left(#1\right)}
\newcommand{\tens}[1]{\bm{\mathtt{#1}\,}}

\DeclareMathOperator*{\gaussian}{\mathcal{N}} % Gaussian distribution
\DeclareMathOperator*{\GP}{\mathcal{G}\!\mathcal{P}} % Gaussian process
\DeclareMathOperator*{\Loss}{Loss} % Loss function

\newcommand{\calert}[1]{{\color{BrickRed}\textbf{#1}}}

% -----------
% References used and shown
\begin{filecontents}{\jobname.bib}
@article{Carbajal2016,
archivePrefix = {arXiv},
arxivId = {1609.08395},
author = {Carbajal, Juan Pablo and Leit{\~{a}}o, Jo{\~{a}}o Paulo and Albert, Carlo and Rieckermann, J{\"{o}}rg},
doi = {10.1016/j.envsoft.2017.02.006},
eprint = {1609.08395},
journal = {Env. Mod. {\&} Soft.},
title = {{Appraisal of data-driven and mechanistic emulators of nonlinear simulators: The case of hydrodynamic urban drainage models}},
volume = {92},
year = {2017}
}
@article{Wani2017,
author = {Wani, Omar and Scheidegger, Andreas and Carbajal, Juan Pablo and Rieckermann, J{\"{o}}rg and Blumensaat, Frank},
doi = {10.1016/j.watres.2017.05.038},
journal = {Water Research},
month = {sep},
title = {{Parameter estimation of hydrologic models using a likelihood function for censored and binary observations}},
volume = {121},
year = {2017}
}
\end{filecontents}
\usepackage[style=authoryear, backend=biber]{biblatex}
\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{1.5em}}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{\jobname.bib}
\renewcommand{\footnotesize}{\tiny}

% -----------
% Set Eawag style
%\usetheme{Eawag_blue}
\usepackage{../beamer_themes/beamerthemeEawag_blue}

% -- transparent overlays
% \setbeamercovered{transparent}

% -- no navigation symbols
\beamertemplatenavigationsymbolsempty

% --change fonts
% \usepackage{helvet}             % Helvetic, almost identical
                                  % to Arial in Eawag ppt.
\usefonttheme[onlymath]{serif}    % make equations serife

%----------------
% title informations

\title{Emulation (surrogate modeling)}

\author[JPi Carbajal]{Juan Pablo Carbajal
\tiny{\texttt{juanpablo.carbajal@eawag.ch}}}

\institute[Eawag]{Eawag: Swiss Federal Institute of Aquatic Science and Technology}

\date{SIAM Summer School, Eawag, Switzerland. June 6, 2019}

% ====================================================================

\begin{document}

% ----------------
% Title frame

% load background for title
\setbeamertemplate{background}{
  \includegraphics[width=\paperwidth,height=\paperheight]
  {../beamer_themes/background_title_blue.png}}

{ \setbeamertemplate{footline}{} % no footer on title
  \begin{frame}
    \titlepage
  \end{frame}
}

% load background for all other slides
\setbeamertemplate{background}{
\includegraphics[width=\paperwidth,height=\paperheight]
{../beamer_themes/background_slides_blue.png}}

\setbeamertemplate{footline}[JPi Carbajal] % set footer
\addtocounter{framenumber}{-1}  % don't count title page

%%----------------
%\part{Speaker's profile}
%\begin{frame}
%  \frametitle{Background}
%  \centering
%  \includegraphics[width=\textwidth]{intro.png}
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Motivation}
  \interludeframe{\Large{\textbf{Motivation for emulation}}}

  \begin{frame}{Environmental science and engineering}
    \framesubtitle{Model calibration, Fast (extreme) event prediction, \& Design}
    \centering
    \includegraphics[width=0.8\textwidth]{flood_bridge.jpg}

    \source{https://www.dailyrecord.co.uk/news/scottish-news/storm-frank-rescuers-brave-storm-7096067}
  \end{frame}

  \begin{frame}{Engineered complex systems}
    \framesubtitle{Optimization, Control, \& Maintenance}
    \centering
    \includegraphics[width=0.8\textwidth]{WindFarm_Fluvanna_2004.jpg}

    \source{Leaflet, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=5704247}}
  \end{frame}

  \begin{frame}{Data fusion/interpretation}
    \centering
    \includegraphics[height=0.4\textheight]
    {PDESmoothing_2_Azzimonti.png}\\
    \includegraphics[height=0.3\textheight]
    {PDESmoothing_1_Azzimonti.png}\\
    \source{Azzimonti et al. 10.1080/01621459.2014.946036}
  \end{frame}

  \interludeframe{
    \Large{\textbf{What is emulation?}}\\
    \vspace{1em}
    {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
    learned from simulation results at selected inputs.\\
    \vspace{1em}
    \Large{How is emulation even possible?}

    \vspace{1em}
    \small{Simulator: a model providing quantitative results, e.g. numerical model}
  }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Is emulation possible?}

  \begin{frame}{Molecular spring}
    \centering
    \begin{columns}[T]
      \column{.5\textwidth}
        \includegraphics[height=0.7\textheight]{molecule_spring.png}
      \column{.5\textwidth}
        \includegraphics[height=0.6\textheight]{HookesLaw.png}\\
        \source{Svjo, CC BY-SA 3.0, \url{commons.wikimedia.org/w/index.php?curid=25795521}}
    \end{columns}
  \end{frame}

  \begin{frame}[t]{From molecules to fluids}
    \centering
      \href{run:../img/Pseudofluids_Chipmunk.avi?autostart&loop&noprogress}%
       {\includegraphics[height=0.8\textheight]{pseudofluids.png}}
  \end{frame}

  \begin{frame}{Emulation}
  \framesubtitle{many names \& similarities ...}
    \begin{tikzpicture}[overlay,remember picture,shift=(current page.north west)]
      \pgfmathsetseed{4}
      \foreach [count=\count] \word in {
                                        Surrogate modeling,
                                        Metamodeling,
                                        Coarse graining,
                                        Model order reduction,
                                        Inter-polation,
                                        Phenom. modeling,
                                        Extra-polation,
                                        Response surface,
                                        Empiric. modeling
                                       }
      {
        \pgfmathparse{round(70+20*rand)}
        \edef\tr{\pgfmathresult}
        \pgfmathparse{round(50+25*rand)}
        \edef\tg{\pgfmathresult}
        \pgfmathparse{round(50+50*rand)}
        \edef\tb{\pgfmathresult}
        \node [
            anchor=west,
            text width=2.5cm,
            xshift=1.5cm,
            yshift=-3cm,
            xshift={mod(\count-1,3)*(\textwidth-2cm)/2},
            yshift={floor((\count-1)/3)*(-(0.9\textheight-4cm))/2},
            xshift=rand*2cm,
            yshift=rand*1cm,
            rotate=rand*15,
            text={rgb:red,\tr;green,\tg;blue,\tb},
            font=\bfseries\large] {\word};
      }
    \end{tikzpicture}

  \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{How-to Emulate}
  \interludeframe{
    \Large{\textbf{How to emulate?}}\\
    \vspace{1em}
    {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
    learned from simulation results at selected inputs.\\
  }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\section{Machine Learning}

  \input{../includes/learning_from_data.tex}

  %\input{../includes/constrained_regularized_regression.tex}

\section{Recipes}
  \input{../includes/emulation_recipe.tex}

  \input{../includes/emulation_numerical_model.tex}
  \input{../includes/emulation_numerical_model_states.tex}

  \input{../includes/emulation_idea.tex}

\section{Example}
\begin{frame}[t]{Example: airborne wind turbine}
  \vspace{-1.5em}
  \begin{columns}
    \column{0.3\paperwidth}
      \centering
      \includegraphics[width=\textwidth]{highaltitudewind.png}\\
      \includegraphics[width=\textwidth]{airborne_windturbine.png}
    \column{0.65\paperwidth}
      \includegraphics[width=\textwidth]{airborne_optimization.png}
  \end{columns}
  Pareto front with NSGA-II algorithm, $> 1\!\times\!10^6$ speed up!\\
  $10$ sims every $8$ h (20 nodes), optim evals $\sim 10^5 \rightarrow 8\!\times\!10^4$ h\\
  $\sim 160'000$ CHF for a single curve (or value: max torque 11.2).
   
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Mechanistic emulation}
  \interludeframe{{\Large \textbf{Mechanistic emulation}}}

  \begin{frame}{Mechanistic emulation}
    Mechanistic emulator: fast interpolation/approximation of a slow simulator, learned from simulation results at selected inputs, \calert{that respects given properties} \textbf{related to underlying mechanisms}.

      \begin{itemize}
      \item Energy/Impulse/Mass conservation (e.g. rotor/divergence free fields)
      \item Transformation invariance (e.g. symmetries)
      \item Monotonicity (increasing/decreasing function), Positiveness, \dots
      \item Smoothness, Regularity
      \item Differential equation~\footfullcite{Carbajal2016}
      \end{itemize}
  \end{frame}

  \begin{frame}[t]{Mechanistic example: marker conservation}
    \vspace{-2em}
    \hspace{-2em}\includegraphics[height=0.3\textheight]{example1.png}

    \vspace{-5.5em}
    \centering
    \[
    M = \int_{0}^T {\dot{m} (t) \ud t} \equiv \texttt{constant}
    \]

    Data: $\bset{t_i, \dot{m}_i - M/T}$

    \includegraphics[width=0.8\textwidth]{invariantGP_cteIntegral_Ginsbourger.png}
    
    \source{Ginsbourger et al. 10.1016/j.jspi.2015.10.002. arXiv:1308.1359}
  \end{frame}

  \section{Prior models}
  \interludeframe{\Large{\textbf{How do we use (prior) models with data?}}}
  \input{../includes/guessing_sample_model.tex}

  \input{../includes/intra_extra_inter.tex}

  \input{../includes/intrapolation.tex}

  \input{../includes/extrapolation.tex}
  
  \begin{frame}{Summary: models \& data}
     \begin{itemize}
      \item without knowledge (assumptions) about the process, there is no "best" interpolant
      \item knowledge about the process is the "mechanistic" part
      \item mechanistic knowledge "shrinks" the hypothesis set
      \item \dots
      \item intra-/extra- depends on the correlation length!
      \item "within" and "away" from data is unclear in higher dimensions
     \end{itemize}

     Popular methods to include mechanistic knowledge are \textbf{kernel methods}.
     Among them \textbf{Gaussian processes} (krigging and co-krigging)
  \end{frame}

  \section{Quadratic programming}
  \interludeframe{{\Large \textbf{Quadratic programming}}}
  
  \input{../includes/qp.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Summary}
  \interludeframe{\Large{\textbf{Summary}}}

  \input{../includes/ML_and_mechanistic_modeling.tex}

  \begin{frame}[t]{Emulation at a glance}
    \vspace{-2.5em}
    \begin{center}
      \includegraphics[height=0.8\textheight]{emulation_errorvscost.png}
    \end{center}
    \begin{reference}{2mm}{2.5em}
   Carbajal, JP (2018), Computational effort and emulation error. EmuMore project blog. 
   \url{https://goo.gl/5fTMWs}
  \end{reference}

  \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\interludeframe{
\Huge{Thank you!}\\
\Huge{Q\&A}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%----------------
\part{Gaussian distributions}
  \interludeframe{{\Large \textbf{Gaussian distributions}}}

  \input{../includes/mvnormal_distribution.tex}

  \input{../includes/gp_regression.tex}

\end{document}
