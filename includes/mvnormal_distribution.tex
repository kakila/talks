\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\begin{frame}[t]{Gaussian distribution}
  \only<1>
  {
    \framesubtitle{1-dimensional (single variable)}
    \centering
    \[
      x \sim \gaussian\left(\mu, \sigma^2\right) \propto e^{\frac{-(x - \mu)^2}{2\sigma^2}} \quad \mu,\sigma \in \mathbb{R}
    \]

    \begin{tikzpicture}
    \begin{axis}
    [
      name = pdf,
      no markers, domain=0:8, samples=100,
      axis lines*=left, xlabel=$x$, ylabel=$\operatorname{pdf}(x)$,
      every axis y label/.style={at=(current axis.above origin),anchor=south},
      every axis x label/.style={at=(current axis.right of origin),anchor=west},
      height=0.5\textheight, width=0.8\textwidth,
      xtick=4, ytick=\empty,
      xticklabel=$\mu$,
      enlargelimits=false, clip=false, axis on top,
      xmajorgrids
      ]
      \addplot [very thick,cyan!50!black] {gauss(4,1)};
      \draw [latex-latex](axis cs:4,0.242) -- node [fill=white] {$\sigma$} (axis cs:5,0.242);

    \end{axis}

    \begin{axis}[
      at=(pdf.right of south east),
      domain=0:2,
      axis lines*=left, xlabel=dim, ylabel=$x$,
      every axis y label/.style={at=(current axis.above origin),anchor=south},
      every axis x label/.style={at=(current axis.right of origin),anchor=west},
      height=0.5\textheight, width=0.2\textwidth,
      xtick=1, ytick=4,
      yticklabel=$\mu$,
      enlargelimits=false, clip=false, axis on top,
      ymajorgrids
      ]
      \addplot[only marks, mark=o,error bars/.cd, y dir=both, y explicit]
      coordinates { (1,4) +- (1,1) };
    \end{axis}

    \end{tikzpicture}
  }

  \only<2>
  {
    \framesubtitle{2-dimensional (two variables)}
    \centering
    \begin{align*}
      &\begin{bmatrix} x_1\\ x_2 \end{bmatrix} = \bm{x} \sim \gaussian\left(\bm{\mu}, \tens{\Sigma}\right) \propto \exp\left[\frac{1}{2} (\bm{x} - \bm{\mu})^\trp \tens{\Sigma}^{-1} (\bm{x} - \bm{\mu})\right] \\
      &\bm{\mu} \in \mathbb{R}^2, \; \tens{\Sigma} \in \mathbb{R}^\stimes{2}{2}
    \end{align*}

    \begin{tikzpicture}
      \begin{axis}[
      name = pdf,
      height=0.5\textheight, width=0.6\textwidth,
      xtick=\empty, ytick=\empty, ztick=\empty,
      xlabel=$x_1$, ylabel=$x_2$,
      enlargelimits=false, clip=false, axis on top,
      ]
      \addplot3[mesh, domain=-4:4] {exp(-(1.02*x^2+1.64*y^2-0.33*x*y)/2)};
      \end{axis}

    \begin{axis}[
      at=(pdf.right of south east), xshift=1cm,
      domain=0:3,
      axis lines*=left, xlabel=$t$, ylabel=$x_t$,
      every axis y label/.style={at=(current axis.above origin),anchor=south},
      every axis x label/.style={at=(current axis.right of origin),anchor=west},
      height=0.5\textheight, width=0.3\textwidth,
      xtick={1,2}, ytick={4,3.5},
      yticklabels={$\mu_1$, $\mu_2$},
      enlargelimits=true, clip=false, axis on top,
      ymajorgrids
      ]
      \addplot[only marks, mark=o,error bars/.cd, y dir=both, y explicit]
      coordinates {
      (1,4) +- (1,1)
      (2,3.5) +- (0.6,0.6)
      };
    \end{axis}

    \end{tikzpicture}
  }

  \only<3>
  {
    \framesubtitle{3-dimensional}
    \centering
    \begin{align*}
      &\bm{x} \sim \gaussian\left(\bm{\mu}, \tens{\Sigma}\right) \propto \exp\left[\frac{1}{2} (\bm{x} - \bm{\mu})^\trp \tens{\Sigma}^{-1} (\bm{x} - \bm{\mu})\right] \\
      &\bm{\mu} \in \mathbb{R}^3, \; \tens{\Sigma} \in \mathbb{R}^\stimes{3}{3}
    \end{align*}

    \begin{tikzpicture}
    \begin{axis}[
      domain=0:4,
      axis lines*=left, xlabel=$t$, ylabel=$x_t$,
      every axis y label/.style={at=(current axis.above origin),anchor=south},
      every axis x label/.style={at=(current axis.right of origin),anchor=west},
      height=0.5\textheight, width=0.5\textwidth,
      xtick={1,2,3}, ytick={4,3.5,4.5},
      yticklabels={$\mu_1$, $\mu_2$, $\mu_3$},
      enlargelimits=true, clip=false, axis on top,
      ymajorgrids
      ]
      \addplot[only marks, mark=o,error bars/.cd, y dir=both, y explicit]
      coordinates {
      (1,4) +- (1,1)
      (2,3.5) +- (0.6,0.6)
      (3,4.5) +- (0.4,0.4)
      };
    \end{axis}

    \end{tikzpicture}
  }

  \only<4>
  {
    \framesubtitle{$\infty$-dimensional: GP}
    %\centering
    \[
    \bm{\mu} \rightarrow m(t) \quad \tens{\Sigma} \rightarrow k(t, t^\prime)
    \]
    \vspace{-1em}
    \[
      x(t) \sim \GP(\cmean{t}, \ccov{t,t'})
    \]

    \includegraphics[width=0.52\textwidth, trim=2cm 0cm 1cm 0cm, clip]{SDE.pdf}%
    \includegraphics[width=0.55\textwidth, trim=1cm 0cm 1cm 0cm, clip]{KernelGPDS.pdf}
  }

\end{frame}

\begin{frame}[t]{Gaussian process in multiple output dimensions}
\framesubtitle{co-krigging}
  \only<1-2>
  {
    2 output (\textbf{function}) components:
    \[
      \bm{f}(t) = \begin{bmatrix}
        f_{1}(t)\\
        f_{2}(t)
      \end{bmatrix}
    \]
    i.e. at each input $t$ we have an output vector.

    Covariance function
  }
  \only<1>
  {
    (independent):
    \[
      K(t, t^\prime) = \begin{bmatrix}
        k_{11}(t, t^\prime) & \tens{0} \\
        \tens{0} & k_{22}(t, t^\prime)
      \end{bmatrix}
    \]
  }
  \only<2>
  {
    (coupled):
    \[
      K(t, t^\prime) = \begin{bmatrix}
        k_{11}(t, t^\prime) & k_{12}(t, t^\prime) \\
        k_{21}(t, t^\prime) & k_{22}(t, t^\prime)
      \end{bmatrix}
    \]
  }
  \only<3>
  {
  \vspace{-2em}
   \[
    \begin{bmatrix}
        \dot{x}(t)\\
        \dot{v}(t)
      \end{bmatrix} = \begin{bmatrix}
                                0 & 1\\
                                a & b
                              \end{bmatrix} \begin{bmatrix}
                                x(t)\\
                                v(t)
                              \end{bmatrix}
   \]

  \vspace{-0.5em}
  \begin{center}
   \includegraphics[width=0.8\textwidth, trim=0 0 0 0.5cm, clip]{covLTIdD.png}
  \end{center}

  \begin{reference}{2mm}{2.5em}
   Carbajal, JP. GP emulation manual v0.0.1 \url{https://kakila.bitbucket.io/gpemulation/manual.html}
  \end{reference}

  }
\end{frame}
