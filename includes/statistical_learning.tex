\begin{frame}{The statistical learning problem}
\textbf{Probability \& statistics}: model of an experiment \textrightarrow reasoning about outcomes

\textbf{Statistical learning}: outcomes of experiments \textrightarrow properties of the underlying model

\vspace{0.5em}
Finite observed data set $\lbrace (x_i, y_i) \rbrace$ independently generated from same unknown experiment (i.i.d.: independent and identically distributed).

Estimate empirical joint distribution $P(X,Y)$:

\only<1>{
  \centering
  \includegraphics[height=0.4\textheight]{SamplingTriptych.png}\\\vspace{-0.5em}
  \source{S. Lau, J. Gonzalez, \& D. Nolan, \href{https://learningds.org/ch/17/inf_pred_gen_dist.html}{CC BY-SA-NC-ND 4.0}}
}
\only<2>{
  \begin{itemize}
  \item Expected output given the input, $f(x) = E[Y | X = x]$, e.g. regression.
  \item Classifier assigns $x$ to a most likely class, $f(x) = \armax_{y} P(Y = y | X = x)$, where $Y$ takes values from a finite set.
  \item the density $p(X,Y)$ of $P(X,Y)$.
  \end{itemize}

  \vspace{0.5em}
  An inverse problem: estimate property of an unobserved object (the underlying distribution), based on an operation applied to the object (sampling from the distribution).
}

\note<1>{
Almost all machine learning theory builds on i.i.d. data.

In practice, the i.i.d. assumption can be violated in various ways, for instance if distributions shift or interventions in a system occur.

\vspace{0.5em}
Liu, J., Shen, Z., He, Y., Zhang, X., Xu, R., Yu, H., \& Cui, P. (2023). Towards Out-Of-Distribution Generalization: A Survey (arXiv:2108.13624). arXiv. http://arxiv.org/abs/2108.13624
}
\note<2>{
$x_i$ inputs, covariates, cases.
$y_i$ outputs, targets, labels.
}
\end{frame}

\begin{frame}{Statistical learning theory}
\only<1-2>{
Using $P(X,Y)$ \textrightarrow $x$ not seen in the data set, conditional expectation $f(x)$ is undefined.
}

\vspace{0.5em}
\only<1>{
  \vspace{-1em}
  \begin{center}
  \includegraphics[height=0.3\textheight,trim=600px 70px 0px 150px, clip]{SamplingTriptych.png}
  \end{center}
  \vspace{-1em}
  {\Large \color{BrickRed}\textbf{!}} Extend it according to any fixed rule.

  Additional assumptions about $f(x) \in \mathcal{F}$ are needed: \textbf{capacity measure} $|\mathcal{F}|$

  \begin{itemize}
  \item Large capacity: can fit most conceivable data sets.
  No surprise it fit ours.
  \item Small capacity: $f$ fits only few data sets.
  If fit ours: reason to believe we found a pattern.
  \end{itemize}

  For the latter, accuracy for future data only \textbf{sampled from the same distribution} $P(X,Y)$.
}
\only<2>{
$f$ should minimize the risk functional (assume there is $p(x,y)$):
\[
R[f] = \int | f(x) - y | p(x,y) \ud x \ud y
\]

but we don't have $p(x,y)!$ \textrightarrow minimize the empirical risk over the data:
\[
R^n_\text{emp}[f] = \frac{1}{n} \sum_{i=1}^n | f(x_i) - y_i |
\]

For $f \in \mathcal{F}$ (a \textbf{small capacity} class): $ R[f] \leq R^n_\text{emp}[f] + O(|\mathcal{F}|)$ with high probability.
$O(|\mathcal{F}|)$ grows with the class capacity: the bound is not so tight.

\textbf{universality}: any $f$ learned in limit of $\infty$ data $\color{BrickRed}\bm{\nrightarrow}$ every $f$ learnable from finite data.
}

\note<2>{
In the slide $|\mathcal{F}|$ denotes the capacity or size of the function class.
A useful measure is the Vapnik-Chervonenkis (VC) Dimension.

From \emph{Jonas Peters, Dominik Janzing, and Bernhard Schlkopf. 2017. Elements of Causal Inference: Foundations and Learning Algorithms. The MIT Press.}, page 220:

"
A typical risk bound of statistical learning theory
states that for all $\delta > 0$, with probability $1 - \delta$ and for all functions in the class $\mathcal{F}$, we have
\[
R[f] \leq R^n_\text{emp}[f] + \sqrt{\frac{h \left[\log \left(\frac{2n}{h}\right) + 1\right] - \log\left(\frac{\delta}{4}\right)}{n}}
\]
where $h$ is the VC dimension of the function class $\mathcal{F}$.
This means that if we can come up with an $\mathcal{F}$ that has small VC dimension yet
contains functions that are sufficiently suitable for the given task to achieve a
small $R^n_\text{emp}[f]$, then we can guarantee (with high probability) that those
functions have small expected error on future data from the same distribution.
This formulates a non-trivial trade-off: on the one hand, we would like to work with a
large class of functions to allow for a small $R^n_\text{emp}[f]$, but on the other hand,
we want the class to be small to control $h$.
"
}
\end{frame}

%\only<2>{
%Solved with assumptions about the class of $f \rightarrow$ if works, function class incorporates prior knowledge that turns out to be consistent with the data.

%Prior knowledge incorporation leads to different machine learning approaches:
%\begin{itemize}
%\item Bayesian: specify prior distributions over function classes and noise models.
%\item Regularization: penalties (regularizers) added into optimization problems to bias our solutions.
%\end{itemize}
%}

