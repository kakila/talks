import re
import sys
import logging
import unicodedata
import argparse
from pathlib import Path


def slugify(value, allow_unicode: bool = False) -> str:
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def parse_args(argv: list[str]) -> argparse.Namespace:

    parser = argparse.ArgumentParser("split beamer frames into single frame files.")
    parser.add_argument("-i", "--input-file", type=Path, default=None, 
                         required=True, help="Path to file to process.")
    parser.add_argument("-of", "--out-folder", type=Path, default=None,
                         help="Folder to store the resulting files."
                              "Default is the same forlder as the input file.")
    parser.add_argument("--tex-input-path", type=Path, default=None,
                         help="The inputs in the TeX splitted file will use the given path to the frame files.")
    
    args = parser.parse_args(argv)
    
    if args.out_folder is None:
        args.out_folder = args.input_file.parent
    
    return args


def frame_regex() -> re.Pattern:
    """ Compiled regular expression to match frames.

        Examples
        --------
        >>> txt = "\\\\begin{frame}{frame title}\\n  contents\\n\\\\end{frame}"
        >>> m = frame_regex().search(txt)
        >>> print(*[": ".join(kv) for kv in m.groupdict().items()], sep="\\n")
        name: frame title
        contents: contents

        >>> txt = "{\\n \\\\something \\n \\\\begin{frame}{frame title}\\n  contents\\n\\\\end{frame} \\n \\\\something}"
        >>> m = frame_regex().search(txt)
        >>> print(*[": ".join(kv) for kv in m.groupdict().items()], sep="\\n")
        name: frame title
        contents: contents

        >>> txt = "\\\\begin{frame}\\\\frametitle{frame title}\\n  contents\\n\\\\end{frame}"
        >>> m = frame_regex().search(txt)
        >>> print(*[": ".join(kv) for kv in m.groupdict().items()], sep="\\n")
        name: frame title
        contents: contents

        >>> txt = "\\\\begin{frame}\\n  \\\\frametitle{frame title}\\n  contents\\n\\\\end{frame}"
        >>> m = frame_regex().search(txt)
        >>> print(*[": ".join(kv) for kv in m.groupdict().items()], sep="\\n")
        name: frame title
        contents: contents

        >>> txt = Path("test_snippet.tex").read_text()
        >>> ms = frame_regex().finditer(txt)
        >>> print(*[m["name"] for m in ms if m["name"]], sep="\\n")
        Speaker's profile
        Alien researcher
        From molecules to fluids
    """
    regex_frame = r"\s*\\begin\{frame\}(\[.*?\])?\s*"\
                  + r"(\\frametitle)?"\
                  + r"(\{(?P<name>.*?)\})?\s*"\
                  + r"(?P<contents>.*?)\s*"\
                  + r"\\end\{frame\}"
    regex_frame = re.compile(regex_frame, re.DOTALL)
    return regex_frame


if __name__ == "__main__":

    args = parse_args(sys.argv[1:])

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    # Create output folder if doesn't exists
    args.out_folder.mkdir(parents=True, exist_ok=True)
    
    contents = args.input_file.read_text()

    splitfile = args.out_folder / (args.input_file.stem + "_split.tex")

    matches = frame_regex().finditer(contents)

    counter = dict()
    with splitfile.open("w+") as of:
        for m in matches:
            if m["name"]:
                # create new file with frame name as filename
                fname = slugify(m["name"])
                if fname in counter:
                    counter[fname] += 1
                    fname = fname + f"_{counter[fname]}"
                else:
                    counter[fname] = 0
                newfile = args.out_folder / (fname + ".tex")
                # write the whole match into the file
                newfile.write_text(m.group().strip())
                # write the input in the split file
                if args.tex_input_path is None:
                    of.write(f"\input{{{newfile}}}\n")
                else:
                    of.write(f"\input{{{args.tex_input_path}/{newfile.name}}}\n")

                logging.info(f"frame: {m['name']} --> {newfile.name}")