from pathlib import Path
from string import Template

container_tex = Template(
"""
\\documentclass[usenames,dvipsnames,USenglish]{beamer}
$preamble

\\usetheme{Madrid}
\\definecolor{OSTpink}{RGB}{140,25,95}
\\usecolortheme[named=OSTpink]{structure}
\\setbeamertemplate{navigation symbols}{}
\\setbeamertemplate{blocks}[rounded][shadow=true]
\\usefonttheme{professionalfonts}

\\usepackage{multimedia}

\\begin{document}
$slides
\\end{document}
"""
)

slide_folder = Path("../includes").resolve()
preamble = ["beamer_basics.tex", "mathsymbols.tex"]
if __name__ == "__main__":
    slides_txt = ""
    preamble_txt = ""
    for slide in sorted(slide_folder.glob("*.tex")):
        if slide.name in preamble:
            preamble_txt += f"\input{{{slide}}}\n"
        else:
            slides_txt += f"\section{{{slide.stem.replace('_',' ')}}}\n\input{{{slide}}}\n"

    of = Path("../slide_gallery/slide_gallery.tex")
    of.parent.mkdir(exist_ok=True)
    of.write_text(container_tex.substitute(preamble=preamble_txt, 
                                           slides=slides_txt))
    print(f"Slide gallery written to {of}")

